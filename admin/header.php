<?php
  // Enable error reporting
  $debug = true;
  if ($debug)
  {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    ini_set('error_reporting', E_ALL);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo (!isset($page_title) || is_null($page_title)) ? "Admin page" : $page_title; ?></title>
  </head>
  <body>
