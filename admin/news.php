<?php
  // Include header
  $page_title = "Add/Edit/View News";
  include("header.php");

  // Include required libraries
  include_once("../include/languages.php");
  include_once("../include/news.php");

  // Get current filename
  $current_filename = basename($_SERVER['PHP_SELF']);

  // Get a list of all supported languages
  $languages = Languages::getLanguageCodes();
?>

<?php
  // Handle requests
  if (isset($_POST['action']))
  {
     if ($_POST['action']=="update") // Update a news
    {
      if (News::updateNews($_POST['id'], $_POST['language_code'], $_POST['title'], $_POST['body']))
      {
        echo "<p><b>News updated</b></p>";
      }
      else
      {
        echo "<p><b>Updating news impossible</b></p>";
      }
    }
    else if ($_POST['action']=="delete") // Delete a news
    {
      if (News::deleteNews($_POST['id']))
      {
        echo "<p><b>News deleted</b></p>";
      }
      else
      {
        echo "<p><b>Deleting news impossible</b></p>";
      }
    }
    else if ($_POST['action']=="add") // Add a news
    {
      if (News::addNews($_POST['news']))
      {
        echo "<p><b>News added</b></p>";
      }
      else
      {
        echo "<p><b>Adding news impossible</b></p>";
      }
    }
  }
?>

<?php
  // Get all news
  $all_news = [];
  foreach ($languages as $language_code)
  {
    $all_news[$language_code] = News::getNews($language_code, -1);
  }

  // Create a table with all news (and all languages)
  $th_languages = "";
  foreach ($languages as $language_code)
  {
    $th_languages .= "
        <th>Title in ".$language_code."</th>
        <th>Body in ".$language_code."</th>
        <th>Update ".$language_code."</th>";
  }

  echo "
    <table border=\"1\">
      <caption>All news</caption>
      <tr>
        <th>ID</th>".$th_languages."
        <th>Date</th>
        <th>Action</th>
      </tr>";

  // Add a row to "add a news"
  echo "
      <tr>
        <form action=\"".$current_filename."\" method=\"post\">
          <input name=\"action\" value=\"add\" type=\"hidden\">
          <td></td>";

  foreach ($languages as $language_code)
  {
    echo "
          <td><input name=\"news[".$language_code."][title]\" type=\"text\"></td>
          <td><textarea rows=\"4\" columns=\"50\" name=\"news[".$language_code."][body]\" type=\"text\"></textarea></td>
          <td></td>";
  }

  echo "
          <td></td>
          <td><input value=\"Add\" type=\"submit\"></td>
        </form>
      </tr>";

  // Get news information for every news ID
  $news_ids = News::getAllNewsId();
  foreach ($news_ids as $news_id)
  {
    $tr_news = "
      <tr>
        <td>".$news_id."</td>";

    // Add the date and title/body for every language
    foreach ($languages as $language_code)
    {
      $date = "";
      $title = "";
      $body = "";

      $key = array_search($news_id, array_column($all_news[$language_code], 'id'));
      if (array_key_exists($key, $all_news[$language_code]))
      {
        $date = $all_news[$language_code][$key]['date'];
        $title = $all_news[$language_code][$key]['title'];
        $body = $all_news[$language_code][$key]['body'];
      }
      $tr_news .= "
        <form action=\"".$current_filename."\" method=\"post\">
          <input name=\"action\" value=\"update\" type=\"hidden\">
          <input name=\"id\" value=\"".$news_id."\" type=\"hidden\">
          <input name=\"language_code\" value=\"".$language_code."\" type=\"hidden\">
          <td><input name=\"title\" type=\"text\" value=\"".$title."\"></td>
          <td><textarea rows=\"4\" columns=\"50\" name=\"body\" type=\"text\">".$body."</textarea></td>
          <td><input value=\"Update\" type=\"submit\">
        </form></td>";
    }
    $tr_news .= "
        <td>".$date."</td>
        <td>
          <form action=\"".$current_filename."\" method=\"post\">
            <input name=\"action\" value=\"delete\" type=\"hidden\">
            <input name=\"id\" value=\"".$news_id."\" type=\"hidden\">
            <input value=\"Delete\" type=\"submit\">
          </form>
        </td>
      </tr>";

    echo $tr_news;
  }

  echo "
    </table>";
?>

<?php
  // Include footer
  include("footer.php");
?>
