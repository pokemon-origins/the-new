$(document).ready(
  function()
  {
    function toogleMainChatBox()
    {
      $('.chat_body').slideToggle('slow');
    }

    function deleteMessageBox()
    {
      $('.msg_box').remove();
    }

    function toogleMessageBox()
    {
      $('.msg_wrap').slideToggle('slow');
    }

    // Toogle the main chat box when clicking on its header
    $('.chat_head').click(function(){toogleMainChatBox();});

    // TODO: Add function to fetch new messages every X sec from the API and update chat_body content
    // It should:
    //    - Update the message box if the received message are from the right channel_id
    //    - Update the chat box with number of unread messages

    // Open a specific message box
    $('.chat_user').click(
      function(event)
      {
        // Delete previous message box (if exist)
        deleteMessageBox();

        // Add the new message box
        // TODO: Query the API to get the real messages
        var channel_id = $(event.target).attr("channel_id");
        var channel_name = $(event.target).text();
        var div_add = '<div class="msg_box" channel_id='+channel_id+'>\
          <div class="msg_head">'+channel_name+'\
            <div class="msg_close">x</div>\
          </div>\
          <div class="msg_wrap">\
            <div class="msg_body">\
              <div class="msg_a">Hey it\'s '+channel_name+'!</div>\
              <div class="msg_b">Hi! How are you?</div>\
              <div class="msg_a">I\'m fine :)<br/>Do you want to play?</div>\
              <div class="msg_push"></div>\
            </div>\
            <div class="msg_footer"><textarea class="msg_input" rows="1"></textarea></div>\
          </div>\
        </div>';
        $('body').append(div_add);

        // Toogle message box when clicking on its header
        $('.msg_head').click(function(){toogleMessageBox();});

        // Close message box when clicking the close message box button
        $('.msg_close').click(function(){deleteMessageBox();});

        // Written message is appended to messages and is sent to the API
        $('.msg_input').keypress(
          function(e)
          {
            if (e.keyCode == 13) {
              e.preventDefault();
              var msg = $(this).val();
              $(this).val('');
              if(msg!='')
              {
                // TODO: Send the message to the API to store it in the database
                $('<div class="msg_b">'+msg+'</div>').insertBefore('.msg_push');
              }
              $('.msg_body').scrollTop($('.msg_body')[0].scrollHeight);
            }
          }
        );
      }
    );
  }
);
