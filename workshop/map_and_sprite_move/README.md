Basic found there:
http://digipiph.com/blog/creating-sprite-character-movement-javascript-and-jquery-ver-11

Medium found there:
http://digipiph.com/blog/creating-sprite-character-movement-javascript-and-jquery-ver-12

Advanced found there:
http://digipiph.com/digipiph-game-manager
https://bitbucket.org/dustincochran/digipiph-game-manager.git


Note for the advanced package:
DGM v1.x.x Requires:
EaselJS/CreateJS v0.7.0 (included in the package)
Modified version of Pixel Perfect Collision Detection for v0.7.0 EaselJS (in the package): http://indiegamr.com/easeljs-pixel-perfect-collision-detection-for-bitmaps-with-alpha-threshold/ and https://github.com/olsn/Collision-Detection-for-EaselJS


What is the Digipiph Game Manager?
The Digipiph Game Manager, or DGM for short, is a HTML5 framework that utilizes EaselJS / CreateJS to create HTML5 games with little coding. The only coding required for a basic game is configuring your sprites and images. All basic 8 directional movement, sprite animations, collisions, canvas drawing and updating are performed within the framework.

Getting Started with the Digipiph Game Manager
Check out the documentation here. It will teach you everything you need to know about using the DGM, from setting up your canvases to moving your sprites.

Documentation for advanced package: http://digipiph.com/digipiph-game-manager/docs