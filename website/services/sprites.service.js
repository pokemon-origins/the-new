services.factory('SpritesService', function() {
  // ---- 32x32 Pokemon sprites ----
  var getStaticPokemonSprite32x32Style = function(pokemon_id) {
    var int_id = parseInt(pokemon_id);

    // Find the right pokemon in the full image
    var horizontal = 0;
    var vertical = 0;

    var modulus = (int_id % 100);
    // Case for pokemon 100, 200, ...
    if (modulus == 0)
    {
      horizontal = (100 - 1)*32;
      vertical = ((Math.floor((int_id) / 100) - 1)*32);
    }
    // Case for all other pokemons
    else
    {
      horizontal = ((modulus - 1)*32);
      vertical = (Math.floor(int_id / 100)*32);
    }

    // Send the image style
    return "width:32px;height:32px;background-image:url(../images/pokemons/32x32.png);background-position: -"+horizontal+"px -"+vertical+"px;' src='../images/32x32_transparent.png";
  };

  var getBase32x32Src = function() {
    return "../images/32x32_transparent.png";
  };

  // ---- Animated Pokemon images ----
  var getAnimatedPokemonSrc = function(pokemon_id, is_back, is_shiny) {
    var path = "../images/pokemons/animated/";
    if (is_shiny) {
      path += "shiny/";
    }
    if (is_back) {
      path += "back/";
    }
    path += pokemon_id;
    path += ".png";

    return path;
  };

  var getPokemonMediumSizeSrc = function(pokemon_id, is_back, is_shiny) {
    var path = "../images/pokemons/96x96/";
    if (is_shiny) {
      path += "shiny/";
    }
    if (is_back) {
      path += "back/";
    }
    path += pokemon_id;
    path += ".png";

    return path;
  };

  // ---- Gender images ----
  var getMaleGenderSrc = function() {
    return "../images/genders/1.png";
  };
  var getFemaleGenderSrc = function() {
    return "../images/genders/2.png";
  };
  var getNoGenderSrc = function() {
    return "../images/genders/3.png";
  };
  var getGenderSrc = function(gender_id) {
    return "../images/genders/"+gender_id+".png";
  };

  // ---- Footprints images ----
  var getFootprintSrc = function(pokemon_id) {
    return "../images/footprints/"+pokemon_id+".png";
  };

  // ---- Shapes images ----
  var getShapeSrc = function(shape_id) {
    return "../images/shapes/"+shape_id+".png";
  };

  // ---- Habitats images ----
  var getHabitatSrc = function(habitat_id) {
    return "../images/habitats/64x48/"+habitat_id+".png";
  };

  // ---- Logo image ----
  var getLogoSrc = function() {
    return "../images/logo/logo.png";
  };

  // ---- Damage class images ----
  var getDamageClassSrc = function(damage_id) {
    return "../images/damage-classes/"+damage_id+".png";
  };

  // ---- Language images ----
  var getLanguageSrc = function(language_code) {
    return "../images/languages/32x21/"+language_code+".png";
  };

  // ---- Social images ----
  var getFacebookLogoSrc = function() {
    return "../images/social/facebook.svg";
  };

  var getTwitterLogoSrc = function() {
    return "../images/social/twitter.svg";
  };

  // ---- Menu icons ----
  var getAboutSrc = function() {
    return "../images/others/about.svg";
  };

  var getNewsSrc = function() {
    return "../images/others/news.svg";
  };

  var getHomeSrc = function() {
    return "../images/others/home.svg";
  };

  var getNotesSrc = function() {
    return "../images/others/notes.svg";
  };

  var getPokedexSrc = function() {
    return "../images/others/pokedex.svg";
  };

  var getSettingsSrc = function() {
    return "../images/others/settings.svg";
  };

  var getBagSrc = function() {
    return "../images/others/bag.svg";
  };

  var getNotificationsSrc = function() {
    return "../images/others/notifications.svg";
  };

  var getNoNotificationSrc = function() {
    return "../images/others/no_notification.svg";
  };

  // Money icons
  var getPokedollarSrc = function() {
    return "../images/others/pokedollar.svg";
  };

  var getGoldSrc = function() {
    return "../images/others/gold.svg";
  };

  var getRankSrc = function() {
    return "../images/others/rank.svg";
  };

  var getHuntingSrc = function() {
    return "../images/others/hunting.svg";
  };

  var getAvatarSrc = function() {
    return "../images/others/avatar.svg";
  };

  var getProfileSrc = function() {
    return "../images/others/profile.svg";
  };

  var getTrophySrc = function() {
    return "../images/others/trophy.svg";
  };

  var getAchievementSrc = function(rank) {
    return "../images/achievements/" + rank + ".svg";
  };

  var getAchievementsSrc = function() {
    return "../images/others/achievements.svg";
  };

  var getStarSrc = function() {
    return "../images/others/star.svg";
  };

  var getMarketSrc = function() {
    return "../images/others/shop.svg";
  };

  var getUserReportSrc = function() {
    return "../images/others/report.svg";
  };

  var getAttackSrc = function() {
    return "../images/others/fight.svg";
  };

  // ---- Casino ----
  var getCasinoSrc = function() {
    return "../images/others/casino.svg";
  };

  var getCasinoSlotSrc = function(slot) {
    return "../images/casino/"+slot+".png";
  };

  // ---- Items ----
  var getItemIconSrc = function(id) {
    return "../images/items/small/" + id + ".png";
  };

  var getBigItemSrc = function(id) {
    return "../images/items/big/" + id + ".png";
  };

  var getItemPocketSrc = function(id, selected) {
    var path = "../images/item-pockets/";
    if (selected) {
      path += "selected/";
    }
    path += id + ".png";

    return path;
  };

  // Capture icons
  var getCapturedNormalPokemonSrc = function() {
    return getItemIconSrc(4);
  };

  var getCapturedShinyPokemonSrc = function() {
    return getItemIconSrc(9);
  };

  // ---- Badges ----
  var getBadgeSrc = function(badge_id) {
    return "../images/badges/"+badge_id+".png";
  };

  // ---- User avatars ----
  var getFallbackUserAvatarSrc = function(gender_id) {
    return "../images/player_gender/"+gender_id+".svg";
  };

  var avatar_no_cache = 0;
  var reloadAvatars = function()  {
    avatar_no_cache += 1;
  };

  var getUserAvatarSrc = function(user_id) {
    // Give dummy avatar if invalid user (prevent error logs in server side)
    if (user_id < 1 || user_id == null)
    {
      return getFallbackUserAvatarSrc(1);
    }

    return "../images/avatars/"+user_id+".png?r=" + avatar_no_cache;
  };

  var getBannerSrc = function() {
    return "../images/banner.png";
  };

  var getFavIconSrc = function() {
    return "../images/favicon.png";
  };

  // ---- List of all available functions ----
  return {
    getStaticPokemonSprite32x32Style: getStaticPokemonSprite32x32Style,
    getBase32x32Src: getBase32x32Src,
    getAnimatedPokemonSrc: getAnimatedPokemonSrc,
    getPokemonMediumSizeSrc: getPokemonMediumSizeSrc,
    getMaleGenderSrc: getMaleGenderSrc,
    getFemaleGenderSrc: getFemaleGenderSrc,
    getNoGenderSrc: getNoGenderSrc,
    getGenderSrc: getGenderSrc,
    getFootprintSrc: getFootprintSrc,
    getShapeSrc: getShapeSrc,
    getHabitatSrc: getHabitatSrc,
    getLogoSrc: getLogoSrc,
    getLanguageSrc: getLanguageSrc,
    getFacebookLogoSrc: getFacebookLogoSrc,
    getTwitterLogoSrc: getTwitterLogoSrc,
    getAboutSrc: getAboutSrc,
    getNewsSrc: getNewsSrc,
    getHomeSrc: getHomeSrc,
    getNotesSrc: getNotesSrc,
    getPokedexSrc: getPokedexSrc,
    getSettingsSrc: getSettingsSrc,
    getBagSrc: getBagSrc,
    getUserAvatarSrc: getUserAvatarSrc,
    reloadAvatars: reloadAvatars,
    getFallbackUserAvatarSrc: getFallbackUserAvatarSrc,
    getPokedollarSrc: getPokedollarSrc,
    getGoldSrc: getGoldSrc,
    getCapturedNormalPokemonSrc: getCapturedNormalPokemonSrc,
    getCapturedShinyPokemonSrc: getCapturedShinyPokemonSrc,
    getBannerSrc: getBannerSrc,
    getFavIconSrc: getFavIconSrc,
    getRankSrc: getRankSrc,
    getHuntingSrc: getHuntingSrc,
    getAvatarSrc: getAvatarSrc,
    getProfileSrc: getProfileSrc,
    getBadgeSrc: getBadgeSrc,
    getTrophySrc: getTrophySrc,
    getAchievementSrc: getAchievementSrc,
    getStarSrc: getStarSrc,
    getItemIconSrc: getItemIconSrc,
    getItemPocketSrc: getItemPocketSrc,
    getBigItemSrc: getBigItemSrc,
    getMarketSrc: getMarketSrc,
    getNotificationsSrc: getNotificationsSrc,
    getNoNotificationSrc: getNoNotificationSrc,
    getUserReportSrc: getUserReportSrc,
    getAttackSrc: getAttackSrc,
    getDamageClassSrc: getDamageClassSrc,
    getAchievementsSrc: getAchievementsSrc,
    getCasinoSrc: getCasinoSrc,
    getCasinoSlotSrc: getCasinoSlotSrc
  };
});
