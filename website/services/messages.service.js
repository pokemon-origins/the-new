services.factory('MessagesService', function($timeout) {
  var maxMessagesBuffer = 5;
  messages = [];

  // Maximum elements the service should keep track (first in first out system)
  var setMaxMessagesBufferSize = function(size) {
    maxMessagesBuffer = size;
    while (messages.length > maxMessagesBuffer) {
      messages.shift();
    }
  };

  var getMaxMessagesBufferSize = function() {
    return maxMessagesBuffer;
  };

  // Enable debug messages handling
  var debugEnabled = false;

  var isDebugEnabled = function() {
      return debugEnabled;
  };

  var enableDebugMessages = function(enable) {
    debugEnabled = enable;
  };

  // Messages
  var addMessage = function(severity, title, content, optionalDelay) {
    var delay = optionalDelay || 2500;

    // Delete previous elements if buffer is full
    while (messages.length >= maxMessagesBuffer) {
      messages.shift();
    }

    // Add requested element into the buffer
    var alert = {'severity': severity, 'title': title, 'content': content};
    messages.push(alert);
    $timeout(function() {
      var index = messages.indexOf(alert);
      deleteMessage(index);
    }, delay);
  };

  var addDebugMessage = function(title, content, optionalDelay) {
    if (debugEnabled) {
      addMessage('info', title, content, optionalDelay);
    }

    console.debug(title+': '+content);
  }

  var addSuccessMessage = function(title, content, optionalDelay) {
    addMessage('success', title, content, optionalDelay);
    console.log(title+': '+content);
  };

  var addInfoMessage = function(title, content, optionalDelay) {
    addMessage('info', title, content, optionalDelay);
    console.info(title+': '+content);
  };

  var addWarningMessage = function(title, content, optionalDelay) {
    addMessage('warning', title, content, optionalDelay);
    console.warn(title+': '+content);
  };

  var addErrorMessage = function(title, content, optionalDelay) {
    addMessage('danger', title, content, optionalDelay);
    console.error(title+': '+content);
  };

  var deleteMessage = function(index) {
    if (index > -1) {
      messages.splice(index, 1);
    }
  };

  return {
    messages: messages,
    deleteMessage: deleteMessage,
    setMaxMessagesBufferSize: setMaxMessagesBufferSize,
    getMaxMessagesBufferSize: getMaxMessagesBufferSize,
    isDebugEnabled: isDebugEnabled,
    enableDebugMessages: enableDebugMessages,
    addDebugMessage: addDebugMessage,
    addSuccessMessage: addSuccessMessage,
    addInfoMessage: addInfoMessage,
    addWarningMessage: addWarningMessage,
    addErrorMessage: addErrorMessage
  };
});
