
services.factory('UserService', function(MessagesService) {
  // Language
  var language = "EN";
  var getLanguage = function() {
    return language;
  };
  var setLanguage = function(value) {
    if (language != value) {
      language = value;
      MessagesService.addDebugMessage("UserService", "Language set to " + value);
    }
  };

  // Login
  var login = "";
  var getLogin = function() {
    return login;
  };
  var setLogin = function(value) {
    if (login != value) {
      login = value;
      MessagesService.addDebugMessage("UserService", "Login set to " + value);
    }
  };

  // Password
  var password = "";
  var getPassword = function() {
    return password;
  };
  var setPassword = function(value) {
    if (password != value) {
      password = value;
      MessagesService.addDebugMessage("UserService", "Password set to "+password);
    }
  };

  // Is connected
  var is_connected = false;
  var getIsConnected = function() {
    return is_connected;
  };
  var setIsConnected = function(value) {
    if (is_connected != value) {
      is_connected = value;
      // If not connected, the associated ID should be deleted
      if (!is_connected) {
        setId(-1);
      }
      MessagesService.addDebugMessage("UserService", "Connection state set to "+value);
    }
  };

  // User ID
  var id = null;
  var getId = function() {
    return id;
  };
  var setId = function(value) {
    if (id != value) {
      id = value;
      MessagesService.addDebugMessage("UserService", "User id set to "+value);
    }
  };

  // Email
  var email = "";
  var getEmail = function() {
    return email;
  };
  var setEmail = function(value) {
    if (email != value) {
      email = value;
      MessagesService.addDebugMessage("UserService", "Email set to "+value);
    }
  };

  // Accept newsletters
  var accept_newsletter = false;
  var getAcceptNewsletter = function() {
    return accept_newsletter;
  };
  var setAcceptNewsletter = function(value) {
    if (accept_newsletter != value) {
      accept_newsletter = value;
      MessagesService.addDebugMessage("UserService", "Accept newsletter set to "+value);
    }
  };

  // Referral ID
  var referral_id = -1;
  var getReferralId = function() {
    return referral_id;
  };
  var setReferralId = function(value) {
    if (referral_id != value) {
      referral_id = value;
      MessagesService.addDebugMessage("UserService", "Referral ID set to "+value);
    }
  };

  // Referral name
  var referral_name = "";
  var getReferralName = function() {
    return referral_name;
  };
  var setReferralName = function(value) {
    if (referral_name != value) {
      referral_name = value;
      MessagesService.addDebugMessage("UserService", "Referral name set to "+value);
    }
  };

  // Gender ID
  var gender_id = 1;
  var getGenderId = function() {
    return gender_id;
  };
  var setGenderId = function(value) {
    if (gender_id != value) {
      gender_id = value;
      MessagesService.addDebugMessage("UserService", "Gender ID set to "+value);
    }
  };

  // Description
  var description = "";
  var getDescription = function() {
    return description;
  };
  var setDescription = function(value) {
    if (description != value) {
      description = value;
      MessagesService.addDebugMessage("UserService", "Description set to "+value);
    }
  };

  // Accept PVP fights
  var accept_pvp_fights = true;
  var getAcceptPvpFights = function() {
    return accept_pvp_fights;
  };
  var setAcceptPvpFights = function(value) {
    if (accept_pvp_fights != value) {
      accept_pvp_fights = value;
      MessagesService.addDebugMessage("UserService", "Accept PVP fights set to "+value);
    }
  };

  // Automatic evolution
  var automatic_evolution = true;
  var getAutomaticEvolution = function() {
    return automatic_evolution;
  };
  var setAutomaticEvolution = function(value) {
    if (automatic_evolution != value) {
      automatic_evolution = value;
      MessagesService.addDebugMessage("UserService", "Automatic evolution set to "+value);
    }
  };

  // Number of unseen news
  var unseen_news = 0;
  var getNumberOfUnseenNews = function() {
    return unseen_news;
  };
  var setNumberOfUnseenNews = function(value) {
    if (unseen_news != value) {
      unseen_news = value;
      MessagesService.addDebugMessage("UserService", "Number of unseen news set to "+value);
    }
  };

  // Number of days connected in a row
  var number_days_connected_in_a_row = 0;
  var getNumberDaysConnectedInARow = function() {
    return number_days_connected_in_a_row;
  };
  var setNumberDaysConnectedInARow = function(value) {
    if (number_days_connected_in_a_row != value) {
      number_days_connected_in_a_row = value;
      MessagesService.addDebugMessage("UserService", "Number of days connected in a row set to "+value);
    }
  };

  // Show tips
  var show_tips = 0;
  var getShowTips = function() {
    return show_tips;
  };
  var setShowTips = function(value) {
    if (show_tips != value) {
      show_tips = value;
      MessagesService.addDebugMessage("UserService", "Show tips set to "+value);
    }
  };

  // Spent gold
  var spent_gold = 0;
  var getSpentGold = function() {
    return spent_gold;
  };
  var setSpentGold = function(value) {
    if (spent_gold != value) {
      spent_gold = value;
      MessagesService.addDebugMessage("UserService", "Spent gold set to "+value);
    }
  };

  // Spent pokedollar
  var spent_pokedollar = 0;
  var getSpentPokedollar = function() {
    return spent_pokedollar;
  };
  var setSpentPokedollar = function(value) {
    if (spent_pokedollar != value) {
      spent_pokedollar = value;
      MessagesService.addDebugMessage("UserService", "Spent pokedollar set to "+value);
    }
  };

  // Spent real euros
  var spent_real_euros = 0;
  var getSpentRealEuros = function() {
    return spent_real_euros;
  };
  var setSpentRealEuros = function(value) {
    if (spent_real_euros != value) {
      spent_real_euros = value;
      MessagesService.addDebugMessage("UserService", "Spent real euros set to "+value);
    }
  };

  // Registration date
  var registration_date = "";
  var getRegistrationDate = function() {
    return registration_date;
  };
  var setRegistrationDate = function(value) {
    if (registration_date != value) {
      registration_date = value;
      MessagesService.addDebugMessage("UserService", "Registration date set to "+value);
    }
  };

  // Pokedollar
  var pokedollar = 0;
  var getPokedollar = function() {
    return pokedollar;
  };
  var setPokedollar = function(value) {
    if (pokedollar != value) {
      pokedollar = value;
      MessagesService.addDebugMessage("UserService", "Pokedollar set to "+value);
    }
  };

  // Gold
  var gold = 0;
  var getGold = function() {
    return gold;
  };
  var setGold = function(value) {
    if (gold != value) {
      gold = value;
      MessagesService.addDebugMessage("UserService", "Gold set to "+value);
    }
  };

  // Storage size
  var storage_size = 0;
  var getStorageSize = function() {
    return storage_size;
  };
  var setStorageSize = function(value) {
    if (storage_size != value) {
      storage_size = value;
      MessagesService.addDebugMessage("UserService", "Storage size set to "+value);
    }
  };

  // Rank
  var rank = 0;
  var getRank = function() {
    return rank;
  };
  var setRank = function(value) {
    if (rank != value) {
      rank = value;
      MessagesService.addDebugMessage("UserService", "Rank set to "+value);
    }
  };

  // Score
  var score = 0;
  var getScore = function() {
    return score;
  };
  var setScore = function(value) {
    if (score != value) {
      score = value;
      MessagesService.addDebugMessage("UserService", "Score set to "+value);
    }
  };

  // PVP Score
  var pvp_score = 0;
  var getPvpScore = function() {
    return pvp_score;
  };
  var setPvpScore = function(value) {
    if (pvp_score != value) {
      pvp_score = value;
      MessagesService.addDebugMessage("UserService", "PVP score set to "+value);
    }
  };

  // Lottery tickets
  var lottery_tickets = 0;
  var getLotteryTickets = function() {
    return lottery_tickets;
  };
  var setLotteryTickets = function(value) {
    if (lottery_tickets != value) {
      lottery_tickets = value;
      MessagesService.addDebugMessage("UserService", "Lottery tickets set to "+value);
    }
  };

  // Hunting competition score
  var hunting_competition_score = 0;
  var getHuntingCompetitionScore = function() {
    return hunting_competition_score;
  };
  var setHuntingCompetitionScore = function(value) {
    if (hunting_competition_score != value) {
      hunting_competition_score = value;
      MessagesService.addDebugMessage("UserService", "Hunting competition score set to "+value);
    }
  };

  // Number of owned pokemons
  var number_owned_pokemons = 0;
  var getNumberOwnedPokemons = function() {
    return number_owned_pokemons;
  };
  var setNumberOwnedPokemons = function(value) {
    if (number_owned_pokemons != value) {
      number_owned_pokemons = value;
      MessagesService.addDebugMessage("UserService", "Number of owned pokemons set to "+value);
    }
  };

  // Private notes
  var notes = "";
  var getNotes = function() {
    return notes;
  };
  var setNotes = function(value) {
    if (notes != value) {
      notes = value;
      MessagesService.addDebugMessage("UserService", "Notes set to "+value);
    }
  };

  // Notifications count
  var notifications_count = 0;
  var getNotificationCount = function() {
    return notifications_count;
  };
  var setNotificationCount = function(value) {
    if (notifications_count != value) {
      notifications_count = value;
      MessagesService.addDebugMessage("UserService", "Notifications count set to "+value);
    }
  };

  // Return all getter/setter functions
  return {
    getLanguage: getLanguage,
    setLanguage: setLanguage,
    getLogin: getLogin,
    setLogin: setLogin,
    getPassword: getPassword,
    setPassword: setPassword,
    getIsConnected: getIsConnected,
    setIsConnected: setIsConnected,
    getId: getId,
    setId: setId,
    getEmail: getEmail,
    setEmail: setEmail,
    getAcceptNewsletter: getAcceptNewsletter,
    setAcceptNewsletter: setAcceptNewsletter,
    getReferralId: getReferralId,
    setReferralId: setReferralId,
    getReferralName: getReferralName,
    setReferralName: setReferralName,
    getGenderId: getGenderId,
    setGenderId: setGenderId,
    getDescription: getDescription,
    setDescription: setDescription,
    getAcceptPvpFights: getAcceptPvpFights,
    setAcceptPvpFights: setAcceptPvpFights,
    getAutomaticEvolution: getAutomaticEvolution,
    setAutomaticEvolution: setAutomaticEvolution,
    getNumberDaysConnectedInARow: getNumberDaysConnectedInARow,
    setNumberDaysConnectedInARow: setNumberDaysConnectedInARow,
    getShowTips: getShowTips,
    setShowTips: setShowTips,
    getSpentGold: getSpentGold,
    setSpentGold: setSpentGold,
    getSpentPokedollar: getSpentPokedollar,
    setSpentPokedollar: setSpentPokedollar,
    getSpentRealEuros: getSpentRealEuros,
    setSpentRealEuros: setSpentRealEuros,
    getRegistrationDate: getRegistrationDate,
    setRegistrationDate: setRegistrationDate,
    getPokedollar: getPokedollar,
    setPokedollar: setPokedollar,
    getGold: getGold,
    setGold: setGold,
    getStorageSize: getStorageSize,
    setStorageSize: setStorageSize,
    getScore: getScore,
    setScore: setScore,
    getRank: getRank,
    setRank: setRank,
    getPvpScore: getPvpScore,
    setPvpScore: setPvpScore,
    getLotteryTickets: getLotteryTickets,
    setLotteryTickets: setLotteryTickets,
    getHuntingCompetitionScore: getHuntingCompetitionScore,
    setHuntingCompetitionScore: setHuntingCompetitionScore,
    getNumberOwnedPokemons: getNumberOwnedPokemons,
    setNumberOwnedPokemons: setNumberOwnedPokemons,
    getNotes: getNotes,
    setNotes: setNotes,
    getNotificationCount: getNotificationCount,
    setNotificationCount: setNotificationCount,
    getNumberOfUnseenNews: getNumberOfUnseenNews,
    setNumberOfUnseenNews: setNumberOfUnseenNews
  };
});
