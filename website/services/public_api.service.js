services.factory('PublicApiService', function($http, MessagesService) {
  var base_url = "https://private.pokemon-origins.com/api/";

  // TODO: Delete this line after implementation (used only for local<->API compatibility)
  $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

  // Get all available news in a specific language
  var getAllNews = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all news in " + language);
    return $http.get(base_url + "news/" + language);
  };

  // Get news in a specific language
  var getNews = function(language, number_of_news) {
    MessagesService.addDebugMessage("PublicApiService", "Getting " + number_of_news + " news in " + language);
    return $http.get(base_url + "news/" + language + "/" + number_of_news);
  };

  // Get the current API version
  var getApiVersion = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting API version");
    return $http.get(base_url + "version");
  };

  // Get the number of registered users
  var getNumberOfRegisteredUsers = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting the number of registered users");
    return $http.get(base_url + "users/registered");
  };

  // Get the number of connected users
  var getNumberOfConnectedUsers = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting the number of connected users");
    return $http.get(base_url + "users/connected");
  };

  // Get all pokemons to hunt for the hunting competition in a specific language
  var getHuntingCompetitionPokemons = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all pokemons for the hunting competition in " + language);
    return $http.get(base_url + "huntingCompetition/pokemons/" + language);
  };

  // Set all news as seen
  var setAllNewsAsSeen = function() {
    MessagesService.addDebugMessage("PublicApiService", "Set all news as seen");
    return $http.post(base_url + "user/seenNews");
  };

  // Search users
  var searchUsers = function(base) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all users begining with " + base);
    return $http.get(base_url + "users/search/" + base);
  };

  // Get public user information
  var getPublicUserInformation = function(language, id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting public user information of " + id + " in " + language);
    return $http.get(base_url + "user/public/base/" + language + "/" + id);
  };

  // Get public user owned pokemons
  var getPublicUserOwnedPokemons = function(language, id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting public owned pokemons of " + id + " in " + language);
    return $http.get(base_url + "user/public/ownedPokemons/" + language + "/" + id);
  };

  // Get user badges
  var getUserBadges = function(language, id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting badges of " + id + " in " + language);
    return $http.get(base_url + "user/public/badges/" + language + "/" + id);
  };

  // Get user grades
  var getUserGrades = function(language, id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting grades of " + id + " in " + language);
    return $http.get(base_url + "user/public/grades/" + language + "/" + id);
  };

  // Get user achievements
  var getPublicUserAchievements = function(language, id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting achievements of " + id + " in " + language);
    return $http.get(base_url + "user/public/achievements/" + language + "/" + id);
  };

  // Get all attacks in a specific language
  var getAllAttacks = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all attacks in " + language);
    return $http.get(base_url + "attacks/all/" + language);
  };

  // Get all pokemons learning an attack (with a specific method) in a specific language
  var getPokemonsLearningAttack = function(language, attack_id, method_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all pokemon learning attack " + attack_id + " with method " + method_id + " in " + language);
    return $http.get(base_url + "attack/pokemonLearning/" + language + "/" + attack_id + "/" + method_id);
  };

  // Get pokemon base info in a specific language
  var getAttack = function(language, attack_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting attack " + attack_id + " info in " + language);
    return $http.get(base_url + "attack/" + language + "/" + attack_id);
  };

  // Get all pokemons in a specific language
  var getAllPokemons = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Getting all pokemons in " + language);
    return $http.get(base_url + "pokemons/all/" + language);
  };

  // Get pokemon base info in a specific language
  var getPokemon = function(language, pokemon_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting pokemon " + pokemon_id + " info in " + language);
    return $http.get(base_url + "pokemon/" + language + "/" + pokemon_id);
  };

  // Get pokemon learnable attacks with item in a specific language
  var getPokemonLearnableAttacksWithItem = function(language, pokemon_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting learnable attacks with items for pokemon " + pokemon_id + " in " + language);
    return $http.get(base_url + "attacks/learnableWithItem/" + language + "/" + pokemon_id);
  };

  // Get pokemon learnable attacks with level up in a specific language
  var getPokemonLearnableAttacksWithLevelUp = function(language, pokemon_id, max_level) {
    MessagesService.addDebugMessage("PublicApiService", "Getting learnable attacks by level up for pokemon " + pokemon_id + " in " + language);
    return $http.get(base_url + "attacks/learnableWithLevelUp/" + language + "/" + pokemon_id + "/" + max_level);
  };

  // Get number of different captured pokemons
  var getNumberOfDifferentCapturedPokemons = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting the number of different captured pokemons");
    return $http.get(base_url + "pokemons/differentCaptured");
  };

  // Get pokemon evolutions
  var getPokemonEvolutions = function(language, pokemon_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting the evolutions of pokemon " + pokemon_id + " in " + language);
    return $http.get(base_url + "pokemon/evolutions/" + language + "/" + pokemon_id);
  };

  // Get parent of a pokemon
  var getParentPokemon = function(language, pokemon_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting the parent of pokemon " + pokemon_id + " in " + language);
    return $http.get(base_url + "pokemon/parent/" + language + "/" + pokemon_id);
  };

  // Get pokemon type information in specific language
  var getType = function(language, type_id) {
    MessagesService.addDebugMessage("PublicApiService", "Getting type " + type_id + " info in " + language);
    return $http.get(base_url + "types/" + language + "/" + type_id);
  };

  // Check if user is connected
  var isConnected = function() {
    MessagesService.addDebugMessage("PublicApiService", "Checking if currently connected");
    return $http.get(base_url + "user/isConnected");
  };

  // Connect
  var connect = function(login, password) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to connect with login '"+login+"' and password '"+password+"'");
    var credentials = {
      "login": login,
      "password": password
    };
    return $http.post(base_url + "user/connect", credentials);
  };

  // Update user login
  var setUserLogin = function(login) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user login to '"+login+"'");
    var query_message = {
      "login": login
    };
    return $http.post(base_url + "user/login", query_message);
  };

  // Update user language
  var setUserLanguage = function(language_code) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user language to '"+language_code+"'");
    var query_message = {
      "language_code": language_code
    };
    return $http.post(base_url + "user/language", query_message);
  };

  // Update user "accept newsletter" flag
  var setUserAcceptNewsletter = function(accept_newsletter) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user newsletter status to '"+accept_newsletter+"'");
    var query_message = {
      "accept_newsletter": accept_newsletter
    };
    return $http.post(base_url + "user/acceptNewsletter", query_message);
  };

  // Update user gender
  var setUserGender = function(gender_id) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user gender to '"+gender_id+"'");
    var query_message = {
      "gender_id": gender_id
    };
    return $http.post(base_url + "user/gender", query_message);
  };

  // Update user description
  var setUserDescription = function(description) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user description to '"+description+"'");
    var query_message = {
      "description": description
    };
    return $http.post(base_url + "user/description", query_message);
  };

  // Update user notes
  var setUserNotes = function(notes) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user notes to '"+notes+"'");
    var query_message = {
      "notes": notes
    };
    return $http.post(base_url + "user/notes", query_message);
  };

  // Update user avatar
  var setUserAvatar = function(avatar) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user avatar to '"+avatar+"'");
    var query_message = {
      "avatar": avatar
    };
    return $http.post(base_url + "user/avatar", query_message);
  };

  // Update user password
  var setUserPassword = function(password) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user password to '"+password+"'");
    var query_message = {
      "password": password
    };
    return $http.post(base_url + "user/password", query_message);
  };

  // Update user accept PVP fights
  var setUserAcceptPvpFights = function(accept_pvp_fights) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user accept PVP fights to '"+accept_pvp_fights+"'");
    var query_message = {
      "accept_pvp_fights": accept_pvp_fights
    };
    return $http.post(base_url + "user/acceptPvpFights", query_message);
  };

  // Update user automatic evolution
  var setUserAutomaticEvolution = function(automatic_evolution) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user automatic evolution to '"+automatic_evolution+"'");
    var query_message = {
      "automatic_evolution": automatic_evolution
    };
    return $http.post(base_url + "user/automaticEvolution", query_message);
  };

  // Update user show tips
  var setUserShowTips = function(show_tips) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to update the user show tips to '"+show_tips+"'");
    var query_message = {
      "show_tips": show_tips
    };
    return $http.post(base_url + "user/showTips", query_message);
  };

  // Get user static informations
  var getUserStaticInformation = function() {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user static informations");
    return $http.get(base_url + "user/staticInformation");
  };

  // Get user base informations
  var getUserBaseInformation = function() {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user base informations");
    return $http.get(base_url + "user/baseInformation");
  };

  // Get user recommendees
  var getUserRecommendees = function() {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user recommendees");
    return $http.get(base_url + "user/recommendees");
  };

  // Get user items
  var getUserItems = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user items");
    return $http.get(base_url + "user/items/" + language);
  };

  // Get item pockets
  var getItemPockets = function(language) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get item pockets");
    return $http.get(base_url + "items/pockets/" + language);
  };

  // Get item basic information
  var getItem = function(language, item_id) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get item " + item_id + " basic information");
    return $http.get(base_url + "item/baseData/" + language + "/" + item_id);
  };

  // Get item flags
  var getItemFlags = function(language, item_id) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get item " + item_id + " flags");
    return $http.get(base_url + "item/flags/" + language + "/" + item_id);
  };

  // Get top 50 users
  var getTop50Users = function(type) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get top 50 users with type " + type);
    return $http.get(base_url + "users/rank/top50/" + type);
  };

  // Get users better than current user
  var getUsersBetterThanUser = function(type) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get users better than current user with type " + type);
    return $http.get(base_url + "users/rank/betterThan/" + type);
  };

  // Get users worse than current user
  var getUsersWorseThanUser = function(type) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get users worse than current user with type " + type);
    return $http.get(base_url + "users/rank/worseThan/" + type);
  };

  // Get users worse than current user
  var getUserRank = function(type) {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user rank with type " + type);
    return $http.get(base_url + "user/rank/" + type);
  };

  // Get user notifications
  var getUserNotifications = function() {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user notifications");
    return $http.get(base_url + "user/notifications/all");
  };

  // Get user notification count
  var getUserNotificationCount = function() {
    MessagesService.addDebugMessage("PublicApiService", "Trying to get user notification count");
    return $http.get(base_url + "user/notifications/count");
  };

  // Disconnect
  var disconnect = function() {
    MessagesService.addDebugMessage("PublicApiService", "Disconnecting...");
    return $http.post(base_url + "user/disconnect");
  };

  // Get user gold in sell
  var getUserGoldInSell = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting the user gold in sell");
    return $http.get(base_url + "market/gold/self");
  };

  // Get all gold in sell
  var getAllGoldInSell = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting all gold in sell");
    return $http.get(base_url + "market/gold/all");
  };

  // Update user gold in sell
  var updateUserGoldInSell = function(gold_to_add_or_remove) {
    var query_message = {
      "gold_to_add_or_remove": gold_to_add_or_remove
    };
    MessagesService.addDebugMessage("PublicApiService", "Update user gold in sell");
    return $http.post(base_url + "market/gold/quantity", query_message);
  };

  // Update user gold price
  var updateUserGoldPrice = function(price) {
    var query_message = {
      "price": price
    };
    MessagesService.addDebugMessage("PublicApiService", "Update user gold price");
    return $http.post(base_url + "market/gold/price", query_message);
  };

  // Buy gold
  var buyGoldFromOtherUser = function(seller_id, quantity, requested_gold_price) {
    var query_message = {
      "seller_id": seller_id,
      "requested_gold_price": requested_gold_price,
      "quantity": quantity
    };
    MessagesService.addDebugMessage("PublicApiService", "Update user gold price");
    return $http.post(base_url + "market/gold/buy", query_message);
  };

  // Send a report
  var sendReport = function(reported_user_id, title, message) {
    var query_message = {
      "reported_user_id": reported_user_id,
      "title": title,
      "message": message
    };
    MessagesService.addDebugMessage("PublicApiService", "Sending report");
    return $http.post(base_url + "report", query_message);
  };

  // Send a message to admin
  var sendMessageToAdmin = function(title, message) {
    var query_message = {
      "title": title,
      "message": message
    };
    MessagesService.addDebugMessage("PublicApiService", "Sending a message '"+title+"' to admin");
    return $http.post(base_url + "contact", query_message);
  };

  // Get all blacklisted users
  var getAllBlacklistedUsers = function() {
    MessagesService.addDebugMessage("PublicApiService", "Getting all blacklisted users");
    return $http.get(base_url + "blacklist/all");
  };

  // Check if an user is blacklisted
  var isUserBlacklisted = function(to_id) {
    MessagesService.addDebugMessage("PublicApiService", "Checking if user "+to_id+" is blacklisted");
    return $http.get(base_url + "blacklist/" + to_id);
  };

  // Blacklist an user
  var blackListUser = function(to_id) {
    MessagesService.addDebugMessage("PublicApiService", "Blacklisting user "+to_id);
    return $http.post(base_url + "blacklist/" + to_id);
  };

  // Remove an user from blacklist
  var removeUserFromBlacklist = function(to_id) {
    MessagesService.addDebugMessage("PublicApiService", "Removing user "+to_id+" from blacklist");
    return $http.delete(base_url + "blacklist/" + to_id);
  };

  // Get all achievements
  var getUserAchievements = function() {
    MessagesService.addDebugMessage("PublicApiService", "Get all achievements");
    return $http.get(base_url + "achievements/all");
  };

  // Upgrade achievement
  var upgradeUserAchievement = function(achievement_id) {
    MessagesService.addDebugMessage("PublicApiService", "Upgrade achievement "+achievement_id);
    return $http.post(base_url + "achievements/" + achievement_id);
  };

  // Get all casino rewards
  var getCasinoRewards = function() {
    MessagesService.addDebugMessage("PublicApiService", "Get all casino rewards");
    return $http.get(base_url + "casino/rewards");
  };

  // Play in casino
  var playCasino = function(is_gold_casino) {
    MessagesService.addDebugMessage("PublicApiService", "Play casino (with gold? "+is_gold_casino+")");
    var query_message = {
      "is_gold_casino": is_gold_casino
    };
    return $http.post(base_url + "casino/play", query_message);
  };

  return {
    getAllNews: getAllNews,
    getNews: getNews,
    getApiVersion: getApiVersion,
    getNumberOfRegisteredUsers: getNumberOfRegisteredUsers,
    getNumberOfConnectedUsers: getNumberOfConnectedUsers,
    getAllPokemons: getAllPokemons,
    getPokemonLearnableAttacksWithItem: getPokemonLearnableAttacksWithItem,
    getPokemonLearnableAttacksWithLevelUp: getPokemonLearnableAttacksWithLevelUp,
    getPokemon: getPokemon,
    getHuntingCompetitionPokemons: getHuntingCompetitionPokemons,
    getType: getType,
    isConnected: isConnected,
    connect: connect,
    disconnect: disconnect,
    getUserStaticInformation: getUserStaticInformation,
    getUserBaseInformation: getUserBaseInformation,
    getUserRecommendees: getUserRecommendees,
    setUserLogin: setUserLogin,
    setUserLanguage: setUserLanguage,
    setUserAcceptNewsletter: setUserAcceptNewsletter,
    setUserGender: setUserGender,
    setUserDescription: setUserDescription,
    setUserNotes: setUserNotes,
    setUserAvatar: setUserAvatar,
    setUserPassword: setUserPassword,
    setUserAcceptPvpFights: setUserAcceptPvpFights,
    setUserAutomaticEvolution: setUserAutomaticEvolution,
    setUserShowTips: setUserShowTips,
    getNumberOfDifferentCapturedPokemons: getNumberOfDifferentCapturedPokemons,
    getPokemonEvolutions: getPokemonEvolutions,
    getParentPokemon: getParentPokemon,
    getTop50Users: getTop50Users,
    getUsersWorseThanUser: getUsersWorseThanUser,
    getUsersBetterThanUser: getUsersBetterThanUser,
    getUserRank: getUserRank,
    searchUsers: searchUsers,
    getPublicUserInformation: getPublicUserInformation,
    getPublicUserOwnedPokemons: getPublicUserOwnedPokemons,
    getUserBadges: getUserBadges,
    getUserGrades: getUserGrades,
    getPublicUserAchievements: getPublicUserAchievements,
    getUserItems: getUserItems,
    getItemPockets: getItemPockets,
    getItem: getItem,
    getItemFlags: getItemFlags,
    getUserGoldInSell: getUserGoldInSell,
    getAllGoldInSell: getAllGoldInSell,
    updateUserGoldInSell: updateUserGoldInSell,
    updateUserGoldPrice: updateUserGoldPrice,
    buyGoldFromOtherUser: buyGoldFromOtherUser,
    getUserNotifications: getUserNotifications,
    getUserNotificationCount: getUserNotificationCount,
    setAllNewsAsSeen: setAllNewsAsSeen,
    sendReport: sendReport,
    sendMessageToAdmin: sendMessageToAdmin,
    getAllAttacks: getAllAttacks,
    getAttack: getAttack,
    getPokemonsLearningAttack: getPokemonsLearningAttack,
    getAllBlacklistedUsers: getAllBlacklistedUsers,
    isUserBlacklisted: isUserBlacklisted,
    blackListUser: blackListUser,
    removeUserFromBlacklist: removeUserFromBlacklist,
    getUserAchievements: getUserAchievements,
    upgradeUserAchievement: upgradeUserAchievement,
    getCasinoRewards: getCasinoRewards,
    playCasino: playCasino
  };
});
