services.factory('StaticDataService', function(MessagesService, PublicApiService) {
  // ---- Search user functionnality ----
  var already_searched_users = [];
  var users_list = [];
  var searchUsers = function(base_username) {
    // Do not search user if less than 2 char provided
    if (!base_username || base_username.length < 2) {
      return [];
    }

    // Query usernames only once (if already queried, give the result, else get result from API)
    if (already_searched_users.indexOf(base_username) !== -1) {
      return users_list[base_username];
    } else {
      already_searched_users.push(base_username);

      PublicApiService.searchUsers(base_username).then(function (response) {
        if (response.data) {
          users_list[base_username] = response.data;
        } else {
          MessagesService.addErrorMessage("StaticDataService", $translate.instant("ERROR_SEARCHING_USER"));
        }
      });
    }
  };

  // ---- Get all attacks ----
  var already_requested_attacks = [];
  var all_attacks = [];
  var getAllAttacks = function(language_code) {
    // Query usernames only once (if already queried, give the result, else get result from API)
    if (already_requested_attacks.indexOf(language_code) !== -1) {
      return all_attacks[language_code];
    } else {
      already_requested_attacks.push(language_code);

      PublicApiService.getAllAttacks(language_code).then(function (response) {
        if (response.data) {
          all_attacks[language_code] = response.data;
        } else {
          MessagesService.addErrorMessage("StaticDataService", "Error while getting the attacks");
        }
      });
    }
  };

  // Return all functions
  return {
    searchUsers: searchUsers,
    getAllAttacks: getAllAttacks
  };
});
