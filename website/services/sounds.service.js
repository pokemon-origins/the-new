services.factory('SoundsService', function() {
  // ---- Pokemon cries ----
  var getPokemonCrySrc = function(pokemon_id) {
    return "../ressources/cries/OGG/"+pokemon_id+".ogg";
  };

  // ---- List of all available functions ----
  return {
    getPokemonCrySrc: getPokemonCrySrc
  };
});
