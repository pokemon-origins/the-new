// List of searched users
app.component('searchedUsers', {
  bindings: {
    selectedUser: '='
  },
  controller: function(StaticDataService, SpritesService) {
    var self = this;

    function selectUser(user) {
      self.selectedUser = user;
    }

    self.selectUser = selectUser;
    self.StaticDataService = StaticDataService;
    self.SpritesService = SpritesService;
  },
  templateUrl: "components/searched_users/searched_users.html",
  css: "style/common.css"
});
