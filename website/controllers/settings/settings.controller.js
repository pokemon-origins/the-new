app.controller("SettingsCtrl", function($scope, $translate, $uibModalInstance, PublicApiService, MessagesService, UserService, SpritesService) {
  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Handle messages settings
  $scope.MessagesService = MessagesService;
  $scope.is_debug_enabled = MessagesService.isDebugEnabled();
  $scope.max_messages_buffer_size = MessagesService.getMaxMessagesBufferSize();

  // Handle user settings
  $scope.UserService = UserService;
  $scope.show_tips = UserService.getShowTips();
  $scope.accept_pvp_fights = UserService.getAcceptPvpFights();
  $scope.accept_newsletter = UserService.getAcceptNewsletter();
  $scope.automatic_evolution = UserService.getAutomaticEvolution();
  $scope.password = UserService.getPassword();

  // Store the user elements in the DB
  $scope.$watch('show_tips', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserShowTips(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("SETTINGS"), $translate.instant("SHOW_TIPS_UPDATED"));
          UserService.setShowTips(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("SETTINGS"), $translate.instant("ERROR_SAVING_SHOW_TIPS"));
        }
      });
    }
  });
  $scope.$watch('accept_pvp_fights', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserAcceptPvpFights(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("SETTINGS"), $translate.instant("ACCEPT_PVP_FIGHTS_UPDATED"));
          UserService.setAcceptPvpFights(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("SETTINGS"), $translate.instant("ERROR_SAVING_ACCEPT_PVP_FIGHTS"));
        }
      });
    }
  });
  $scope.$watch('accept_newsletter', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserAcceptNewsletter(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("SETTINGS"), $translate.instant("ACCEPT_NEWSLETTER_UPDATED"));
          UserService.setAcceptNewsletter(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("SETTINGS"), $translate.instant("ERROR_SAVING_ACCEPT_NEWSLETTER"));
        }
      });
    }
  });
  $scope.$watch('automatic_evolution', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserAutomaticEvolution(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("SETTINGS"), $translate.instant("AUTOMATIC_EVOLUTION_UPDATED"));
          UserService.setAutomaticEvolution(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("SETTINGS"), $translate.instant("ERROR_SAVING_AUTOMATIC_EVOLUTION"));
        }
      });
    }
  });
  $scope.$watch('password', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserPassword(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("SETTINGS"), $translate.instant("PASSWORD_UPDATED"));
          UserService.setPassword(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("SETTINGS"), $translate.instant("ERROR_SAVING_PASSWORD"));
        }
      });
    }
  });
});
