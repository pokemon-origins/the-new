app.controller("CasinoCtrl", function($scope, $translate, $timeout, $uibModalInstance, PublicApiService, MessagesService, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
  $scope.reward_description = "";
  $scope.played = false;
  $scope.won = false;

  PublicApiService.getCasinoRewards().then(function (response) {
    $scope.rewards = response.data;
  }, function (response) {
    MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not get casino rewards');
  });

  // Initialize the casino
  angular.element(document).ready(function () {
    var machine1 = $("#casino1").slotMachine({
      active  : 1,
      delay  : 500
    });
    var machine2 = $("#casino2").slotMachine({
      active  : 2,
      delay  : 500
    });
    machine3 = $("#casino3").slotMachine({
      active  : 3,
      delay  : 500
    });

    var getRandomResult = function() {
      return Math.floor(Math.random() * 6) + 1;
    };

    // Lost result generator
    var lostResult1 = 1;
    var lostResult2 = 2;
    var lostResult3 = 3;
    var generateLostResult = function() {
      lostResult1 = getRandomResult();
      lostResult2 = getRandomResult();
      var result3 = getRandomResult();
      while ((result3 == lostResult2) && (result3 == lostResult1)) {
        result3 = getRandomResult();
      }
      lostResult3 = result3;
    };

    // Launch the casino
    $scope.play = function(is_gold_casino) {
      if (!machine1.running && !machine2.running && !machine3.running) {
        $("#slotMachineButtonShuffleGold").attr('disabled','disabled');
        $("#slotMachineButtonShuffleDollar").attr('disabled','disabled');

        PublicApiService.playCasino(is_gold_casino).then(function (response) {
          var result = response.data;

          if (result == false) {
            MessagesService.addErrorMessage($translate.instant("CASINO"), 'You don\'t have enough money or pokemon space');
            $("#slotMachineButtonShuffleGold").removeAttr('disabled');
            $("#slotMachineButtonShuffleDollar").removeAttr('disabled');
            return;
          }

          // Shuffle (visual effect)
          machine1.shuffle();
          machine2.shuffle();
          machine3.shuffle();

          // Show the correct result
          if (result['won'] == false) {
            generateLostResult();
            machine1.setRandomize(lostResult1);
            machine2.setRandomize(lostResult2);
            machine3.setRandomize(lostResult3);
          } else {
            machine1.setRandomize(parseInt(result['slot']));
            machine2.setRandomize(parseInt(result['slot']));
            machine3.setRandomize(parseInt(result['slot']));
          }

          // Stop the slots one by one (visual effect)
          $timeout(function () {
            machine1.stop();
            $timeout(function () {
              machine2.stop();
              $timeout(function () {
                machine3.stop();
                $timeout(function () {
                  $("#slotMachineButtonShuffleGold").removeAttr('disabled');
                  $("#slotMachineButtonShuffleDollar").removeAttr('disabled');

                  $scope.played = true;
                  $scope.won = result['won'];
                  $scope.reward_description = result['reward_description'];
                }, 1000);
              }, 500);
            }, 500);
          }, 1000);
        }, function (response) {
          MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not play casino');
        });
      }
    };
  });
});
