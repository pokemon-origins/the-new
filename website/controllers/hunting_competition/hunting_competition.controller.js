app.controller("HuntingCompetitionCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Open hunting rank modal if requested
  $scope.openHuntingRank = function() {
    $uibModal.open({
      templateUrl: 'controllers/rank/rank.html',
      controller: 'RankCtrl',
      size: 'lg',
      resolve: {
        rankType: function () {
          return "hunting_score";
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("HuntingCompetitionCtrl", 'Rank modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("HuntingCompetitionCtrl", 'Rank modal closed with cancel');
    });
  };

  $scope.openPokedex = function(pokemon_id) {
    $uibModal.open({
      templateUrl: 'controllers/pokedex/pokedex.html',
      controller: 'PokedexCtrl',
      size: 'lg',
      resolve: {
        pokemonId: function () {
          return pokemon_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("HuntingCompetitionCtrl", 'Pokedex modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("HuntingCompetitionCtrl", 'Pokedex modal closed with cancel');
    });
  };

  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  PublicApiService.getHuntingCompetitionPokemons(UserService.getLanguage()).then(function (response) {
    if (response.data) {
      $scope.pokemons_to_hunt = response.data;
    } else {
      MessagesService.addWarningMessage($translate.instant("HUNTING_COMPETITION"), $translate.instant("ERROR_GETTING_POKEMONS_TO_HUNT"));
    }
  });
});
