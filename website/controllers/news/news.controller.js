app.controller("NewsCtrl", function($scope, $translate, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService) {
  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Show unseen news (or at least one)
  PublicApiService.getNews(UserService.getLanguage(),
                           UserService.getNumberOfUnseenNews() > 0 ?
                             UserService.getNumberOfUnseenNews() :
                             1
                          ).then(function (response) {
    $scope.news = response.data;
    if (UserService.getNumberOfUnseenNews() > 0) {
      PublicApiService.setAllNewsAsSeen().then(function (response) {
        UserService.setNumberOfUnseenNews(0);
      }, function (response) {
        MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not set all news as seen');
      });
    }
  }, function (response) {
    MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve the news');
  });

  $scope.allNewsLoaded = false;
  $scope.showAllNews = function() {
    PublicApiService.getAllNews(UserService.getLanguage()).then(function (response) {
      $scope.news = response.data;
      $scope.allNewsLoaded = true;
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve the news');
    });
  };
});
