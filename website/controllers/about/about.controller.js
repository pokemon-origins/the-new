app.controller("AboutCtrl", function($scope, $translate, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService) {
  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.contact_title = "";
  $scope.contact_message = "";
  $scope.sendMessageToAdmin = function() {
    if ($scope.contact_title != "" && $scope.contact_message != "")  {
      PublicApiService.sendMessageToAdmin($scope.contact_title, $scope.contact_message).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("ABOUT"), $translate.instant("MESSAGE_SENT"));
          $scope.contact_title = "";
          $scope.contact_message = "";
        } else {
          MessagesService.addWarningMessage($translate.instant("ABOUT"), 'Could not send message to admin');
        }
      }, function (response) {
        MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not send message to admin');
      });
    }
  };

  PublicApiService.getApiVersion().then(function (response) {
    $scope.version = response.data['version'];
  }, function (response) {
    MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve the API version');
  });

  PublicApiService.getNumberOfRegisteredUsers().then(function (response) {
    $scope.nb_of_registered_users = response.data;
  }, function (response) {
    MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve the number of registered users');
  });

  PublicApiService.getNumberOfConnectedUsers().then(function (response) {
    $scope.nb_of_connected_users = response.data;
  }, function (response) {
    MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve the number of connected users');
  });
});
