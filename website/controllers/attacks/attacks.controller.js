app.controller("AttacksCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, StaticDataService, PublicApiService, SpritesService, MessagesService, attackId) {
  $scope.full_attacks = (attackId <= 0);

  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Show current attack when requested
  $scope.showAttack = function(id) {
    PublicApiService.getAttack(UserService.getLanguage(), id).then(function (response) {
      $scope.current_attack = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested attack information');
    });

    PublicApiService.getPokemonsLearningAttack(UserService.getLanguage(), id, 1).then(function (response) {
      $scope.pokemons_level_up = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested pokemons learning attack with level up');
    });

    PublicApiService.getPokemonsLearningAttack(UserService.getLanguage(), id, 4).then(function (response) {
      $scope.pokemons_item = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested pokemons learning attack with item');
    });
  };

  // Show an attack directly if requested in the instantiation of the controller (else show the full attack list)
  if (attackId > 0) {
    $scope.showAttack(attackId);
  }

  $scope.attacks = function() {
    return StaticDataService.getAllAttacks(UserService.getLanguage());
  }

  // Open pokedex if requested
  $scope.openPokedex = function(pokemon_id) {
    $uibModal.open({
      templateUrl: 'controllers/pokedex/pokedex.html',
      controller: 'PokedexCtrl',
      size: 'lg',
      resolve: {
        pokemonId: function () {
          return pokemon_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("AttacksCtrl", 'Pokedex modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("AttacksCtrl", 'Pokedex modal closed with cancel');
    });
  };
});
