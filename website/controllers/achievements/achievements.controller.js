app.controller("AchievementsCtrl", function($scope, $translate, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;

  $scope.achievements = [];
  $scope.getUserAchievements = function() {
    PublicApiService.getUserAchievements().then(function (response) {
      $scope.achievements = response.data;
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not get achievements');
    });
  };
  $scope.getUserAchievements();

  $scope.compareAchievementSteps = function(current, next) {
    return parseInt(current) >= parseInt(next);
  };
  $scope.upgradeAchievement = function(achievement_id) {
    PublicApiService.upgradeUserAchievement(achievement_id).then(function (response) {
      if (response.data) {
        MessagesService.addSuccessMessage($translate.instant("ACHIEVEMENTS"), $translate.instant("REWARD_RECEIVED"));
        $scope.getUserAchievements();
      } else {
        MessagesService.addWarningMessage($translate.instant("ACHIEVEMENTS"), 'Could not upgrade the achievement');
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not upgrade the achievement');
    });
  };
});
