app.controller("ReportCtrl", function($scope, $translate, $uibModalInstance, PublicApiService, MessagesService, SpritesService, userId, userName) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  if (userId <= 0 || userName == "")
  {
    $scope.close();
  }

  $scope.SpritesService = SpritesService;

  $scope.user_id = userId;
  $scope.user_name = userName;

  $scope.report_title = "";
  $scope.report_message = "";
  $scope.sendReport = function() {
    if ($scope.report_message != "" && $scope.report_title != "") {
      PublicApiService.sendReport($scope.user_id, $scope.report_title, $scope.report_message).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("REPORT"), $translate.instant("REPORT_SENT"));
          $scope.report_title = "";
          $scope.report_message = "";
        } else {
          MessagesService.addWarningMessage($translate.instant("REPORT"), 'Could not send the report');
        }
      }, function (response) {
        MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not send the report');
      });
    }
  };
});

app.controller("PublicProfileCtrl", function($scope, $translate, $uibModalInstance, $uibModal, UserService, PublicApiService, MessagesService, SpritesService, user_id) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  // Open private profile editor modal if requested
  $scope.openProfileEditor = function() {
    $uibModal.open({
      templateUrl: 'controllers/profile/profile.html',
      controller: 'ProfileCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Private profile modal closed with ok');
      $scope.showPublicProfile(UserService.getId());
    }, function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Private profile modal closed with cancel');
      $scope.showPublicProfile(UserService.getId());
    });
  };

  $scope.openPokedex = function(pokemon_id) {
    $uibModal.open({
      templateUrl: 'controllers/pokedex/pokedex.html',
      controller: 'PokedexCtrl',
      size: 'lg',
      resolve: {
        pokemonId: function () {
          return pokemon_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Pokedex modal closed with ok');

    }, function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Pokedex modal closed with cancel');
    });
  };

  // Open rank modal if requested
  $scope.openRank = function(rank_type) {
    $uibModal.open({
      templateUrl: 'controllers/rank/rank.html',
      controller: 'RankCtrl',
      size: 'lg',
      resolve: {
        rankType: function () {
          return rank_type;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Rank modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Rank modal closed with cancel');
    });
  };

  // Open report user modal if requested
  $scope.openReportUser = function(user_id, user_name) {
    $uibModal.open({
      templateUrl: 'controllers/public_profile/report.html',
      controller: 'ReportCtrl',
      size: 'lg',
      resolve: {
        userId: function () {
          return user_id;
        },
        userName: function () {
          return user_name;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Report modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("PublicProfileCtrl", 'Report modal closed with cancel');
    });
  };
  $scope.showPublicProfile = function(id) {
    $scope.currentUserId = id;

    // Get the base user information
    PublicApiService.getPublicUserInformation(UserService.getLanguage(), id).then(function (response) {
      $scope.user_base = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the public user information');
    });
    // Get the owned pokemons of the user
    PublicApiService.getPublicUserOwnedPokemons(UserService.getLanguage(), id).then(function (response) {
      $scope.user_pokemons = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the user pokemons');
    });

    // Get the owned badges of the user
    PublicApiService.getUserBadges(UserService.getLanguage(), id).then(function (response) {
      $scope.user_badges = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the user badges');
    });
    // Get the owned grades of the user
    PublicApiService.getUserGrades(UserService.getLanguage(), id).then(function (response) {
      $scope.user_grades = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the user grades');
    });
    // Get the owned achievements of the user
    PublicApiService.getPublicUserAchievements(UserService.getLanguage(), id).then(function (response) {
      $scope.user_achievements = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the user achievements');
    });
    // Check if user is blacklisted
    if (UserService.getId() != id) {
      PublicApiService.isUserBlacklisted(id).then(function (response) {
        $scope.is_blacklisted = response.data;
      }, function (response) {
        MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not check if user is blacklisted');
      });
    }
  };

  $scope.removeUserFromBlacklist = function() {
    PublicApiService.removeUserFromBlacklist($scope.currentUserId).then(function (response) {
      $scope.is_blacklisted = !(response.data);
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not remove user from blacklist');
    });
  };

  $scope.blacklistUser = function() {
    PublicApiService.blackListUser($scope.currentUserId).then(function (response) {
      $scope.is_blacklisted = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not blacklist user');
    });
  };

  if (user_id > 0) {
    $scope.showPublicProfile(user_id);
  }

  // Allow user to select an user with search box
  $scope.selected_user = user_id;
  $scope.$watch('selected_user', function(val) {
    if (val != $scope.currentUserId)
    {
      $scope.showPublicProfile(val);
    }
  });
});
