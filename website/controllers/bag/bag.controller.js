app.controller("BagCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, PublicApiService, SpritesService, MessagesService) {
  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Get static item pockets information
  PublicApiService.getItemPockets(UserService.getLanguage()).then(function (response) {
    $scope.pockets = response.data;
  }, function (response) {
    MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve item pockets');
  });

  // Show items
  $scope.showItems = function() {
    PublicApiService.getUserItems(UserService.getLanguage()).then(function (response) {
      $scope.items = response.data;
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not retrieve user items');
    });
  };
  $scope.showItems();

  // Open item modal if requested
  $scope.openItem = function(item_id) {
    $uibModal.open({
      templateUrl: 'controllers/item/item.html',
      controller: 'ItemCtrl',
      size: 'lg',
      resolve: {
        itemId: function () {
          return item_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("BagCtrl", 'Item modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("BagCtrl", 'Item modal closed with cancel');
    });
  };
});
