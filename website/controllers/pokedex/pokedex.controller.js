app.controller("PokedexCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, PublicApiService, SpritesService, SoundsService, MessagesService, pokemonId) {
  $scope.full_pokedex = (pokemonId <= 0);

  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Show current pokemon when requested
  $scope.showPokemon = function(id) {
    PublicApiService.getPokemon(UserService.getLanguage(), id).then(function (response) {
      $scope.current_pokemon = response.data;

      // Query types of the pokemon
      if ($scope.current_pokemon.type_1_id != null) {
        PublicApiService.getType(UserService.getLanguage(), $scope.current_pokemon.type_1_id).then(function (response) {
          $scope.current_type_1 = response.data;
        }, function (response) {
          MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested pokemon type 1 information');
        });
      } else {
        $scope.current_type_1 = null;
      }

      if ($scope.current_pokemon.type_2_id != null) {
        PublicApiService.getType(UserService.getLanguage(), $scope.current_pokemon.type_2_id).then(function (response) {
          $scope.current_type_2 = response.data;
        }, function (response) {
          MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested pokemon type 2 information');
        });
      } else {
        $scope.current_type_2 = null;
      }
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve requested pokemon information');
    });

    // Get learnable attacks with level up
    PublicApiService.getPokemonLearnableAttacksWithLevelUp(UserService.getLanguage(), id, 100).then(function (response) {
      $scope.learnable_attacks_with_level_up = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve attacks (level up) for the pokemon');
    });

    // Get learnable attacks with items
    PublicApiService.getPokemonLearnableAttacksWithItem(UserService.getLanguage(), id).then(function (response) {
      $scope.learnable_attacks_with_item = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve attacks (items) for the pokemon');
    });

    // Get evolutions of the pokemon
    PublicApiService.getPokemonEvolutions(UserService.getLanguage(), id).then(function (response) {
      $scope.evolution_pokemons = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve evolutions for the pokemon');
    });

    // Get parent of the pokemon
    /*
    PublicApiService.getParentPokemon(UserService.getLanguage(), id).then(function (response) {
      $scope.parent_pokemon = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve parent of the pokemon');
    });
    */
  };

  // Show a pokemon directly if requested in the instantiation of the controller (else show the full pokedex)
  if (pokemonId > 0) {
    $scope.showPokemon(pokemonId);
  } else {
    // Get all pokemons
    PublicApiService.getAllPokemons(UserService.getLanguage()).then(function (response) {
      $scope.pokemons = response.data;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve pokemons');
    });

    // Get the number of different captured pokemons
    PublicApiService.getNumberOfDifferentCapturedPokemons().then(function (response) {
      $scope.different_normal = response.data.different_normal;
      $scope.different_shiny = response.data.different_shiny;
    }, function (response) {
      MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not retrieve the number of different captured pokemons');
    });
  }

  // Function to check if the pokemon is just before the current pokemon
  $scope.isPreviousPokemon = function(pokemon) {
    return parseInt($scope.current_pokemon.id) == (parseInt(pokemon.id) + 1);
  };

  // Function to check if the pokemon is just after the current pokemon
  $scope.isNextPokemon = function(pokemon) {
    return parseInt($scope.current_pokemon.id) == (parseInt(pokemon.id) - 1);
  };

  // Import sound getters needed in the view
  $scope.getPokemonCrySrc = SoundsService.getPokemonCrySrc

  $scope.getReadableRequirementForEvolution = function(pokemon) {
    var result = "";
    if (pokemon.minimum_level > 0) {
      result += $translate.instant('HAVE_LEVEL') + " " + pokemon.minimum_level + " ";
    }
    if (pokemon.minimum_hapiness > 0) {
      result += $translate.instant("HAVE_HAPINESS") + " " + pokemon.minimum_hapiness + " ";
    }
    if (pokemon.need_trade > 0) {
      result += $translate.instant("TRADE") + " ";
    }
    if (pokemon.need_time_of_day > 0) {
      result += $translate.instant("SPECIFIC_TIME_OF_DAY") + " " + pokemon.time_of_day_name + " (" +pokemon.time_of_day_description + ")";
    }
    if (pokemon.required_item_id > 0) {
      result += $translate.instant("NEED_ITEM") + " " + pokemon.required_item_name + " ";
    }
    if (pokemon.required_held_item_id > 0) {
      result += $translate.instant("NEED_HELD_ITEM") + " " + pokemon.required_held_item_name + " ";
    }

    return result;
  };

  $scope.openAttack = function(id) {
    $uibModal.open({
      templateUrl: 'controllers/attacks/attacks.html',
      controller: 'AttacksCtrl',
      size: 'lg',
      resolve: {
        attackId: function () {
          return id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("PokedexCtrl", 'Attacks modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("PokedexCtrl", 'Attacks modal closed with cancel');
    });
  };

  // Import image getters needed in the view
  $scope.base32x32Src = SpritesService.getBase32x32Src();
  $scope.getStaticPokemonSprite32x32Style = SpritesService.getStaticPokemonSprite32x32Style;
  $scope.getAnimatedPokemonSrc = SpritesService.getAnimatedPokemonSrc;
  $scope.maleGenderSrc = SpritesService.getMaleGenderSrc();
  $scope.femaleGenderSrc = SpritesService.getFemaleGenderSrc();
  $scope.noGenderSrc = SpritesService.getNoGenderSrc();
  $scope.getFootprintSrc = SpritesService.getFootprintSrc;
  $scope.getShapeSrc = SpritesService.getShapeSrc;
  $scope.getHabitatSrc = SpritesService.getHabitatSrc;
  $scope.capturedNormalPokemonSrc = SpritesService.getCapturedNormalPokemonSrc();
  $scope.capturedShinyPokemonSrc = SpritesService.getCapturedShinyPokemonSrc();
  $scope.getPokemonMediumSizeSrc = SpritesService.getPokemonMediumSizeSrc;
});
