app.controller("NotesCtrl", function($scope, $translate, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  // Update notes if requested
  $scope.notes = UserService.getNotes();
  $scope.$watch('notes', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserNotes(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("NOTES"), $translate.instant("NOTES_UPDATED"));
          UserService.setNotes(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("NOTES"), $translate.instant("ERROR_UPDATING_NOTES"));
        }
      });
    }
  });
});
