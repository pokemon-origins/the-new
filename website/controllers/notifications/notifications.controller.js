app.controller("NotificationsCtrl", function($scope, $translate, $uibModalInstance, PublicApiService, MessagesService, SpritesService) {
  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  PublicApiService.getUserNotifications().then(function (response) {
    if (response.data) {
      $scope.notifications = response.data;
    } else {
      MessagesService.addWarningMessage($translate.instant("NOTIFICATIONS"), "Impossible to get notifications");
    }
  });
});
