app.controller("ItemCtrl", function($scope, $translate, $uibModalInstance, PublicApiService, UserService, MessagesService, SpritesService, itemId) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  if (itemId <= 0) {
    $scope.close();
  }

  $scope.SpritesService = SpritesService;

  PublicApiService.getItem(UserService.getLanguage(), itemId).then(function (response) {
    if (response.data) {
      $scope.item = response.data;
    } else {
      MessagesService.addWarningMessage("ITEM", "Impossible to get item");
    }
  });
  PublicApiService.getItemFlags(UserService.getLanguage(), itemId).then(function (response) {
    if (response.data) {
      $scope.item_flags = response.data;
    } else {
      MessagesService.addWarningMessage("ITEM", "Impossible to get item flags");
    }
  });
});
