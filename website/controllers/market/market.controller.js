app.controller("MarketCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, PublicApiService, SpritesService, MessagesService, marketType) {
  $scope.SpritesService = SpritesService;

  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  // Function to open a public profile modal view
  $scope.openPublicProfile = function(user_id) {
    $uibModal.open({
      templateUrl: 'controllers/public_profile/public_profile.html',
      controller: 'PublicProfileCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return user_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("MarketCtrl", 'Public Profile modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("MarketCtrl", 'Public Profile modal closed with cancel');
    });
  };

  // -------------------------------- GOLD MARKET FUNCTIONS ----------------------------------

  // Function to get data used for the gold market
  $scope.showGoldMarket = function() {
    $scope.market_type = "gold_market";

    PublicApiService.getUserGoldInSell().then(function (response) {
      if (response.data) {
        $scope.user_gold_sell = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("MARKET"), "Could not get user gold in sell");
      }
    });

    PublicApiService.getAllGoldInSell().then(function (response) {
      if (response.data) {
        $scope.all_gold_sell = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("MARKET"), "Could not get all gold in sell");
      }
    });
  };

  // Function to update the user gold sell price
  $scope.updateGoldSellPrice = function() {
    PublicApiService.updateUserGoldPrice($scope.gold_sell_price).then(function (response) {
      if (response.data == true) {
        MessagesService.addInfoMessage($translate.instant("MARKET"), $translate.instant("GOLD_SELL_PRICE_UPDATED"));
        $scope.showGoldMarket();
      } else {
        MessagesService.addWarningMessage($translate.instant("MARKET"), "Error updating the gold sell price");
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Error updating the gold sell price');
    });
  };

  // Function to update the user gold to sell
  $scope.updateGoldInSell = function() {
    PublicApiService.updateUserGoldInSell($scope.gold_to_add_or_remove).then(function (response) {
      if (response.data == true) {
        MessagesService.addInfoMessage($translate.instant("MARKET"), $translate.instant("GOLD_IN_SELL_UPDATED"));
        $scope.showGoldMarket();
      } else {
        MessagesService.addWarningMessage($translate.instant("MARKET"), "Error updating the gold sell price");
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Error updating the gold sell price');
    });
  };

  // Function to buy gold from an other user
  $scope.buyGoldFromOtherUser = function(seller_id, quantity, requested_gold_price) {
    PublicApiService.buyGoldFromOtherUser(seller_id, quantity, requested_gold_price).then(function (response) {
      if (response.data == true) {
        MessagesService.addInfoMessage($translate.instant("MARKET"), $translate.instant("GOLD_BOUGHT"));
        $scope.showGoldMarket();
      } else {
        MessagesService.addWarningMessage($translate.instant("MARKET"), "Error buying the gold");
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Error buying the gold');
    });
  };

  // -------------------------------- SHOW REQUESTED MARKET ----------------------------------

  // Show a specific market if requested
  if (marketType == "gold_market")
  {
    $scope.updateGoldMarket();
  }
});
