app.controller("MenuCtrl", function($scope, $css, $uibModal, SpritesService, MessagesService, UserService) {
  // Bind the CSS to the controller in order to be integrated directly
  $css.bind({href: 'controllers/menu/menu.css'}, $scope);

  // Import image service needed in the view
  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  // Hide navbar when clicking on a menu click
  $(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide');
    }
  });

  // Functions to open the different modal views
  $scope.openSettings = function() {
    $uibModal.open({
      templateUrl: 'controllers/settings/settings.html',
      controller: 'SettingsCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("MenuCtrl", 'Settings modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("MenuCtrl", 'Settings modal closed with cancel');
    });
  };
  $scope.openAbout = function() {
    $uibModal.open({
      templateUrl: 'controllers/about/about.html',
      controller: 'AboutCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("MenuCtrl", 'About modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("MenuCtrl", 'About modal closed with cancel');
    });
  };
  $scope.openNews = function() {
    $uibModal.open({
      templateUrl: 'controllers/news/news.html',
      controller: 'NewsCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("MenuCtrl", 'News modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("MenuCtrl", 'News modal closed with cancel');
    });
  };
  $scope.openNotifications = function() {
    $uibModal.open({
      templateUrl: 'controllers/notifications/notifications.html',
      controller: 'NotificationsCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("MenuCtrl", 'Notifications modal closed with ok');
      UserService.setNotificationCount(0);
    }, function () {
      MessagesService.addDebugMessage("MenuCtrl", 'Notifications modal closed with cancel');
      UserService.setNotificationCount(0);
    });
  };
});
