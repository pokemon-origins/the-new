// Basic Modal views
app.controller("BasicModalCtrl", function($scope, $uibModalInstance, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
});
