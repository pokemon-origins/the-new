app.controller("ProfileCtrl", function($scope, $translate, $uibModalInstance, $uibModal, UserService, PublicApiService, MessagesService, SpritesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  // Get recommendees & blacklisted users
  $scope.recommendees = [];
  $scope.blacklist = [];
  if (UserService.getIsConnected())
  {
    PublicApiService.getUserRecommendees().then(function (response) {
      if (response.data) {
        $scope.recommendees = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("PROFILE"), $translate.instant("ERROR_GETTING_RECOMMENDEES"));
      }
    });

    PublicApiService.getAllBlacklistedUsers().then(function (response) {
      if (response.data) {
        $scope.blacklist = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("PROFILE"), "Impossible to get blacklisted users");
      }
    });
  }

  // Update login if requested
  $scope.login = UserService.getLogin();
  $scope.$watch('login', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserLogin(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("PROFILE"), $translate.instant("LOGIN_UPDATED"));
          UserService.setLogin(val);
        } else {
          MessagesService.addWarningMessage($translate.instant("PROFILE"), $translate.instant("ALREADY_USED_LOGIN"));
        }
      });
    }
  });

  // Update gender if requested
  $scope.gender = UserService.getGenderId();
  $scope.genders = [
    {value: 1, text: $translate.instant("GIRL")},
    {value: 2, text: $translate.instant("MAN")}
  ];
  $scope.$watch('gender', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserGender(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("PROFILE"), $translate.instant("GENDER_UPDATED"));
          UserService.setGenderId(val);
        } else {
          MessagesService.addErrorMessage($translate.instant("PROFILE"), $translate.instant("ERROR_UPDATING_GENDER"));
        }
      });
    }
  });

  // Update description if requested
  $scope.description = UserService.getDescription();
  $scope.$watch('description', function(val, oldVal) {
    if (UserService.getIsConnected() && val != oldVal)
    {
      PublicApiService.setUserDescription(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("PROFILE"), $translate.instant("DESCRIPTION_UPDATED"));
          UserService.setDescription(val);
        } else {
          MessagesService.addErrorMessage($translate.instant("PROFILE"), $translate.instant("ERROR_UPDATING_DESCRIPTION"));
        }
      });
    }
  });

  // Open avatar modal if requested
  $scope.openAvatarEditor = function() {
    $uibModal.open({
      templateUrl: 'controllers/profile/avatar.html',
      controller: 'AvatarCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("ProfileCtrl", 'Avatar modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("ProfileCtrl", 'Avatar modal closed with cancel');
    });
  };

  // Open public profile if requested
  $scope.openPublicProfile = function(user_id) {
    $uibModal.open({
      templateUrl: 'controllers/public_profile/public_profile.html',
      controller: 'PublicProfileCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return user_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("ProfileCtrl", 'Public Profile modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("ProfileCtrl", 'Public Profile modal closed with cancel');
    });
  };
});

app.controller("AvatarCtrl", function($scope, $translate, $timeout, $uibModalInstance, SpritesService, UserService, PublicApiService, MessagesService) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;

  $scope.uploadedImage='';
  $scope.croppedImage='';

  var handleFileSelect=function(evt) {
    var file=evt.currentTarget.files[0];
    var reader = new FileReader();
    reader.onload = function (evt) {
      $scope.$apply(function($scope){
        $scope.uploadedImage=evt.target.result;
      });
    };
    reader.readAsDataURL(file);
  };

  // Wait that the HTML is loaded before checking file selection (else not working)
  $timeout(function() {
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
  }, 3000, false);

  $scope.showErrorLoadingImage = function() {
    MessagesService.addWarningMessage($translate.instant("AVATAR"), $translate.instant("IMPOSSIBLE_LOAD_IMAGE"));
  };

  $scope.uploadAvatar = function() {
    if (UserService.getIsConnected())
    {
      PublicApiService.setUserAvatar($scope.croppedImage.replace("data:image/png;base64,", "")).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("AVATAR"), $translate.instant("AVATAR_UPDATED"));
          SpritesService.reloadAvatars();
        } else {
          MessagesService.addErrorMessage($translate.instant("AVATAR"), $translate.instant("ERROR_UPDATING_AVATAR"));
        }
      });
    }
  };
});
