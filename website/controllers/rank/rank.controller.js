app.controller("RankCtrl", function($scope, $translate, $uibModal, $uibModalInstance, UserService, PublicApiService, MessagesService, SpritesService, rankType) {
  // Function to close the modal
  $scope.close = function() {
    $uibModalInstance.close();
  };

  $scope.SpritesService = SpritesService;
  $scope.UserService = UserService;

  $scope.sum = function(val1, val2) {
    return parseInt(val1) + parseInt(val2);
  };

  // Open hunting competition modal if requested
  $scope.openHuntingCompetition = function() {
    $uibModal.open({
      templateUrl: 'controllers/hunting_competition/hunting_competition.html',
      controller: 'HuntingCompetitionCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("RankCtrl", 'Hunting competition modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("RankCtrl", 'Hunting competition modal closed with cancel');
    });
  };

  // Function to open a public profile modal view
  $scope.openPublicProfile = function(user_id) {
    $uibModal.open({
      templateUrl: 'controllers/public_profile/public_profile.html',
      controller: 'PublicProfileCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return user_id;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("RankCtrl", 'Public Profile modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("RankCtrl", 'Public Profile modal closed with cancel');
    });
  };

  // All potential rank types
  $scope.rankTypes = [ {code: "score", name: $translate.instant("SCORE")},
                       {code: "pvp_score", name: $translate.instant("PVP_SCORE")},
                       {code: "hunting_score", name: $translate.instant("HUNTING_SCORE")}
                     ];

  // Select the rank type requested by the modal caller
  $scope.rankType = $scope.rankTypes.filter(function(item) { return item.code === rankType})[0];

  // Don't show any element at the begining
  $scope.show_score = false;
  $scope.show_pvp_score = false;
  $scope.show_hunting_score = false;

  $scope.getTop50 = function(type) {
    PublicApiService.getTop50Users(type).then(function (response) {
      if (response.data) {
        $scope.top_50 = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("RANK"), $translate.instant("ERROR_GETTING_TOP_50"));
      }
    });
  };
  $scope.getRank = function(type) {
    PublicApiService.getUserRank(type).then(function (response) {
      if (response.data) {
        $scope.rank = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("RANK"), $translate.instant("ERROR_GETTING_RANK"));
      }
    });
  };
  $scope.getBetterThan = function(type) {
    PublicApiService.getUsersBetterThanUser(type).then(function (response) {
      if (response.data) {
        $scope.better_than = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("RANK"), $translate.instant("ERROR_GETTING_BETTER_THAN"));
      }
    });
  };
  $scope.getWorseThan = function(type) {
    PublicApiService.getUsersWorseThanUser(type).then(function (response) {
      if (response.data) {
        $scope.worse_than = response.data;
      } else {
        MessagesService.addWarningMessage($translate.instant("RANK"), $translate.instant("ERROR_GETTING_WORSE_THAN"));
      }
    });
  };

  // Functions to update data
  $scope.updateScore = function() {
    $scope.show_score = true;
    $scope.show_pvp_score = false;
    $scope.show_hunting_score = false;
    $scope.getRank("score");
    $scope.getTop50("score");
    $scope.getBetterThan("score");
    $scope.getWorseThan("score");
  };
  $scope.updatePvpScore = function() {
    $scope.show_score = false;
    $scope.show_pvp_score = true;
    $scope.show_hunting_score = false;
    $scope.getRank("pvp_score");
    $scope.getTop50("pvp_score");
    $scope.getBetterThan("pvp_score");
    $scope.getWorseThan("pvp_score");
  };
  $scope.updateHuntingScore = function() {
    $scope.show_score = false;
    $scope.show_pvp_score = false;
    $scope.show_hunting_score = true;
    $scope.getRank("hunting_competition_score");
    $scope.getTop50("hunting_competition_score");
    $scope.getBetterThan("hunting_competition_score");
    $scope.getWorseThan("hunting_competition_score");
  };

  if ($scope.rankType.code == "score") {
    $scope.updateScore();
  } else if ($scope.rankType.code == "pvp_score") {
    $scope.updatePvpScore();
  } else if ($scope.rankType.code == "hunting_score") {
    $scope.updateHuntingScore();
  } else {
    MessagesService.addErrorMessage($translate.instant("RANK"), "Invalid rank type "+ rankType +", please contact the webmaster");
    $scope.updateScore();
  }

  $scope.$watch('rankType', function(val, oldVal) {
    if (val.code != oldVal.code) {
      if ($scope.rankType.code == "score") {
        $scope.updateScore();
      } else if ($scope.rankType.code == "pvp_score") {
        $scope.updatePvpScore();
      } else if ($scope.rankType.code == "hunting_score") {
        $scope.updateHuntingScore();
      }
    }
  });
});
