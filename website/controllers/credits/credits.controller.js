// All the credits + social media buttons
app.controller("CreditsCtrl", function($scope, $uibModal, SpritesService, MessagesService) {
  // Import images service needed in the view
  $scope.SpritesService = SpritesService;

  // Functions to open the different modal views
  $scope.openFacebook = function() {
    $uibModal.open({
      templateUrl: 'controllers/credits/facebook.html',
      controller: 'BasicModalCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("CreditsCtrl", 'Facebook modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("CreditsCtrl", 'Facebook modal closed with cancel');
    });
  };
  $scope.openTwitter = function() {
    $uibModal.open({
      templateUrl: 'controllers/credits/twitter.html',
      controller: 'BasicModalCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("CreditsCtrl", 'Twitter modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("CreditsCtrl", 'Twitter modal closed with cancel');
    });
  };
});
