app.controller("UserCtrl", function($scope, $translate, $timeout, $uibModal, PublicApiService, MessagesService, UserService, SpritesService) {
  // Allow user service in watch functions and in the view
  $scope.UserService = UserService;
  $scope.SpritesService = SpritesService;

  // Functions to open the different modal views
  $scope.openPublicProfile = function() {
    $uibModal.open({
      templateUrl: 'controllers/public_profile/public_profile.html',
      controller: 'PublicProfileCtrl',
      size: 'lg',
      resolve: {
        user_id: function () {
          return UserService.getId();
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Public Profile modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Public Profile modal closed with cancel');
    });
  };

  $scope.openPokedex = function() {
    $uibModal.open({
      templateUrl: 'controllers/pokedex/pokedex.html',
      controller: 'PokedexCtrl',
      size: 'lg',
      resolve: {
        pokemonId: function () {
          return 0;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Pokedex modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Pokedex modal closed with cancel');
    });
  };

  $scope.openBag = function() {
    $uibModal.open({
      templateUrl: 'controllers/bag/bag.html',
      controller: 'BagCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Bag modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Bag modal closed with cancel');
    });
  };

  $scope.openNotes = function() {
    $uibModal.open({
      templateUrl: 'controllers/notes/notes.html',
      controller: 'NotesCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Notes modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Notes modal closed with cancel');
    });
  };

  $scope.openMarket = function() {
    $uibModal.open({
      templateUrl: 'controllers/market/market.html',
      controller: 'MarketCtrl',
      size: 'lg',
      resolve: {
        marketType: function () {
          return "none";
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Market modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Market modal closed with cancel');
    });
  };

  $scope.openCasino = function() {
    $uibModal.open({
      templateUrl: 'controllers/casino/casino.html',
      controller: 'CasinoCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Casino modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Casino modal closed with cancel');
    });
  };

  $scope.openAchievements = function() {
    $uibModal.open({
      templateUrl: 'controllers/achievements/achievements.html',
      controller: 'AchievementsCtrl',
      size: 'lg'
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Achievements modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Achievements modal closed with cancel');
    });
  };

  $scope.openRank = function(rank) {
    $uibModal.open({
      templateUrl: 'controllers/rank/rank.html',
      controller: 'RankCtrl',
      size: 'lg',
      resolve: {
        rankType: function () {
          return rank;
        }
      }
    }).result.then(function () {
      MessagesService.addDebugMessage("UserCtrl", 'Rank modal closed with ok');
    }, function () {
      MessagesService.addDebugMessage("UserCtrl", 'Rank modal closed with cancel');
    });
  };

  // Store the user language in the database if updated by the user in the UI
  $scope.$watch('UserService.getLanguage()', function(val, oldVal) {
    if (UserService.getIsConnected() && val != null && val != oldVal)
    {
      PublicApiService.setUserLanguage(val).then(function (response) {
        if (response.data) {
          MessagesService.addSuccessMessage($translate.instant("USER"), $translate.instant("USER_LANGUAGE_SAVED"));
        } else {
          MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("ERROR_SAVING_USER_LANGUAGE"));
        }
      });
    }
  });

  // Be sure no other session is already in use (close them if necessary)
  PublicApiService.isConnected().then(function (response) {
    if (response.data["is_connected"] == true) {
      PublicApiService.disconnect().then(function (response) {
        if (response.data == true) {
          MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("CLOSED_DETECTED_MULTI_SESSION"));
          UserService.setIsConnected(false);
        } else {
          MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("ERROR_CLOSING_DETECTED_MULTI_SESSION"));
          UserService.setIsConnected(false);
        }
      });
    }
  });

  // Function to try to connect with a login/password
  $scope.connect = function() {
    PublicApiService.connect(UserService.getLogin(), UserService.getPassword()).then(function (response) {
      if (response.data["is_connected"] == true) {
        // The user is now connected
        MessagesService.addInfoMessage($translate.instant("USER"), $translate.instant("CONNECTED"));
        UserService.setId(response.data["user_id"]);

        // Get 'static' informations needed for the whole game
        PublicApiService.getUserStaticInformation().then(function (response) {
          UserService.setLogin(response.data["login"]);
          UserService.setLanguage(response.data["language_code"]);
          UserService.setEmail(response.data["email"]);
          UserService.setAcceptNewsletter(response.data["accept_newsletter"] == "1");
          UserService.setReferralId(response.data["referral_id"] != null ? parseInt(response.data["referral_id"]) : null);
          UserService.setReferralName(response.data["referral_name"]);
          UserService.setGenderId(parseInt(response.data["gender_id"]));
          UserService.setDescription(response.data["description"]);
          UserService.setAcceptPvpFights(response.data["accept_pvp_fights"] == "1");
          UserService.setNumberDaysConnectedInARow(parseInt(response.data["number_days_connected_in_a_row"]));
          UserService.setShowTips(response.data["show_tips"] == "1");
          UserService.setSpentGold(parseInt(response.data["spent_gold"]));
          UserService.setSpentPokedollar(parseInt(response.data["spent_pokedollar"]));
          UserService.setSpentRealEuros(parseInt(response.data["spent_real_euros"]));
          UserService.setRegistrationDate(response.data["registration_date"]);
          UserService.setAutomaticEvolution(response.data["automatic_evolution"] == "1");
          UserService.setNotes(response.data["notes"]);
          UserService.setNumberOfUnseenNews(parseInt(response.data["unseen_news"]));

          UserService.setIsConnected(true);

          // Check that user is not disconnected for inactivity (for the whole time of the session)
          checkIfConnectedLoop();

          // Update base user information periodically
          updateBaseInformation();
          updateNotifications();
        }, function (response) {
          // Since those informations are essential for the game, close the session
          MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not get static user informations needed for the game');
          PublicApiService.disconnect();
          UserService.setIsConnected(false);
        });
      } else {
        MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("NOT_CONNECTED"));
        UserService.setIsConnected(false);
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not connect');
      UserService.setIsConnected(false);
    });
  };

  // Function to try to disconnect
  $scope.disconnect = function() {
    PublicApiService.disconnect().then(function (response) {
      if (response.data == true) {
        MessagesService.addInfoMessage($translate.instant("USER"), $translate.instant("DISCONNECTED"));
        UserService.setIsConnected(false);
      } else {
        MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("NOT_DISCONNECTED"));
      }
    }, function (response) {
      MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could not disconnect');
    });
  };

  // Internal function to update base information every 20sec
  updateBaseInformation = function() {
    if (UserService.getIsConnected()) {
      PublicApiService.getUserBaseInformation().then(function (response) {
        UserService.setPokedollar(response.data["pokedollar"]);
        UserService.setGold(response.data["gold"]);
        UserService.setRank(response.data["rank"]);
        UserService.setStorageSize(response.data["storage_size"]);
        UserService.setScore(response.data["score"]);
        UserService.setPvpScore(response.data["pvp_score"]);
        UserService.setLotteryTickets(parseInt(response.data["lottery_tickets"]));
        UserService.setHuntingCompetitionScore(response.data["hunting_competition_score"]);
        UserService.setNumberOwnedPokemons(response.data["number_owned_pokemons"]);
      }, function (response) {
        MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not get base user informations (will try again in 20sec)');
      });

      $timeout(updateBaseInformation, 20000);
    }
  }

  // Internal function to check if notifications are available
  updateNotifications = function() {
    if (UserService.getIsConnected()) {
      PublicApiService.getUserNotificationCount().then(function (response) {
        UserService.setNotificationCount(response.data);
      }, function (response) {
        MessagesService.addWarningMessage($translate.instant("API_ERROR"), 'Could not get notification count (will try again in 60sec)');
      });

      $timeout(updateNotifications, 60000);
    }
  }

  // Internal function to alert if the user is still connected (check every 60sec)
  checkIfConnectedLoop = function() {
    // Check connection only if already connected
    if (UserService.getIsConnected()) {
      PublicApiService.isConnected().then(function (response) {
        if (response.data["is_connected"] == true) {
          if (response.data["user_id"] == UserService.getId()) {
            // Check every minute if still connected
            $timeout(checkIfConnectedLoop, 60000);
          } else {
            // A session with an other account is already in use... Something messed up
            PublicApiService.disconnect().then(function (response) {
              if (response.data == true) {
                MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("CLOSED_DETECTED_MULTI_SESSION"));
                UserService.setIsConnected(false);
              } else {
                MessagesService.addErrorMessage($translate.instant("USER"), $translate.instant("ERROR_CLOSING_DETECTED_MULTI_SESSION"));
                UserService.setIsConnected(false);
              }
            });
          }
        } else {
          UserService.setIsConnected(false);
          MessagesService.addWarningMessage($translate.instant("USER"), $translate.instant("LOST_CONNECTION"));
        }
      }, function (response) {
        UserService.setIsConnected(false);
        MessagesService.addErrorMessage($translate.instant("API_ERROR"), 'Could check the connection status');
      });
    }
  };
});
