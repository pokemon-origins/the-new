app.filter("randomSrc", function () {
  return function (input) {
    if (input) {
      var sep = input.indexOf("?") != -1 ? "&" : "?";
      return input + sep + "r=" + Math.round(Math.random() * 999999);
    }
  }
});
