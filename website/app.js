// Base module
var app = angular.module("app", ["services", "ngRoute", "ngSanitize", "ngAnimate", "ngTouch", "ngImgCrop", "ui.bootstrap", "ui.toggle", "pascalprecht.translate", "angularCSS", "xeditable", "mwl.confirm"]);

// Route all controllers to the page view depending on the user choice
app.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl : "controllers/welcome/welcome.html",
    controller : "WelcomeCtrl"
    //, css: ["", "", ...]
  })
  .otherwise({
    redirectTo: "/"
  });
});

var services = angular.module('services', []);
