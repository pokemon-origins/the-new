<?php

// Start the API session
session_start();

// TODELETE ONCE API FINISHED
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("error_reporting", E_ALL);

use Phalcon\Mvc\Micro;

$api = new Micro();

// Base API page
$api->get(
  "/",
  function ()
  {
    echo "Welcome to Pokemon-Origins API";
  }
);

// API version
$api->get(
  "/version",
  function () use($api)
  {
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(["version" => 0.1]);
    $api->response->send();
  }
);

// Get languages
$api->get(
  "/languages/allInfo",
  function () use($api)
  {
    include_once("../include/languages.php");
    $api->response->setJsonContent(Languages::getLanguages());
    $api->response->send();
  }
);

$api->get(
  "/languages/isValid/{language}",
  function($language) use($api)
  {
    include_once("../include/languages.php");
    $api->response->setJsonContent(Languages::checkLanguage($language));
    $api->response->send();
  }
);

$api->get(
  "/languages/code",
  function () use($api)
  {
    include_once("../include/languages.php");
    $api->response->setJsonContent(Languages::getLanguageCodes());
    $api->response->send();
  }
);

// Get all news (language code should be specified)
$api->get(
  "/news/{language}",
  function ($language) use($api)
  {
    include_once("../include/news.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(News::getNews($language, -1));
    $api->response->send();
  }
);

// Get news (language code and number of news should be specified)
$api->get(
  "/news/{language}/{numbers:([\d]+)}",
  function ($language, $numbers) use($api)
  {
    include_once("../include/news.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(News::getNews($language, $numbers));
    $api->response->send();
  }
);

// Get types
$api->get(
  "/types/{language}",
  function ($language) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getTypes($language));
    $api->response->send();
  }
);

// Get specific type
$api->get(
  "/types/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getType($id, $language));
    $api->response->send();
  }
);

// Get attack categories
$api->get(
  "/attackCategories/{language}",
  function ($language) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getAttackCategories($language));
    $api->response->send();
  }
);

// Get all attacks
$api->get(
  "/attacks/all/{language}",
  function ($language) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getAllAttacks($language));
    $api->response->send();
  }
);

// Get specific attack
$api->get(
  "/attack/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getAttack($language, $id));
    $api->response->send();
  }
);

// Get pokemons learning a specific attack with a specific method
$api->get(
  "/attack/pokemonLearning/{language}/{attack_id:([\d]+)}/{method_id:([\d]+)}",
  function ($language, $attack_id, $method_id) use($api)
  {
    include_once("../include/attacks.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Attacks::getPokemonsLearningAttack($language, $attack_id, $method_id));
    $api->response->send();
  }
);

// Get number of registered users
$api->get(
  "/users/registered",
  function () use($api)
  {
    include_once("../include/users.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Users::getNumberOfRegisteredUsers());
    $api->response->send();
  }
);

// Get number of registered users
$api->get(
  "/users/search/{base}",
  function ($base) use($api)
  {
    if (strlen($base) >= 2)
    {
      include_once("../include/users.php");
      $api->response->setHeader('Access-Control-Allow-Origin', '*');
      $api->response->setJsonContent(Users::search($base));
      $api->response->send();
    }
  }
);

// Get number of connected users
$api->get(
  "/users/connected",
  function () use($api)
  {
    include_once("../include/users.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Users::getNumberOfConnectedUsers());
    $api->response->send();
  }
);

// Get top 50 user by type
$api->get(
  "/users/rank/top50/{type}",
  function ($type) use($api)
  {
    include_once("../include/users.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Users::getTop50Users($type));
    $api->response->send();
  }
);

// Get 10 users better than connected user
$api->get(
  "/users/rank/betterThan/{type}",
  function ($type) use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/users.php");
      $result = Users::get10UsersBetterThan($type, $_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get 10 users worse than connected user
$api->get(
  "/users/rank/worseThan/{type}",
  function ($type) use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/users.php");
      $result = Users::get10UsersWorseThan($type, $_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get user rank
$api->get(
  "/user/rank/{type}",
  function ($type) use($api)
  {
    $result = -1;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::getRank($type, $_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get user items
$api->get(
  "/user/items/{language}",
  function ($language) use($api)
  {
    $result = [];
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/items.php");
      $result = Items::getUserItems($language, $_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get item pockets
$api->get(
  "/items/pockets/{language}",
  function ($language) use($api)
  {
    include_once("../include/items.php");
    $result = Items::getPockets($language);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get item basic data
$api->get(
  "/item/baseData/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/items.php");
    $result = Items::getItem($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get item flags
$api->get(
  "/item/flags/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/items.php");
    $result = Items::getItemFlags($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get pokemons to hunt for the hunting competition
$api->get(
  "/huntingCompetition/pokemons/{language}",
  function ($language) use($api)
  {
    include_once("../include/hunting_competition.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(HuntingCompetition::getPokemonsGivingPoints($language));
    $api->response->send();
  }
);

// Get all pokemons
$api->get(
  "/pokemons/all/{language}",
  function ($language) use($api)
  {
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/pokemons.php");
      $api->response->setHeader('Access-Control-Allow-Origin', '*');
      $api->response->setJsonContent(Pokemons::getAllPokemons($language, $_SESSION["user_id"]));
      $api->response->send();
    }
  }
);

// Get number of different captured pokemons
$api->get(
  "/pokemons/differentCaptured",
  function () use($api)
  {
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/pokemons.php");
      $api->response->setHeader('Access-Control-Allow-Origin', '*');
      $api->response->setJsonContent(Pokemons::getNumberOfDifferentCapturedPokemons($_SESSION["user_id"]));
      $api->response->send();
    }
  }
);

// Get learnable attacks (with level up) for a pokemon
$api->get(
  "/attacks/learnableWithLevelUp/{language}/{id:([\d]+)}/{max_level:([\d]+)}",
  function ($language, $id, $max_level) use($api)
  {
    include_once("../include/pokemons.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Pokemons::getPokemonLearnableAttacksWithLevel($language, $id, $max_level));
    $api->response->send();
  }
);

// Get learnable attacks (with item) for a pokemon
$api->get(
  "/attacks/learnableWithItem/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/pokemons.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Pokemons::getPokemonLearnableAttacksWithItem($language, $id));
    $api->response->send();
  }
);

// Get evolutions of a pokemon
$api->get(
  "/pokemon/evolutions/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/pokemons.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Pokemons::getEvolutions($language, $id));
    $api->response->send();
  }
);

// Get parent pokemon
$api->get(
  "/pokemon/parent/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/pokemons.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Pokemons::getParentPokemon($language, $id));
    $api->response->send();
  }
);

// Get pokemon information
$api->get(
  "/pokemon/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/pokemons.php");
    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent(Pokemons::getPokemon($language, $id));
    $api->response->send();
  }
);

// Connect with a login and password
// TODO: Add captcha check
$api->post(
  "/user/connect",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (array_key_exists("login", $json_data) and array_key_exists("password", $json_data))
      {
        include_once("helper/user_session.php");
        $result = UserSession::connect($json_data['login'], $json_data['password']);
      }
    }

    $array_result = array("is_connected" => $result);
    if ($result)
    {
      $array_result["user_id"] = $_SESSION["user_id"];
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($array_result);
    $api->response->send();
  }
);

// Disconnect
$api->post(
  "/user/disconnect",
  function() use($api)
  {
    include_once("helper/user_session.php");
    $result = UserSession::disconnect();

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Check if user is connected
$api->get(
  "/user/isConnected",
  function () use($api)
  {
    include_once("helper/user_session.php");
    $is_connected = UserSession::isConnected();

    $array_result = array("is_connected" => $is_connected);
    if ($is_connected)
    {
      $array_result["user_id"] = $_SESSION["user_id"];
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($array_result);
    $api->response->send();
  }
);

// Update the user login
$api->post(
  "/user/login",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("login", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateLogin($_SESSION["user_id"], $json_data['login']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user language
$api->post(
  "/user/language",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("language_code", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateLanguage($_SESSION["user_id"], $json_data['language_code']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user "accept newsletter"
$api->post(
  "/user/acceptNewsletter",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("accept_newsletter", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateAcceptNewsletter($_SESSION["user_id"], $json_data['accept_newsletter']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user avatar
$api->post(
  "/user/avatar",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("avatar", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateAvatar($_SESSION["user_id"], $json_data['avatar']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user password
$api->post(
  "/user/password",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("password", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updatePassword($_SESSION["user_id"], $json_data['password']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user "automatic evolution"
$api->post(
  "/user/automaticEvolution",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("automatic_evolution", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateAutomaticEvolution($_SESSION["user_id"], $json_data['automatic_evolution']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user gender
$api->post(
  "/user/gender",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("gender_id", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateGender($_SESSION["user_id"], $json_data['gender_id']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user description
$api->post(
  "/user/description",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("description", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateDescription($_SESSION["user_id"], $json_data['description']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user notes
$api->post(
  "/user/notes",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("notes", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateNotes($_SESSION["user_id"], $json_data['notes']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user accept PVP fights
$api->post(
  "/user/acceptPvpFights",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("accept_pvp_fights", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateAcceptPvpFights($_SESSION["user_id"], $json_data['accept_pvp_fights']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user show tips
$api->post(
  "/user/showTips",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("show_tips", $json_data))
      {
        include_once("../include/user.php");
        $result = User::updateShowTips($_SESSION["user_id"], $json_data['show_tips']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the user show tips
$api->post(
  "/user/seenNews",
  function() use($api)
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::setAllNewsAsSeen($_SESSION["user_id"]);

      // Keep session alive
      include_once("helper/user_session.php");
      UserSession::keepAlive();
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get static informations about the connected user
$api->get(
  "/user/staticInformation",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::getStaticInformation($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get base informations about the connected user
$api->get(
  "/user/baseInformation",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::getBaseInformation($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get recommendees of the connected user
$api->get(
  "/user/recommendees",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::getRecommendees($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get notifications of the connected user
$api->get(
  "/user/notifications/all",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/notifications.php");
      $result = Notifications::getAndDelete($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get notification count of the connected user
$api->get(
  "/user/notifications/count",
  function () use($api)
  {
    $result = 0;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/notifications.php");
      $result = Notifications::getCount($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get public base informations about a specific user
$api->get(
  "/user/public/base/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/user.php");
    $result = User::getPublicInformation($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get public pokemons owned of a specific user
$api->get(
  "/user/public/ownedPokemons/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/user.php");
    $result = User::getPublicOwnedPokemons($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get badges of a specific user
$api->get(
  "/user/public/badges/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/user.php");
    $result = User::getBadges($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get grades of a specific user
$api->get(
  "/user/public/grades/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/user.php");
    $result = User::getGrades($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get achievements of a specific user
$api->get(
  "/user/public/achievements/{language}/{id:([\d]+)}",
  function ($language, $id) use($api)
  {
    include_once("../include/user.php");
    $result = User::getAchievements($language, $id);

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get gold in sell for user
$api->get(
  "/market/gold/self",
  function () use($api)
  {
    $result = [];
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/market.php");
      $result = Market::getUserGoldInSell($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get all gold in sell (except yours)
$api->get(
  "/market/gold/all",
  function () use($api)
  {
    $result = [];
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/market.php");
      $result = Market::getAllGoldInSell($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the quantity of gold to sell
$api->post(
  "/market/gold/quantity",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("gold_to_add_or_remove", $json_data))
      {
        include_once("../include/market.php");
        $result = Market::updateGoldInSell($_SESSION["user_id"], $json_data['gold_to_add_or_remove']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Update the price of gold to sell
$api->post(
  "/market/gold/price",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND array_key_exists("price", $json_data))
      {
        include_once("../include/market.php");
        $result = Market::updateUserGoldPrice($_SESSION["user_id"], $json_data['price']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Buy gold
$api->post(
  "/market/gold/buy",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND
          array_key_exists("seller_id", $json_data) AND
          array_key_exists("requested_gold_price", $json_data) AND
          array_key_exists("quantity", $json_data))
      {
        include_once("../include/market.php");
        $result = Market::buyGold($json_data['seller_id'], $_SESSION["user_id"],
                                  $json_data['quantity'], $json_data['requested_gold_price']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Send a user report
$api->post(
  "/report",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND
          array_key_exists("reported_user_id", $json_data) AND
          array_key_exists("title", $json_data) AND
          array_key_exists("message", $json_data))
      {
        include_once("../include/contact.php");
        $result = ContactAndReport::sendReport($json_data['title'], $json_data['message'],
                                               $_SESSION["user_id"], $json_data['reported_user_id']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Send a contact form to admin
$api->post(
  "/contact",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND
          array_key_exists("title", $json_data) AND
          array_key_exists("message", $json_data))
      {
        include_once("../include/contact.php");
        $result = ContactAndReport::sendContact($json_data['title'], $json_data['message'],
                                                $_SESSION["user_id"]);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get all blacklisted users
$api->get(
  "/blacklist/all",
  function () use($api)
  {
    $result = [];
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/blacklist.php");
      $result = Blacklist::getAllBlacklistedByAnUser($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Check if a specific user is blacklisted
$api->get(
  "/achievements/all",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/achievements.php");
      $result = Achievements::getAllAchievements($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Upgrade an achievement
$api->post(
  "/achievements/{id:([\d]+)}",
  function ($id) use($api)
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/achievements.php");
      $result = Achievements::upgradeAchievement($_SESSION["user_id"], $id);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Check if a specific user is blacklisted
$api->get(
  "/blacklist/{id:([\d]+)}",
  function ($id) use($api)
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/blacklist.php");
      $result = Blacklist::isBlacklistedByAnUser($_SESSION["user_id"], $id);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Delete a specific user from blacklist
$api->delete(
  "/blacklist/{id:([\d]+)}",
  function ($id) use($api)
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/blacklist.php");
      $result = Blacklist::removeUserFromBlacklist($_SESSION["user_id"], $id);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Add a specific user in blacklist
$api->post(
  "/blacklist/{id:([\d]+)}",
  function ($id) use($api)
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/blacklist.php");
      $result = Blacklist::addUserInBlackList($_SESSION["user_id"], $id);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Get casino rewards
$api->get(
  "/casino/rewards",
  function () use($api)
  {
    $result = array();
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/casino.php");
      $result = Casino::getAllRewards($_SESSION["user_id"]);
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Play in casino
$api->post(
  "/casino/play",
  function() use($api)
  {
    $result = false;
    $json_data = json_decode($api->request->getRawBody(), TRUE);
    if (JSON_ERROR_NONE == json_last_error())
    {
      if (isset($_SESSION["user_id"]) AND
          array_key_exists("is_gold_casino", $json_data))
      {
        include_once("../include/casino.php");
        $result = Casino::play($_SESSION["user_id"], $json_data['is_gold_casino']);

        // Keep session alive
        include_once("helper/user_session.php");
        UserSession::keepAlive();
      }
    }

    $api->response->setHeader('Access-Control-Allow-Origin', '*');
    $api->response->setJsonContent($result);
    $api->response->send();
  }
);

// Path not found
$api->notFound(
  function () use($api)
  {
    $api->response->setStatusCode(404, "Not Found");
    $api->response->setContent("Incorrect API request");
    $api->response->send();
  }
);

// Launch the API
$api->handle();
