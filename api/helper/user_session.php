<?php

class UserSession
{
  /**
   * Connect the user (session + DB)
   *
   * @return bool User connected
   *
   * @example print_r(UserSession::connect());
   */
  public static function connect($login, $password)
  {
    include_once("../include/user.php");
    $id = User::connect($login, $password);
    if ($id > 0)
    {
      $_SESSION["last_time_connected"] = time();
      $_SESSION["user_id"] = $id;

      return true;
    }

    return false;
  }

  /**
   * Check if current user is still connected
   *
   * @return bool User still connected
   *
   * @example print_r(UserSession::isConnected());
   */
  public static function isConnected()
  {
    if (isset($_SESSION["last_time_connected"]))
    {
      // If no action in the last 15 minutes, then not connected anymore
      return (time() <= $_SESSION["last_time_connected"] + 900);
    }

    return false;
  }

  /**
   * Keep the user alive
   *
   * Call this function for user requests that are legitimate to keep him alive
   *
   * @return bool User kept alive
   *
   * @example print_r(UserSession::keepAlive());
   */
  public static function keepAlive()
  {
    if (UserSession::isConnected() AND isset($_SESSION["user_id"]))
    {
      // Do keep alive a maximum of 1 time per 5 minute (prevent too many SQL DB queries)
      if (isset($_SESSION["last_time_connected"]))
      {
        if ($_SESSION["last_time_connected"] > (time() - 60 * 5))
        {
          return true;
        }
      }

      // Update the database keep alive to inform other users
      include_once("../include/user.php");
      if (User::updateLastTimeConnected($_SESSION["user_id"]))
      {
        // Update the local keep alive to keep the session active
        $_SESSION["last_time_connected"] = time();

        return true;
      }
    }

    return false;
  }

  /**
   * Disconnect the user from the current session
   *
   * @return bool User disconnected
   *
   * @example print_r(UserSession::disconnect());
   */
  public static function disconnect()
  {
    $result = false;
    if (isset($_SESSION["user_id"]))
    {
      include_once("../include/user.php");
      $result = User::disconnect($_SESSION["user_id"]);
    }

    session_unset();
    session_destroy();

    return $result;
  }
}

?>
