# Install PHP-Phalcon
curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install php7.0-phalcon

# Enable Rewrite for Apache2
sudo a2enmod rewrite
sudo /etc/init.d/apache2 restart
