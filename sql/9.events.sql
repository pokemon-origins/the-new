START TRANSACTION;

-- Hunting competition
CREATE TABLE IF NOT EXISTS `hunting_competition_pokemons` (
  `pokemon_id` int(4) unsigned NOT NULL PRIMARY KEY COMMENT 'Pokemon ID (Foreign)',
  `points` int(4) unsigned NOT NULL COMMENT 'Points won when winning against the pokemon',
  CONSTRAINT FK_POKEMON_ID_FOR_HUNTING_COMPETITION_POKEMONS FOREIGN KEY (pokemon_id) REFERENCES pokemons(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Quizz
CREATE TABLE IF NOT EXISTS `quizz` (
  `id` int(5) unsigned NOT NULL PRIMARY KEY COMMENT 'Quizz ID (Primary)',
  `in_use` bool NOT NULL COMMENT 'Can this quizz be answered by users?',
  `points` int(4) unsigned NOT NULL COMMENT 'Points won when answer is correct'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `quizz_translation` (
  `quizz_id` int(5) unsigned NOT NULL COMMENT 'Quizz ID (Foreign)',
  `language_code` char(2) NOT NULL COMMENT 'Language code (Foreign)',
  `question` varchar(100) NOT NULL COMMENT 'Question',
  `response_1` varchar(100) NOT NULL COMMENT 'Proposed response (1)',
  `response_2` varchar(100) NOT NULL COMMENT 'Proposed response (2)',
  `response_3` varchar(100) NOT NULL COMMENT 'Proposed response (3)',
  `response_4` varchar(100) NOT NULL COMMENT 'Proposed response (4)',
  `valid_response` int(1) unsigned NOT NULL COMMENT 'Valid response between all proposed responses',
  CONSTRAINT FK_QUIZZ FOREIGN KEY (quizz_id) REFERENCES quizz(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_QUIZZ_LANGUAGE FOREIGN KEY (language_code) REFERENCES languages(code) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (language_code, quizz_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_quizz_answers` (
  `quizz_id` int(5) unsigned NOT NULL COMMENT 'Quizz ID (Foreign)',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID (Foreign)',
  `valid_answer` bool NOT NULL COMMENT 'Is the answer correct?',
  CONSTRAINT FK_USER_QUIZZ_ANSWERS_QUIZZ_ID FOREIGN KEY (quizz_id) REFERENCES quizz(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_USER_QUIZZ_ANSWERS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (quizz_id, user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

COMMIT;
