START TRANSACTION;

-- Internal messages
CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Channel ID (Primary key)',
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation date of the channel',
  `name` varchar(50) NOT NULL COMMENT 'Channel name',
  `is_public` bool NOT NULL COMMENT 'Is this channel accessible for all users?',
  `guild_id` int(6) unsigned COMMENT 'Potential guild channel (Foreign)',
  `is_read_only` bool NOT NULL COMMENT 'Is this channel in read only? (not possible to send messages from non admin users)',
  CONSTRAINT FK_CHANNELS_GUILD_ID FOREIGN KEY (guild_id) REFERENCES guilds(id) ON DELETE CASCADE ON UPDATE CASCADE,
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `channel_users` (
  `channel_id` int(10) unsigned NOT NULL COMMENT 'Channel ID (Foreign)',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID allowed to access the channel (Foreign)',
  `is_admin` bool NOT NULL COMMENT 'Is this user an admin of the channel? (Can delete channel and send message even in read only mode)',
  `last_seen` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Last time the user saw messages in the channel',
  CONSTRAINT FK_CHANNEL_USERS_CHANNEL_ID FOREIGN KEY (channel_id) REFERENCES channels(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_CHANNEL_USERS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (channel_id, user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `channel_messages` (
  `channel_id` int(10) unsigned NOT NULL COMMENT 'Channel ID (Foreign)',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Sender ID (Foreign)',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of the message',
  `content` varchar(255) NOT NULL COMMENT 'Message content',
  CONSTRAINT FK_CHANNEL_MESSAGES_TO FOREIGN KEY (channel_id) REFERENCES channels(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_MESSAGES_MESSAGES_FROM FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (channel_id, user_id, time, content)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TODO: Add a SQL "EVENT" to delete periodically all "channels" with no channel_messages or no channel_users (when users/messages are deleted, the associated channels are not deleted automatically)

COMMIT;
