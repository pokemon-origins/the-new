START TRANSACTION;

-- Users
-- The foreign constraint of current_pokemon_id, breeding_pokemon_1_id and breeding_pokemon_2_id will be added after creating the user_pokemons table
CREATE TABLE IF NOT EXISTS `users` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'User ID (Primary key)',
 `language_code` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
 `current_pokemon_id` int(10) unsigned NULL COMMENT 'Current pokemon ID (Foreign)',
 `breeding_pokemon_1_id` int(10) unsigned NULL COMMENT 'Pokemon 1 breeding ID (Foreign)',
 `breeding_pokemon_2_id` int(10) unsigned NULL COMMENT 'Pokemon 2 breeding ID (Foreign)',
 `login` varchar(20) NOT NULL COMMENT 'Login/Pseudo',
 `hashed_password` varchar(255) NOT NULL COMMENT 'Hashed and salted password',
 `email` varchar(50) NOT NULL COMMENT 'Email address',
 `accept_newsletter` boolean NOT NULL COMMENT 'Is the user accept to receive emails?',
 `automatic_evolution` boolean NOT NULL COMMENT 'Is the user using automatic evolution?',
 `last_badge_id` int(2) unsigned COMMENT 'Last owned badge',
 `is_account_valid` boolean NOT NULL COMMENT 'Is account activated?',
 `registration_date` datetime NOT NULL COMMENT 'Registration date',
 `ip_address` varchar(45) NOT NULL COMMENT 'Last Ip address used by the user (IPv4/6)',
 `referral_id` int(10) unsigned NULL COMMENT 'Referral ID (Foreign)',
 `cheat_count` int(6) unsigned NOT NULL COMMENT 'Number of time a cheat was registered on this account',
 `gender_id` int(1) unsigned NOT NULL COMMENT 'Gender ID (Foreign)',
 `last_seen_news_id` int(6) unsigned NULL COMMENT 'News ID seen by the user (Foreign)',
 `pokedollar` int(10) unsigned NOT NULL COMMENT 'Pokedollar of the user',
 `gold` int(10) unsigned NOT NULL COMMENT 'Gold of the user',
 `gold_in_sell` int(10) unsigned NOT NULL COMMENT 'Gold of the user currently in sell',
 `gold_sell_price` int(5) unsigned NOT NULL COMMENT 'Price of the gold currently in pokedollar',
 `description` text COMMENT 'Description of the user',
 `notes` text COMMENT 'Private notes of the user',
 `storage_size` int(8) unsigned COMMENT 'Pokemon storage size',
 `score` int(10) unsigned NOT NULL COMMENT 'Score of the user',
 `pvp_score` int(10) unsigned NOT NULL COMMENT 'PVP Score of the user',
 `lottery_tickets` int(5) unsigned NOT NULL COMMENT 'Number of owned tickets for the current lottery',
 `accept_pvp_fights` boolean NOT NULL COMMENT 'Accept PVP fights',
 `hunting_competition_score` int(10) unsigned NOT NULL COMMENT 'Hunting competition score of the user',
 `last_time_connected` datetime NOT NULL COMMENT 'Last time the user was connected into the game',
 `number_days_connected_in_a_row` int(6) unsigned NOT NULL COMMENT 'Number of days a user connected to its account without missing a single day',
 `show_tips` boolean NOT NULL COMMENT 'Show tips in the game?',
 `number_of_births` int(6) unsigned NOT NULL COMMENT 'Number of pokemon birth done by the user',
 `successful_missions` int(10) unsigned NOT NULL COMMENT 'Number of successful missions',
 `successful_side_quests` int(10) unsigned NOT NULL COMMENT 'Number of successful side quests',
 `A_class_victories` int(7) unsigned NOT NULL COMMENT 'Number of victories in the A class fights',
 `B_class_victories` int(7) unsigned NOT NULL COMMENT 'Number of victories in the B class fights',
 `C_class_victories` int(7) unsigned NOT NULL COMMENT 'Number of victories in the C class fights',
 `D_class_victories` int(7) unsigned NOT NULL COMMENT 'Number of victories in the D class fights',
 `last_time_bonus_received` datetime NOT NULL COMMENT 'Last time the user received a bonus',
 `spent_gold` int(12) unsigned NOT NULL COMMENT 'Gold (virtual money) spent by the user in the game',
 `spent_pokedollar` int(12) unsigned NOT NULL COMMENT 'Pokedollar (virtual money) spent by the user in the game',
 `spent_real_euros` int(12) unsigned NOT NULL COMMENT 'Euros (real money) spent by the user in the game',
 `end_increase_number_of_fights_bonus` datetime NOT NULL COMMENT 'End date of the fight bonus',
 `end_xp_bonus` datetime NOT NULL COMMENT 'End date of the xp bonus',
 `end_speed_up_birth_bonus` datetime NOT NULL COMMENT 'End date of the birth bonus',
 `end_show_pokemon_items_bonus` datetime NOT NULL COMMENT 'End date of the bonus used to show items on pokemons',
  CONSTRAINT FK_USERS_LANGUAGE FOREIGN KEY (language_code) REFERENCES languages(code) ON UPDATE CASCADE,
  CONSTRAINT FK_USERS_GENDER FOREIGN KEY (gender_id) REFERENCES genders(id) ON UPDATE CASCADE,
  CONSTRAINT FK_USERS_SEEN_NEWS FOREIGN KEY (last_seen_news_id) REFERENCES news(id) ON UPDATE CASCADE,
  CONSTRAINT FK_USERS_REFERRAL FOREIGN KEY (referral_id) REFERENCES users(id) ON UPDATE CASCADE,
  CONSTRAINT FK_USERS_BADGE FOREIGN KEY (last_badge_id) REFERENCES badges(id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
-- TODO: Add all map related elements in users table (pos_hor, pos_ver, quete_principale, quete_principale_etape, quete_principale_active, nb_quete_principale, quete_secondaire, nb_quete_secondaire, ...)

-- Blacklist between users
CREATE TABLE IF NOT EXISTS `blacklist` (
  `from_id` int(10) unsigned NOT NULL COMMENT 'User wanting to black list and other user',
  `to_id` int(10) unsigned NOT NULL COMMENT 'Backlisted user',
  CONSTRAINT FK_BLACKLIST_FROM FOREIGN KEY (from_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_BLACKLIST_TO FOREIGN KEY (to_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (from_id, to_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- User notifications
CREATE TABLE IF NOT EXISTS `user_notifications` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID (Foreign)',
  `title` varchar(60) COMMENT 'Title of the notification',
  `notification` varchar(255) COMMENT 'Notification',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Publication date',
  CONSTRAINT FK_USER_NOTIFICATIONS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- User items
CREATE TABLE IF NOT EXISTS `user_items` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID (Foreign)',
  `item_id` int(4) unsigned NOT NULL COMMENT 'Item ID (Foreign)',
  `quantity` int(8) unsigned NOT NULL COMMENT 'Quantity',
  `quantity_in_sell` int(8) unsigned NULL COMMENT 'Quantity currently in sale',
  `sell_price` int(8) unsigned NULL COMMENT 'Price of the item in pokedollar',
  CONSTRAINT FK_USER_ITEMS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_USER_ITEMS_ITEM_ID FOREIGN KEY (item_id) REFERENCES items(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (user_id, item_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Berries plantations
CREATE TABLE IF NOT EXISTS `user_berries_plantations` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Berry plantation ID (Primary key)',
 `user_id` int(10) unsigned NOT NULL COMMENT 'Owner of the berries plantation / ID (Foreign)',
 `berry_id` int(4) unsigned NULL COMMENT 'Current berry in the plantation / ID (Foreign)',
 `planted_time` datetime NULL COMMENT 'When the user planted the berry',
  CONSTRAINT FK_USER_BERRIES_PLANTATIONS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_USER_BERRIES_PLANTATIONS_BERRY_ID FOREIGN KEY (berry_id) REFERENCES berries(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- User pokedex
CREATE TABLE IF NOT EXISTS `user_pokedex` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User ID (Foreign)',
  `pokemon_id` int(4) unsigned NOT NULL COMMENT 'Pokemon ID (Foreign)',
  `is_shiny` boolean NOT NULL COMMENT 'Is it a shiny pokemon?',
  CONSTRAINT FK_USER_POKEDEX_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_USER_POKEDEX_POKEMON_ID FOREIGN KEY (pokemon_id) REFERENCES pokemons(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (user_id, pokemon_id, is_shiny)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Storage boxes
CREATE TABLE IF NOT EXISTS `user_storage_boxes` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'User storage box ID (Primary key)',
 `user_id` int(10) unsigned NOT NULL COMMENT 'Owner of the box / ID (Foreign)',
 `name` varchar(30) NOT NULL COMMENT 'Display name',
 `size` int(5) unsigned NOT NULL COMMENT 'Storage capacity',
  CONSTRAINT FK_USERS_STORAGE_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- User pokemons
CREATE TABLE IF NOT EXISTS `user_pokemons` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'User Pokemon (Primary key)',
 `user_id` int(10) unsigned NOT NULL COMMENT 'User ID which owns the pokemon (Foreign)',
 `user_lender_id` int(10) unsigned NULL COMMENT 'User lender ID which really owns the pokemon (Foreign)',
 `user_storage_box_id` int(10) unsigned NULL COMMENT 'Storage box where the pokemon is (if in a box) (Foreign)',
 `pokemon_id` int(4) unsigned NOT NULL COMMENT 'Pokemon ID (Foreign)',
 `gender_id` int(1) unsigned NOT NULL COMMENT 'Gender ID (Foreign)',
 `held_item_id` int(4) unsigned NULL COMMENT 'Held item (Foreign)',
 `nickname` varchar(20) NOT NULL COMMENT 'Nickname',
 `is_shiny` boolean NOT NULL COMMENT 'Is the pokemon shiny?',
 `is_in_sell` boolean NOT NULL COMMENT 'Is the pokemon in sell?',
 `sell_price` int(8) unsigned NULL COMMENT 'Price of the pokemon in pokedollar',
 `is_in_bid` boolean NOT NULL COMMENT 'Is the pokemon in bid?',
 `bid_price` int(8) unsigned NULL COMMENT 'Current bid price of the pokemon in pokedollar',
 `bid_user` int(10) unsigned NULL COMMENT 'Current bidder for the pokemon (Foreign)',
 `bid_end` datetime NULL COMMENT 'End of the bid',
 `is_in_exchange` boolean NOT NULL COMMENT 'Is the pokemon in exchange?',
 `pokemon_exchange_id` int(4) unsigned NULL COMMENT 'Pokemon to exchange with (Foreign)',
 `minimum_level_for_exchange` int(3) unsigned NULL COMMENT 'Required pokemon level to exchange with',
 `need_shiny_for_exchange` bool NULL COMMENT 'Required shiny pokemon to exchange with',
 `victories` int(8) unsigned NOT NULL COMMENT 'Number of won battles with this pokemon',
 `losses` int(8) unsigned NOT NULL COMMENT 'Number of lost battles with this pokemon',
 `happiness` int(3) unsigned NOT NULL COMMENT 'Happiness',
 `level` int(3) unsigned NOT NULL COMMENT 'Level',
 `xp` int(6) unsigned NOT NULL COMMENT 'XP',
 `boost_hp` int(2) unsigned NOT NULL COMMENT 'Hp boost of the pokemon',
 `boost_attack` int(2) unsigned NOT NULL COMMENT 'Attack boost of the pokemon',
 `boost_defense` int(2) unsigned NOT NULL COMMENT 'Defense boost of the pokemon',
 `boost_speed` int(2) unsigned NOT NULL COMMENT 'Speed boost of the pokemon',
 `boost_special_attack` int(2) unsigned NOT NULL COMMENT 'Special attack boost of the pokemon',
 `boost_special_defense` int(2) unsigned NOT NULL COMMENT 'Special defense boost of the pokemon',
 `iv_hp` int(2) unsigned NOT NULL COMMENT 'Hp IV of the pokemon',
 `iv_attack` int(2) unsigned NOT NULL COMMENT 'Attack IV of the pokemon',
 `iv_defense` int(2) unsigned NOT NULL COMMENT 'Defense IV of the pokemon',
 `iv_speed` int(2) unsigned NOT NULL COMMENT 'Speed IV of the pokemon',
 `iv_special_attack` int(2) unsigned NOT NULL COMMENT 'Special attack IV of the pokemon',
 `iv_special_defense` int(2) unsigned NOT NULL COMMENT 'Special defense IV of the pokemon',
 `normal_action_points` int(4) unsigned NOT NULL COMMENT 'Action points of the pokemon when not used',
 `daily_remaining_action_points_increase` int(4) unsigned NOT NULL COMMENT 'Action points to add daily to remaining action points of the pokemon when not used',
 `remaining_action_points` int(4) unsigned NOT NULL COMMENT 'Remaining action points of the pokemon',
 `learned_attack_1_id` int(4) unsigned NOT NULL COMMENT 'Learned attack 1 ID (Foreign)',
 `learned_attack_2_id` int(4) unsigned NULL COMMENT 'Learned attack 2 ID (Foreign)',
 `learned_attack_3_id` int(4) unsigned NULL COMMENT 'Learned attack 3 ID (Foreign)',
 `learned_attack_4_id` int(4) unsigned NULL COMMENT 'Learned attack 4 ID (Foreign)',
 CONSTRAINT FK_USER_POKEMONS_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_USER_LENDER_ID FOREIGN KEY (user_lender_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_USER_BID_ID FOREIGN KEY (bid_user) REFERENCES users(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_USER_STORAGE_BOX_ID FOREIGN KEY (user_storage_box_id) REFERENCES user_storage_boxes(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_POKEMON_ID FOREIGN KEY (pokemon_id) REFERENCES pokemons(id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_POKEMON_EXCHANGE_ID FOREIGN KEY (pokemon_exchange_id) REFERENCES pokemons(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_GENDER_ID FOREIGN KEY (gender_id) REFERENCES genders(id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMONS_HELD_ITEM_ID FOREIGN KEY (held_item_id) REFERENCES items(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMON_LEARN_ATTACK_1 FOREIGN KEY (learned_attack_1_id) REFERENCES attacks(id) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMON_LEARN_ATTACK_2 FOREIGN KEY (learned_attack_2_id) REFERENCES attacks(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMON_LEARN_ATTACK_3 FOREIGN KEY (learned_attack_3_id) REFERENCES attacks(id) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT FK_USER_POKEMON_LEARN_ATTACK_4 FOREIGN KEY (learned_attack_4_id) REFERENCES attacks(id) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE users ADD CONSTRAINT FK_USERS_CURRENT_POKEMON_ID FOREIGN KEY (current_pokemon_id) REFERENCES user_pokemons(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE users ADD CONSTRAINT FK_USERS_BREEDING_POKEMON_1_ID FOREIGN KEY (breeding_pokemon_1_id) REFERENCES user_pokemons(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE users ADD CONSTRAINT FK_USERS_BREEDING_POKEMON_2_ID FOREIGN KEY (breeding_pokemon_2_id) REFERENCES user_pokemons(id) ON DELETE SET NULL ON UPDATE CASCADE;

-- TODO: Add fight pokemon info (JSON data?) => participe, id_combat, poison, poison_grave, gel, paralyse, brule, dodo, fin_dodo

COMMIT;
