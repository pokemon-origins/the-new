START TRANSACTION;

-- Regions
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Region ID (Primary key)',
  `available` bool NOT NULL COMMENT 'Is the region used in the game?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

INSERT INTO `regions` (`id`, `available`) VALUES (1, true), (2, true), (3, true), (4, true), (5, true), (6, false), (7, false), (8, false), (9, false);

CREATE TABLE IF NOT EXISTS `regions_translation` (
  `region_id` int(1) unsigned NOT NULL COMMENT 'Region ID (Foreign)',
  `language_code` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `name` varchar(50) NOT NULL COMMENT 'Region name',
  `description` text NOT NULL COMMENT 'Region description',
  CONSTRAINT FK_REGION FOREIGN KEY (region_id) REFERENCES regions(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_REGION_LANGUAGE FOREIGN KEY (language_code) REFERENCES languages(code) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`region_id`, `language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `regions_translation` (`region_id`, `language_code`, `name`, `description`) VALUES
  (1, 'EN', 'Kanto', 'Kanto is located east of Johto, and as revealed by the radio show Sinnoh Sound, is located south of Sinnoh. All cities in Kanto are named after colors (Viridian City, Lavender Town, Indigo Plateau, Pallet Town,  etc.).'),
  (1, 'FR', 'Kanto', 'Kanto est située à l\'est de Johto, et comme révélé par l\'émission de radio Sinnoh Sound, est située au sud de Sinnoh. Toutes les villes de Kanto portent des noms de couleurs (Jadielle, Lavanville, Plateau Indigo, Bourg Palette,  etc.).'),
  (2, 'EN', 'Johto', 'Johto is located west of Kanto, and as revealed by the radio show Sinnoh Sound, is located south of Sinnoh. Johto and Kanto are part of a large landmass, with everything west of Indigo Plateau falling in the Johto region.'),
  (2, 'FR', 'Johto', 'Johto est située à l\'ouest de Kanto, et comme révélé par l\'émission de radio Sinnoh Sound, est située au sud de Sinnoh. Johto et Kanto font partie d\'une grande masse continentale, tout ce qui se trouve à l\'ouest du plateau Indigo tombant dans la région de Johto.'),
  (3, 'EN', 'Hoenn', 'Hoenn is located south of Sinnoh. It is inspired by the real-world Japanese main island of Kyushu. The names of most of the cities in Hoenn are made of two words put together rather than colors or plants as Kanto and Johto had done.'),
  (3, 'FR', 'Hoenn', 'Hoenn est située au sud de Sinnoh. Elle s\'inspire de l\'île principale japonaise de Kyushu. Les noms de la plupart des villes de Hoenn sont constituées de deux mots réunis plutôt que de couleurs ou de plantes comme dans les régions de Kanto et Johto.'),
  (4, 'EN', 'Sinnoh', 'Sinnoh is located north of Kanto, Johto, and Hoenn. It is composed of the large mainland, the north-eastern island of the Battle Zone, and several smaller islands on both sides of the region. Most of Sinnoh\'s routes are on land, having very few water routes, in vast contrast to Hoenn. The region is divided by Mt. Coronet, roughly comparable to how the continent that Kanto and Johto make up is essentially a contiguous region.'),
  (4, 'FR', 'Sinnoh', 'Sinnoh est située au nord de Kanto, Johto et Hoenn. Elle est composée du grand continent, de l\'île du nord-est de la zone de combat et de plusieurs petites îles des deux côtés de la région. La plupart des routes de Sinnoh sont sur la terre, ayant très peu de routes maritimes, dans le vaste contraste avec Hoenn. La région est divisée par le mont. Couronné, à peu près comparable à la façon dont le continent que Kanto et Johto composent est essentiellement une région contiguë.'),
  (5, 'EN', 'Sevii Islands', 'The Sevii Islands are an archipelago near the Kanto region consisting of nine large islands and several small islands surrounding them. They are located far south of the Kanto mainland.'),
  (5, 'FR', 'Îles Sevii', 'Les îles Sevii sont un archipel près de la région de Kanto composé de neuf grandes îles et de plusieurs petites îles qui les entourent. Ils sont situés loin au sud du continent Kanto.'),
  (6, 'EN', 'Unova', 'Unova is far away from the four other large regions, and the Pokémon which inhabit Unova are diverse and different from those of Kanto, Johto, Hoenn, and Sinnoh. It is geographically based on New York City.'),
  (6, 'FR', 'Unys', 'Unova est loin des quatre autres grandes régions, et les Pokémon qui peuplent Unys sont différents et différents de ceux de Kanto, Johto, Hoenn et Sinnoh. La région est basée sur la ville de New York.'),
  (7, 'EN', 'Kalos', 'The Kalos region is shaped like a five-pointed star. It features a vast network of rivers and waterways snaking through much of its landscape, cities and towns.'),
  (7, 'FR', 'Kalos', 'La région de Kalos a la forme d\'une étoile à cinq branches. Elle dispose d\'un vaste réseau de rivières et de voies navigables à travers une grande partie de la région, des villes et des villages.'),
  (8, 'EN', 'Alola', 'The Alola region is made of up four natural islands and one artificial island. It is a popular resort destination and attracts a lot of tourists from other regions. In Alola, humans and Pokémon coexist in a very close relationship.'),
  (8, 'FR', 'Alola', 'La région d\'Alola est constituée de quatre îles naturelles et d\'une île artificielle. C\'est une destination de villégiature populaire qui attire beaucoup de touristes d\'autres régions. Dans cette région, les humains et les Pokémon ont une relation très étroite.'),
  (9, 'EN', 'Orange Archipelago', 'The Orange Archipelago consists of a large chain of tropical islands that is located south of Kanto.'),
  (9, 'FR', 'Archipel Orange', 'L\'archipel Orange est constituée de nombreuses îles tropicales situées au sud de la région de Kanto.');

COMMIT;
