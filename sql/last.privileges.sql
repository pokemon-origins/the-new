START TRANSACTION;

-- Read only on Languages tables
REVOKE ALL ON languages FROM PUBLIC;
GRANT SELECT ON languages TO PUBLIC;


-- Read only on Attack tables
REVOKE ALL ON stats FROM PUBLIC;
GRANT SELECT ON stats TO PUBLIC;

REVOKE ALL ON stats_translation FROM PUBLIC;
GRANT SELECT ON stats_translation TO PUBLIC;

REVOKE ALL ON attack_classes FROM PUBLIC;
GRANT SELECT ON attack_classes TO PUBLIC;

REVOKE ALL ON attack_classes_translation FROM PUBLIC;
GRANT SELECT ON attack_classes_translation TO PUBLIC;

REVOKE ALL ON types FROM PUBLIC;
GRANT SELECT ON types TO PUBLIC;

REVOKE ALL ON types_translation FROM PUBLIC;
GRANT SELECT ON types_translation TO PUBLIC;

REVOKE ALL ON type_strengths FROM PUBLIC;
GRANT SELECT ON type_strengths TO PUBLIC;

REVOKE ALL ON attack_effects FROM PUBLIC;
GRANT SELECT ON attack_effects TO PUBLIC;

REVOKE ALL ON attack_effects_translation FROM PUBLIC;
GRANT SELECT ON attack_effects_translation TO PUBLIC;

REVOKE ALL ON attack_targets FROM PUBLIC;
GRANT SELECT ON attack_targets TO PUBLIC;

REVOKE ALL ON attack_targets_translation FROM PUBLIC;
GRANT SELECT ON attack_targets_translation TO PUBLIC;

REVOKE ALL ON attacks FROM PUBLIC;
GRANT SELECT ON attacks TO PUBLIC;

REVOKE ALL ON attacks_translation FROM PUBLIC;
GRANT SELECT ON attacks_translation TO PUBLIC;


-- Read only on Item tables
REVOKE ALL ON item_pockets FROM PUBLIC;
GRANT SELECT ON item_pockets TO PUBLIC;

REVOKE ALL ON item_pockets_translation FROM PUBLIC;
GRANT SELECT ON item_pockets_translation TO PUBLIC;

REVOKE ALL ON item_categories FROM PUBLIC;
GRANT SELECT ON item_categories TO PUBLIC;

REVOKE ALL ON item_categories_translation FROM PUBLIC;
GRANT SELECT ON item_categories_translation TO PUBLIC;

REVOKE ALL ON items FROM PUBLIC;
GRANT SELECT ON items TO PUBLIC;

REVOKE ALL ON items_translation FROM PUBLIC;
GRANT SELECT ON items_translation TO PUBLIC;

REVOKE ALL ON berries FROM PUBLIC;
GRANT SELECT ON berries TO PUBLIC;

REVOKE ALL ON item_flags FROM PUBLIC;
GRANT SELECT ON item_flags TO PUBLIC;

REVOKE ALL ON item_flags_translation FROM PUBLIC;
GRANT SELECT ON item_flags_translation TO PUBLIC;

REVOKE ALL ON items_flags_map FROM PUBLIC;
GRANT SELECT ON items_flags_map TO PUBLIC;

REVOKE ALL ON items_attacks_map FROM PUBLIC;
GRANT SELECT ON items_attacks_map TO PUBLIC;

-- Read only on Item tables
REVOKE ALL ON genders FROM PUBLIC;
GRANT SELECT ON genders TO PUBLIC;

REVOKE ALL ON genders_translation FROM PUBLIC;
GRANT SELECT ON genders_translation TO PUBLIC;

REVOKE ALL ON pokemon_shapes FROM PUBLIC;
GRANT SELECT ON pokemon_shapes TO PUBLIC;

REVOKE ALL ON pokemon_shapes_translation FROM PUBLIC;
GRANT SELECT ON pokemon_shapes_translation TO PUBLIC;

REVOKE ALL ON pokemon_habitats FROM PUBLIC;
GRANT SELECT ON pokemon_habitats TO PUBLIC;

REVOKE ALL ON pokemon_habitats_translation FROM PUBLIC;
GRANT SELECT ON pokemon_habitats_translation TO PUBLIC;

REVOKE ALL ON time_of_day FROM PUBLIC;
GRANT SELECT ON time_of_day TO PUBLIC;

REVOKE ALL ON time_of_day_translation FROM PUBLIC;
GRANT SELECT ON time_of_day_translation TO PUBLIC;

REVOKE ALL ON colors FROM PUBLIC;
GRANT SELECT ON colors TO PUBLIC;

REVOKE ALL ON colors_translation FROM PUBLIC;
GRANT SELECT ON colors_translation TO PUBLIC;

REVOKE ALL ON pokemons FROM PUBLIC;
GRANT SELECT ON pokemons TO PUBLIC;

REVOKE ALL ON pokemons_translation FROM PUBLIC;
GRANT SELECT ON pokemons_translation TO PUBLIC;

REVOKE ALL ON pokemon_learn_attack_methods FROM PUBLIC;
GRANT SELECT ON pokemon_learn_attack_methods TO PUBLIC;

REVOKE ALL ON pokemon_learn_attack_methods_translation FROM PUBLIC;
GRANT SELECT ON pokemon_learn_attack_methods_translation TO PUBLIC;

REVOKE ALL ON pokemon_learn_attacks FROM PUBLIC;
GRANT SELECT ON pokemon_learn_attacks TO PUBLIC;

-- Read only on Tips tables
REVOKE ALL ON tips FROM PUBLIC;
GRANT SELECT ON tips TO PUBLIC;

REVOKE ALL ON tips_translation FROM PUBLIC;
GRANT SELECT ON tips_translation TO PUBLIC;

-- Read only on Achievements tables
REVOKE ALL ON achievements FROM PUBLIC;
GRANT SELECT ON achievements TO PUBLIC;

REVOKE ALL ON achievements_translation FROM PUBLIC;
GRANT SELECT ON achievements_translation TO PUBLIC;

-- Read only on Region tables
REVOKE ALL ON regions FROM PUBLIC;
GRANT SELECT ON regions TO PUBLIC;

REVOKE ALL ON regions_translation FROM PUBLIC;
GRANT SELECT ON regions_translation TO PUBLIC;

-- Read only on Badge tables
REVOKE ALL ON badges FROM PUBLIC;
GRANT SELECT ON badges TO PUBLIC;

REVOKE ALL ON badges_translation FROM PUBLIC;
GRANT SELECT ON badges_translation TO PUBLIC;

COMMIT;
