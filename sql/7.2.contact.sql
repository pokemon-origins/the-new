START TRANSACTION;

-- contact and report
CREATE TABLE IF NOT EXISTS `contact_and_report` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Contact ID (Primary key)',
  `creation_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Creation date of the contact/report',
  `title` varchar(100) NOT NULL COMMENT 'Title',
  `message` text NOT NULL COMMENT 'Message',
  `sender_user_id` int(10) unsigned NOT NULL COMMENT 'User id who want to contact/report (Foreign key)',
  `reported_user_id` int(10) unsigned NULL COMMENT 'Potential reported user id (Foreign key)',
  CONSTRAINT FK_CONTACT_AND_REPORT_SENDER_ID FOREIGN KEY (sender_user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_CONTACT_AND_REPORT_REPORTED_ID FOREIGN KEY (reported_user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

COMMIT;
