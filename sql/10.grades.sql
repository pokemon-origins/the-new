START TRANSACTION;

-- Grades
CREATE TABLE IF NOT EXISTS `grades` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User holding the grade / ID (Foreign)',
  `type_id` int(2) unsigned NOT NULL COMMENT 'Pokemon type ID (Foreign)',
  `received_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date when the last user received the grade',
  `number_of_pokemons` int(1) unsigned NOT NULL COMMENT 'Number of pokemons for this grade',
  `max_pokemon_level` int(3) unsigned NOT NULL COMMENT 'Max level for pokemons to fight for the grade',
  CONSTRAINT FK_GRADES_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_GRADES_TYPE_ID FOREIGN KEY (type_id) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (number_of_pokemons, max_pokemon_level, type_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TODO: Create a SQL "trigger" to automatically store old grades into a backup table

COMMIT;
