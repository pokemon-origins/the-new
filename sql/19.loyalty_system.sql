START TRANSACTION;

-- Casino
CREATE TABLE IF NOT EXISTS `loyalty_system` (
  `id` bool PRIMARY KEY DEFAULT TRUE COMMENT 'Only one row allowed for this table (Primary Key, Constraint unique row)',
  `reward_day_1_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 1 / ID (Foreign)',
  `reward_day_2_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 2 / ID (Foreign)',
  `reward_day_3_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 3 / ID (Foreign)',
  `reward_day_4_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 4 / ID (Foreign)',
  `reward_day_5_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 5 / ID (Foreign)',
  `reward_day_6_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 6 / ID (Foreign)',
  `reward_day_7_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 7 / ID (Foreign)',
  `reward_day_8_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 8 / ID (Foreign)',
  `reward_day_9_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 9 / ID (Foreign)',
  `reward_day_10_id` int(10) unsigned NOT NULL COMMENT 'Reward for day 10 / ID (Foreign)',
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_1_ID FOREIGN KEY (reward_day_1_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_2_ID FOREIGN KEY (reward_day_2_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_3_ID FOREIGN KEY (reward_day_3_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_4_ID FOREIGN KEY (reward_day_4_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_5_ID FOREIGN KEY (reward_day_5_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_6_ID FOREIGN KEY (reward_day_6_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_7_ID FOREIGN KEY (reward_day_7_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_8_ID FOREIGN KEY (reward_day_8_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_9_ID FOREIGN KEY (reward_day_9_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_LOYALTY_SYSTEM_REWARD_10_ID FOREIGN KEY (reward_day_10_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT CASINO_ONE_ROW_ID CHECK (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `loyalty_system` (`id`, `reward_day_1_id`, `reward_day_2_id`, `reward_day_3_id`, `reward_day_4_id`, `reward_day_5_id`, `reward_day_6_id`, `reward_day_7_id`, `reward_day_8_id`, `reward_day_9_id`, `reward_day_10_id`) VALUES
  (1, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26);

COMMIT;
