START TRANSACTION;

-- News
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'News ID (Primary key)',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Publication date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `news` (`id`, `date`) VALUES (1, '2017-11-11 20:57:04');

CREATE TABLE IF NOT EXISTS `news_translation` (
  `news_id` int(6) unsigned NOT NULL COMMENT 'News ID (Foreign)',
  `language_code` char(2) NOT NULL COMMENT 'Language ID (Foreign)',
  `title` varchar(255) NOT NULL COMMENT 'Title of the news',
  `body` text NOT NULL COMMENT 'Body of the news',
  CONSTRAINT FK_NEWS FOREIGN KEY (news_id) REFERENCES news(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_NEWS_LANGUAGE FOREIGN KEY (language_code) REFERENCES languages(code) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `news_translation` (`news_id`, `language_code`, `title`, `body`) VALUES (1, 'EN', 'Launch of the website', 'The site is now online!\r\n\r\nGood game everyone!'), (1, 'FR', 'Lancement du site', 'Le site est maintenant en ligne!\r\n\r\nBon jeu à tous!\r\n');

COMMIT;
