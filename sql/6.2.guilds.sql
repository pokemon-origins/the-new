START TRANSACTION;

-- Guilds
CREATE TABLE IF NOT EXISTS `guilds` (
 `id` int(6) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Guild ID (Primary key)',
 `name` varchar(20) NOT NULL COMMENT 'Guild name',
 `description` varchar(255) COMMENT 'Guild description',
 `points` int(6) unsigned NOT NULL COMMENT 'Points to use for specific actions'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- Users in the guild
CREATE TABLE IF NOT EXISTS `guilds_users_map` (
  `guild_id` int(6) unsigned NOT NULL COMMENT 'Guild',
  `user_id` int(10) unsigned NOT NULL COMMENT 'User',
  `is_admin` boolean NOT NULL COMMENT 'Is the user admin of the guild',
  CONSTRAINT FK_GUILDS_USERS_MAP_GUILD_ID FOREIGN KEY (guild_id) REFERENCES guilds(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_GUILDS_USERS_MAP_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY user_id
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

COMMIT;
