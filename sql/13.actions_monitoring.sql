-- Big brother (check critical actions made by animators)
CREATE TABLE IF NOT EXISTS `actions_monitoring` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'User who performed the action / ID (Foreign)',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date when the action was performed',
  `justification` varchar(255) NOT NULL COMMENT 'Explanation of the action from the user',
  `receiver_id` int(10) unsigned NOT NULL COMMENT 'User who received the gift / ID (Foreign)',
  `reward_id` int(10) unsigned NOT NULL COMMENT 'Given reward',
  CONSTRAINT FK_ACTIONS_MONITORING_USER_ID FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ACTIONS_MONITORING_RECEIVER_ID FOREIGN KEY (receiver_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_ACTIONS_MONITORING_REWARD_ID FOREIGN KEY (reward_id) REFERENCES rewards(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

