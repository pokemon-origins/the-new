START TRANSACTION;

-- Casino
CREATE TABLE IF NOT EXISTS `casino` (
  `id` bool PRIMARY KEY DEFAULT TRUE COMMENT 'Only one row allowed for this table (Primary Key, Constraint unique row)',
  `gold_casino_reward_1_id` int(10) unsigned NOT NULL COMMENT 'Reward 1 ID in Gold casino (Foreign)',
  `gold_casino_reward_2_id` int(10) unsigned NOT NULL COMMENT 'Reward 2 ID in Gold casino (Foreign)',
  `gold_casino_reward_3_id` int(10) unsigned NOT NULL COMMENT 'Reward 3 ID in Gold casino (Foreign)',
  `gold_casino_reward_4_id` int(10) unsigned NOT NULL COMMENT 'Reward 4 ID in Gold casino (Foreign)',
  `gold_casino_reward_5_id` int(10) unsigned NOT NULL COMMENT 'Reward 5 ID in Gold casino (Foreign)',
  `gold_casino_reward_6_id` int(10) unsigned NOT NULL COMMENT 'Reward 6 ID in Gold casino (Foreign)',
  `dollar_casino_reward_1_id` int(10) unsigned NOT NULL COMMENT 'Reward 1 ID in Dollar casino (Foreign)',
  `dollar_casino_reward_2_id` int(10) unsigned NOT NULL COMMENT 'Reward 2 ID in Dollar casino (Foreign)',
  `dollar_casino_reward_3_id` int(10) unsigned NOT NULL COMMENT 'Reward 3 ID in Dollar casino (Foreign)',
  `dollar_casino_reward_4_id` int(10) unsigned NOT NULL COMMENT 'Reward 4 ID in Dollar casino (Foreign)',
  `dollar_casino_reward_5_id` int(10) unsigned NOT NULL COMMENT 'Reward 5 ID in Dollar casino (Foreign)',
  `dollar_casino_reward_6_id` int(10) unsigned NOT NULL COMMENT 'Reward 6 ID in Dollar casino (Foreign)',
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_1_ID FOREIGN KEY (gold_casino_reward_1_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_2_ID FOREIGN KEY (gold_casino_reward_2_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_3_ID FOREIGN KEY (gold_casino_reward_3_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_4_ID FOREIGN KEY (gold_casino_reward_4_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_5_ID FOREIGN KEY (gold_casino_reward_5_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_GOLD_CASINO_REWARD_6_ID FOREIGN KEY (gold_casino_reward_6_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_1_ID FOREIGN KEY (dollar_casino_reward_1_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_2_ID FOREIGN KEY (dollar_casino_reward_2_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_3_ID FOREIGN KEY (dollar_casino_reward_3_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_4_ID FOREIGN KEY (dollar_casino_reward_4_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_5_ID FOREIGN KEY (dollar_casino_reward_5_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT FK_CASINO_DOLLAR_CASINO_REWARD_6_ID FOREIGN KEY (dollar_casino_reward_6_id) REFERENCES rewards(id) ON UPDATE CASCADE,
  CONSTRAINT CASINO_ONE_ROW_ID CHECK (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `casino` (`id`, `gold_casino_reward_1_id`, `gold_casino_reward_2_id`, `gold_casino_reward_3_id`, `gold_casino_reward_4_id`, `gold_casino_reward_5_id`, `gold_casino_reward_6_id`, `dollar_casino_reward_1_id`, `dollar_casino_reward_2_id`, `dollar_casino_reward_3_id`, `dollar_casino_reward_4_id`, `dollar_casino_reward_5_id`, `dollar_casino_reward_6_id`) VALUES
  (1, 11, 12, 13, 14, 15, 16, 5, 6, 7, 8, 9, 10);

CREATE TABLE IF NOT EXISTS `casino_all_rewards` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Casino other reward ID (Primary key)',
  `is_dollar_casino` bool NOT NULL COMMENT 'Is it for dollar casino or gold casino?',
  `reward_number` int(1) unsigned NOT NULL COMMENT 'Reward number (1 to 6)',
  `reward_id` int(10) unsigned NOT NULL COMMENT 'Reward ID (Foreign)',
  CONSTRAINT FK_CASINO_ALL_REWARDS_REWARD_ID FOREIGN KEY (reward_id) REFERENCES rewards(id) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- Add base rewards for casino
INSERT INTO `casino_all_rewards` (`id`, `is_dollar_casino`, `reward_number`, `reward_id`) VALUES
  (1, 1, 1, 5),
  (2, 1, 2, 6),
  (3, 1, 3, 7),
  (4, 1, 4, 8),
  (5, 1, 5, 9),
  (6, 1, 6, 10),
  (7, 0, 1, 11),
  (8, 0, 2, 12),
  (9, 0, 3, 13),
  (10, 0, 4, 14),
  (11, 0, 5, 15),
  (12, 0, 6, 16);

-- Add additional rewards for casino

COMMIT;
