START TRANSACTION;

-- Rewards
CREATE TABLE IF NOT EXISTS `rewards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Reward ID (Primary key)',
  `item_id` int(4) unsigned NULL COMMENT 'Item ID won (Foreign)',
  `pokemon_id` int(4) unsigned NULL COMMENT 'Pokemon won (Foreign)',
  `is_shiny` bool NOT NULL COMMENT 'Is the pokemon shiny?',
  `dollar` int(5) unsigned NOT NULL COMMENT 'Dollar won',
  `gold` int(3) unsigned NOT NULL COMMENT 'Gold won',
  `points` int(2) unsigned NOT NULL COMMENT 'Points won',
  CONSTRAINT FK_REWARDS_ITEM_ID FOREIGN KEY (item_id) REFERENCES items(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_REWARDS_POKEMON_ID FOREIGN KEY (pokemon_id) REFERENCES pokemons(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- Add rewards for achievements table
INSERT INTO `rewards` (`id`, `item_id`, `pokemon_id`, `is_shiny`, `dollar`, `gold`, `points`) VALUES
  (1, NULL, NULL, 0, 0, 5, 0),
  (2, NULL, NULL, 0, 0, 20, 0),
  (3, NULL, NULL, 0, 0, 40, 0),
  (4, NULL, NULL, 0, 0, 75, 0);

-- Add base rewards for pokedollar casino
INSERT INTO `rewards` (`id`, `item_id`, `pokemon_id`, `is_shiny`, `dollar`, `gold`, `points`) VALUES
  (5, NULL, NULL, 0, 250, 0, 0),
  (6, NULL, NULL, 0, 1000, 0, 0),
  (7, 1, NULL, 0, 0, 0, 0),
  (8, NULL, 137, 0, 0, 0, 0),
  (9, NULL, 113, 0, 0, 0, 0),
  (10, NULL, 137, 1, 0, 0, 0);

-- Add base rewards for gold casino
INSERT INTO `rewards` (`id`, `item_id`, `pokemon_id`, `is_shiny`, `dollar`, `gold`, `points`) VALUES
  (11, NULL, NULL, 0, 0, 5, 0),
  (12, NULL, NULL, 0, 0, 25, 0),
  (13, 319, NULL, 0, 0, 0, 0),
  (14, NULL, 143, 0, 0, 0, 0),
  (15, NULL, 122, 0, 0, 0, 0),
  (16, NULL, 131, 1, 0, 0, 0);

-- Add rewards for loyalty system
INSERT INTO `rewards` (`id`, `item_id`, `pokemon_id`, `is_shiny`, `dollar`, `gold`, `points`) VALUES
  (17, NULL, NULL, 0, 200, 0, 0),
  (18, 3, NULL, 0, 0, 0, 0),
  (19, NULL, NULL, 0, 0, 1, 0),
  (20, NULL, NULL, 0, 1000, 0, 0),
  (21, NULL, NULL, 0, 0, 2, 0),
  (22, 2, NULL, 0, 0, 2, 0),
  (23, 32, NULL, 0, 1500, 5, 0),
  (24, 29, NULL, 0, 0, 0, 0),
  (25, 1, NULL, 0, 0, 0, 0),
  (26, 50, NULL, 0, 2000, 10, 0);

-- Missions
CREATE TABLE IF NOT EXISTS `missions` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Mission ID (Primary key)',
  `stat_id` int(1) unsigned NOT NULL COMMENT 'Stat ID (Foreign)',
  `difficulty` int(3) unsigned NOT NULL COMMENT 'Mission difficulty',
  `type_advantage_id` int(2) unsigned NOT NULL COMMENT 'Type advantage ID (Foreign)',
  `type_disadvantage_id` int(2) unsigned NOT NULL COMMENT 'Type disadvantage ID (Foreign)',
  `reward_id` int(10) unsigned NOT NULL COMMENT 'Reward ID (Foreign)',
  CONSTRAINT FK_MISSIONS_STAT_ID FOREIGN KEY (stat_id) REFERENCES stats(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_MISSIONS_TYPE_ADVANTAGE_ID FOREIGN KEY (type_advantage_id) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_MISSIONS_TYPE_DISADVANTAGE_ID FOREIGN KEY (type_disadvantage_id) REFERENCES types(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_MISSIONS_REWARD_ID FOREIGN KEY (reward_id) REFERENCES rewards(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

COMMIT;
