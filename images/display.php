<html>
  <body style="background-color:#E6E6FA">

    <?php
      # Get the right folder
      $basedir = "";
      if (isset($_GET["dir"]) AND !(strpos($_GET["dir"], "..") !== false) AND !(0 === strpos($_GET["dir"], "/")))
      {
        $basedir = $_GET["dir"];
        $basedir .= "/";
      }

      # Show images in the folder
      $images = glob("$basedir*.{jpg,png,gif,svg}", GLOB_BRACE);
      sort($images, SORT_NATURAL);

      if ($basedir == "")
      {
        echo "Base image directory:<br/>";
      }
      else
      {
        echo "$basedir:<br/>";
      }

      foreach($images as $image)
      {
        echo "<img src=\"$image\" alt=\"$image\"/>";
      }

      # Show subfolders
      echo "<br/><br/>Subfolders:<br/>";
      $dirs = glob("$basedir*", GLOB_ONLYDIR);
      foreach($dirs as $dir)
      {
              echo "<a href=\"display.php?dir=$dir\">$dir</a><br/>";
      }
    ?>
  </body>
</html>
