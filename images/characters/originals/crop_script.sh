for filename in *.png
do
  convert $filename -crop 68x72+1+1 ../down/$filename
  convert $filename -crop 68x72+69+1 ../down/frame1/$filename
  convert $filename -crop 68x72+205+1 ../down/frame2/$filename

  convert $filename -crop 68x72+1+73 ../left/$filename
  convert $filename -crop 68x72+69+73 ../left/frame1/$filename
  convert $filename -crop 68x72+205+73 ../left/frame2/$filename

  convert $filename -crop 68x72+1+145 ../right/$filename
  convert $filename -crop 68x72+69+145 ../right/frame1/$filename
  convert $filename -crop 68x72+205+145 ../right/frame2/$filename

  convert $filename -crop 68x72+1+217 ../up/$filename
  convert $filename -crop 68x72+69+217 ../up/frame1/$filename
  convert $filename -crop 68x72+205+217 ../up/frame2/$filename
done
