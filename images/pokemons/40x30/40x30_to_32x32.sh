for filename in *.png
do
  convert $filename -gravity south -splice 0x2 /tmp/tmp_$filename
  convert /tmp/tmp_$filename -crop 32x32+4+0 ../32x32/$filename
  rm /tmp/tmp_$filename
done
