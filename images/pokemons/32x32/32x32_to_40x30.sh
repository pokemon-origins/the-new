for filename in *.png
do
  convert -border 4x0 $filename /tmp/tmp_$filename
  convert /tmp/tmp_$filename -crop 40x30+0+2 ../40x30/$filename
  rm /tmp/tmp_$filename
done
