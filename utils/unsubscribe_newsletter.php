<!DOCTYPE HTML>
<html>
<body>
<?php

if (isset($_GET['email']))
{
  include_once("../include/user.php");

  $email = htmlspecialchars($_GET['email']);
  if (User::unsubscribeNewsletter($email))
  {
    echo "Your email address '".$email."' is now unsubscribed from our newsletter.</br>We will miss you :'(";
  }
  else
  {
    echo "Sorry, we could not unsubscribe your email '".$email."' from our newsletter.<br/>
          This is maybe because you are already unsubscribed or because of a techichnical error.<br/>
          If you still receive email after this moment, please contact us about the issue since this should not happen.";
  }
}
else
{
?>
Do you really want to unsubscribe from the newsletter?<br/>
If yes, please fill this form:<br/><br/>
<form method="get">
  E-mail: <input type="email" name="email"><br>
  <input type="submit">
</form>

<?php
}
?>
</body>
</html>
