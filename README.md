# Pokemon Origins website

This project was originally designed to replace the old version of [Pokemon-Origins Video-Game](https://gitlab.com/pokemon-origins/old-game) but was never finished due to copyright violation.
This website is composed of an API with authentication and a AngularJs website communicating with the API.

## Warnings

This website is not finished and is using a deprecated front-end technology (AngularJS).
The authentication is done in API side and is not perfect but is working. It may be easier to use an already done authentication plugin to do this in order to prevent potential side effects.

Due to the copyright violation, you should NOT publish this website nor content in a public website/game, doing so will be at your own risk.
This project is there only for information purpose and to not loose a lot of work.
There is a lot of content and the pokemon database is quite interesting to begin with a legit pokemon project.


# Install in Apache2 Debian server

## Base project

Copy the full project in `/var/www/pokemon-origins`


## Allow avatars to be uploaded by users

```sh
sudo chown -Rf www-data:www-data /var/www/pokemon-origins/images/avatars
```


## SQL database configuration

Add those lines in `/etc/mysql/my.cnf` (enables strict mode):

```sh
[mysqld]
key_buffer_size=128000000
sql-mode="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION,ERROR_FOR_DIVISION_BY_ZERO"

```

And then restart mysql service
```sh
service mysql restart
```

## Install database

Create an SQL user `pokemon_origins` with read/write access to `pokemon_origins` database (password: `sdopiOPIF65D2sg`)

Note: All those values can be edited in `include/database.php`.


## Phalcon API integration

### Install Phalcon API

```sh
curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install php7.0-phalcon
```

or

```sh
sudo apt-get install php5-dev php5-mysql gcc libpcre3-dev
git clone --depth=1 "git://github.com/phalcon/cphalcon.git"
cd cphalcon/build
sudo ./install
# Add "extension=phalcon.so" in php.ini
```

### Enable Rewrite for Apache2

```
sudo a2enmod rewrite
sudo /etc/init.d/apache2 restart
```


## Add project in Apache2

Copy `apache2/pokemon-origins.conf` file in `/etc/apache2/sites-available/` folder and send following command to enable the website: `sudo a2ensite pokemon-origins.conf`

The website is now ready to run.


# What was still to be done ?

Check `TODO.md` to find out what was still to be done.


