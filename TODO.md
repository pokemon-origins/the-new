Grades view (4h):

Show all grades in the game + link with rank view (rank of number of grades)
---------------------------------------------------------------------------
Noigrume factory (1d):

convert noigrume into balls if owned
--> directly in items view???

--> think of that logic for all game item -> maybe modal from items view...
---------------------------------------------------------------------------
Quizz view (1d):

 - Show user score + rank (in rank view?)
 - Show all not answered questions + potential responses
 - Show all questions already answered (+ earned points)
 - Allow user to answer questions
---------------------------------------------------------------------------
Achievements view (1h):

add this in rank view: sum(user_achievements->grade)


SELECT
  oua.user_id AS id,
  ou.login AS login,
  SUM(oua.grade) AS sscore
FROM users ou
  INNER JOIN user_achievements oua ON (oua.user_id = ou.id)
WHERE oua.user_id != 1 AND (SELECT SUM(grade) FROM user_achievements WHERE user_id = ou.id) >= (SELECT SUM(grade) FROM user_achievements WHERE user_id = 1)
GROUP BY oua.user_id
ORDER BY sscore ASC
LIMIT 10;

---------------------------------------------------------------------------
In owned Pokemon (3d):
 - Finish to implement the calculus of xp to level up
 - Check how to get stats from IV/loses/wins/base/lvl: https://bulbapedia.bulbagarden.net/wiki/Statistic
    IV: 1->15
    EV (effort value): win/lose (max 85/stat)
    HP = f1(base, IV, EV, lvl)
    Stats (exept HP) = f2(base, IV, EV, lvl)

 - List all pokemons with default bag in the "pokemons bag" view + in user view
 - Get a list of bags with number of pokemons inside
 - Per bag: Get all owned pokemons (id, name, level)
--> If selected (name, pokemon_id, pokemon_name, is_shiny, gender, level, xp, item, ...)
 - Get all possible evolutions (+how to evolve for each of them)
 - Evolve a pokemon (delete item if needed + check everything before doing it, can maybe be done in one query)
 - Get all learned attacks (item & level up)
 - Update learned attacks with item or available attacks
 - Get list of items that can be put on the pokemon
 - Sell pokemon (other 'modal' to confirm with the price)

Box system display: http://marceljuenemann.github.io/angular-drag-and-drop-lists/demo/#/types
----------------------------------------------------------------------------
Welcome popup (1d):

- Check if receive a recompense + take it and show them
  (number_days_connected_in_a_row + last_time_bonus_received)

If not, don't show popup
----------------------------------------------------------------------------
Subscription/Lost password/Login (4d):

- Update a forgotten password by specifying the email address (5h):
   --> send an email giving a static link to change pass (generated with hashed previous pass, id and subscription_date (only if user->is_activated=true)) that grant password update even without being connected.
       Send email requirements: https://www.w3schools.com/php/php_ref_mail.asp
       Send email function: https://www.w3schools.com/php/func_mail_mail.asp
   --> If user click on the link and that the link is correct, then allow user to change the password, else say something went wrong (already clicked?).

- Add a subscribe possibility (5h):
   --> send an email giving a static link (generated with hashed previous pass, id and subscription_date (only if user->is_activated=false)) to activate the user
   --> If user click on the link and that the link is correct, then activate the account, else say something went wrong (already clicked?).

- Add captcha in Login/Subscription/Lost password

--------------------------------------------------------------------------------
Market (2w):
 - Item market (5h): (show items by category)
        1) In one transaction:
          - Take the money from the buyer and give to the seller: payWithPokedollar/payWithGold
          - Update the user items:
            IF MORE THAN ONE FIELD AFECTED ON: UPDATE user_items SET quantity_in_sell=quantity_in_sell-10 WHERE item_id=10 AND user_id=1;
              INSERT INTO user_items(user_id, item_id, quantity) VALUES (2, 10, 10) ON DUPLICATE KEY UPDATE quantity_in_sell=quantity_in_sell+10;
            ELSE:
              USER IS TRYING TO CREATE ITEMS, NOT SELLING THEM
        2) Inform the seller its item is sold??? HOW??? <--- TODO: user_notifications table? (to show during login)
 - Pokemon market (5h):
 - Share a pokemon (5h):
 - Exchange a pokemon (5h):
 - Enchere pokemon (10h)
 - Item shop (5h): (show items by category)
 - Gold mine (10h): Add paypal account / define earned gold/pokemons if pay with real money
      - Checkout with paypal button https://github.com/paypal/paypal-checkout/blob/master/demo/angular.htm
      - Verify that the action was done and add it in the database to not duplicate: https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/get-notifications/
      - Give gold/pokemons to user + page confirming the action

Important note:
Have a function to be sure that user have only one item which is not countable:
SELECT DISTINCT
    i.id,
    it.name,
    it.description
FROM
    items i
    INNER JOIN items_translation it
WHERE
    NOT EXISTS (
       SELECT 1 FROM items_flags_map ifm_test WHERE i.id = ifm_test.item_id and ifm_test.item_flag_id = 1
    )
    AND it.item_id = i.id AND it.language_code = "FR"
--------------------------------------------------------------------------
Elevage (3d):

birth (to update in the "birth" view):
SELECT number_of_births, end_speed_up_birth_bonus FROM users WHERE id = 1;
Integrate IV calulation in the birth system

tester algo
--------------------------------------------------------------------------
Missions (3d):

(+UPDATE successful_missions FROM users)

--------------------------------------------------------------------------
Blind battle (3d):

(+ UPDATE A_class_victories, B_class_victories, C_class_victories, D_class_victories FROM users)
--------------------------------------------------------------------------
Tombola view (gambling view) (1d):

 - Show user bought tickets
 - Show number of already bought tickets
 - Show prize of the week (<-- Create the associated sql table)
 - Allow user to buy tickets
--------------------------------------------------------------------------
Plantation de baies (2d):
 - Show current planted (user_berries_plantations with berry_id != null)
 - Show number of owned plantations (number of user_berries_plantations rows)
 - Allow user to haverest
 - Allow user to buy more fields with gold (max 5: first: free, second: 50 -> 200 -> 500 -> 2000)
 - Allow user to plant berries if owned
--------------------------------------------------------------------------
Newsletters (mails) (3d)

--------------------------------------------------------------------------
Guildes (3d):

Points in guild? (need points to edit name/description/...)

 - Create a view with all guilds and users inside
 - Create a way to add a new guild (pay x gold)
 - Add user that requested to be in the guild, edit description, edit name
 - Remove a guild if admin
 - Give admin right to an other user (with warning only one admin is allowed)
 - Request to be in a guild / Delete request

Guild(s) where the user is in:
SELECT
  g.id AS id,
  g.name AS name,
  g.description AS description
FROM guilds g
  INNER JOIN guilds_users_map gum ON g.id = gum.guild_id
WHERE gum.user_id = :user_id;

List of users in a specific guild:
SELECT
  gum.user_id AS id,
  gum.is_admin AS is_admin,
  u.login AS login
FROM guilds_users_map gum
  INNER JOIN users u ON u.id = gum.user_id
WHERE gum.guild_id = :guild_id;

--------------------------------------------------------------------------
Logic behind fights:

--------------------------------------------------------------------------
Tournois mensuel:

--------------------------------------------------------------------------
Messagerie (1w)

+-------------+
| SEARCH BAR  | <-- Already implemented 
|-------------|
| Private msg | <-- With user status, user ID, user name, channel ID (only last month messages)
|             |
|-------------|
| See all btn | <-- Button to show all private conversation (> 1 month)
|-------------|
| Guild chan  | <-- Show the guild channel if associated to the user (channel id, guild name, guild id)
|-------------|
| Public chan | <-- Show all public channel (channel id, channel name, is_read_only
|             |
+-------------+

 - Public channels:
SELECT id, name, is_read_only FROM channels WHERE is_public = 1;

 - Guild channels allowed to the user:
SELECT
  c.id AS channel_id,
  c.name AS channel_name,
  g.name AS guild_name
FROM channels c
  INNER JOIN guilds_users_map gum ON gum.guild_id = c.guild_id
  INNER JOIN guilds g on g.id = c.guild_id
WHERE
  gum.user_id = :user_id AND
  c.is_public = 0 AND
  c.guild_id IS NULL;

All private channels (1-1) seen in the last month (delete last where element to have all private messages):
SELECT
  c.id AS channel_id,
  c.name AS channel_name,
  other_u.id AS other_user_id,
  other_u.login AS other_user_name
FROM channels c
  INNER JOIN channel_users cu ON cu.channel_id = c.id
  INNER JOIN channel_users other_cu ON other_cu.channel_id = c.id
  INNER JOIN users other_u ON other_u.id = other_cu.user_id
WHERE
  c.is_public = 0 AND
  c.guild_id IS NULL AND
  cu.user_id = :user_id AND
  other_cu.user_id != :user_id AND
  cu.last_seen >= date_sub(now(), interval 1 month);

Check if message received:
SELECT DISTINCT cu.channel_id
FROM channel_users cu
  INNER JOIN channel_messages cm ON cm.user_id = cu.user_id
WHERE
  cu.user_id = 1 AND
  cm.time > cu.last_seen;
---------------------------------------------------------------------------
Protection (4h):

- Add SQL table for failed connection attempts (IP, last_try, number_of_time)
  -> delete the row if CURRENT_DAY() - nb_of_time * ONE_DAY - DAY(last_try) > 0
  -> Can't connect if nb_of_time > 5 (until row is deleted)
----------------------------------------------------------------------------
Improve IP address cheat detection (4h):
- Add list of IPs associated with each user instead of static IP in users table:
 -> map(IP, user_id, date), (IP, user_id) being a primary key
 (update the value at connection time)
 (delete deprecated IPs if date older than a month)
---------------------------------------------------------------------------
Fix SQL issues:
 - improve "items_flags_map" (missing NUMEROUS VALUES): 5d
 - Fix pokemon attacks duplicates (2h)
 - attack_effects_translation EN/FR (3d)
 - items_translation (FR) (2d)
---------------------------------------------------------------------------
Cron + mysql trigger:

 - Prepare basic example of script to execute every x sec (example: missions)
    - Update casino rewards every day (with random changes)
    - Find a winner of the weekly tombola (+tell user if they won/lost something + delete all tickets)
 - Mysql trigger to:
    - delete empty user_items (quantity = quantity_in_sell = 0)
    - (script or trigger?) delete channel_messages if too old or more than x elements
    - delete all user_quizz_answers where quizz->in_use = 0
    - store old grades into a backup table
--------------------------------------------------------------------------

