<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

class News
{
  /**
   * Add a news (all languages should be provided)
   *
   * @param array $news Array with each element (id being the language code) containing a 'title' and a 'body'
   *
   * @return bool Success
   *
   * @example News::addNews([
                             "EN" => ["title" => "English title", "body" => "English body"],
                             "FR" => ["title" => "French title", "body" => "French body"]
                            ]);
   */
  public static function addNews($news)
  {
    $languages = Languages::getLanguageCodes();
    if (SPDO::getInstance()->beginTransaction())
    {
      // Create base news
      SPDO::getInstance()->query("INSERT INTO news (id) VALUES (NULL);");

      // Get the created news ID
      $res = SPDO::getInstance()->query("SELECT LAST_INSERT_ID();");
      $news_id = $res->fetch()[0];

      // Add language specific news
      foreach($languages as $language_code)
      {
        if (array_key_exists($language_code, $news))
        {
          $specific_news = $news[$language_code];
          if (array_key_exists("title", $specific_news) and array_key_exists("body", $specific_news))
          {
            $title = $news[$language_code]['title'];
            $body = $news[$language_code]['body'];

            $sql = "INSERT INTO news_translation (news_id, language_code, title, body) VALUES (:news_id, :language_code, :title, :body)";
            $query = SPDO::getInstance()->prepare($sql);

            try
            {
              $query->execute(array(":news_id" => $news_id, ":language_code" => $language_code, ":title" => $title, ":body" => $body));
            }
            catch (PDOException $e)
            {
              error_log("Could not create translation news with elements news_id=".$news_id.", language_code=".$language_code.", title='".$title."', body='".$body."': ".$e->getMessage());
              SPDO::getInstance()->rollBack();
              return false;
            }
          }
          else
          {
            error_log("Title or body not specified in order to create a news.");
            SPDO::getInstance()->rollBack();
            return false;
          }
        }
        else
        {
          error_log("Language ".$language_code." not specified in order to add the news.");
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      if (!SPDO::getInstance()->commit())
      {
        error_log("Could not commit the news.");
        SPDO::getInstance()->rollBack();
        return false;
      }
    }
    else
    {
      error_log("Failed to begin transaction.");
      return false;
    }

    return true;
  }

  /**
   * Get all news (for a specific language)
   *
   * @param char(2) $language_code Language
   * @param int $number_of_news (optional) Max number of news to get (all by default)
   *
   * @return array Array with each element containing the news 'id', the 'date' of the publication, the 'title' and the 'body'
   *
   * @example
       - Get all english news: print_r(News::getNews("EN"));
       - Get the 2 latest french news: print_r(News::getNews("FR", 2));
   */
  public static function getNews($language_code, $number_of_news=-1)
  {
    $sql = "SELECT n.id, n.date, nt.title, nt.body FROM news n INNER JOIN news_translation nt ON n.id = nt.news_id WHERE nt.language_code = :language_code ORDER BY n.date DESC";

    if ($number_of_news > 0)
    {
      $sql = $sql." LIMIT ".$number_of_news;
    }
    $sql = $sql.";";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $values = array(":language_code" => $language_code);
      $query->execute($values);
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get news: ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get all news ID
   *
   * @return array Array with all news ID
   *
   * @example print_r(News::getAllNewsId());
   */
  public static function getAllNewsId()
  {
    $sql = "SELECT id FROM news ORDER BY date DESC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all news id: ".$e->getMessage());
      return array();
    }

    $all_ids = $query->fetchAll(PDO::FETCH_NAMED);

    $returned_ids = [];
    foreach ($all_ids as $id)
    {
      array_push($returned_ids, $id['id']);
    }

    return $returned_ids;
  }

  /**
   * Delete a specific news
   *
   * @param int $id ID of the news to delete
   *
   * @return bool News deleted
   *
   * @example News::deleteNews(5);
   */
  public static function deleteNews($id)
  {
    return SPDO::getInstance()->exec("DELETE FROM news WHERE news.id = ".intval($id, 10).";") > 0;
  }

  /**
   * Update a specific news with a specific language
   *
   * @param int $id ID of the news to update
   * @param char(2) $language_code Language to update
   * @param string $title New title
   * @param string $body New body
   *
   * @return bool News updated
   *
   * @example News::updateNews(5, "EN", "My new title", "My new body");
   */
  public static function updateNews($id, $language_code, $title, $body)
  {
    $sql = "UPDATE news_translation SET title = :title, body = :body WHERE news_id = :id AND language_code = :language_code;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":title" => $title, ":body" => $body, ":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update news: ".$e->getMessage());
      return false;
    }

    return true;
  }
}

?>
