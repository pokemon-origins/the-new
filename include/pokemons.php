<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

class Pokemons
{
  /**
   * Get user pokedex (for a specific language)
   *
   * @param char(2) $language_code Language
   * @param int $user_id User ID
   *
   * @return array Array with each sub array containing the pokemon 'id', 'name', 'is_normal_owned', 'is_shiny_owned'
   *
   * @example print_r(Pokemons::getAllPokemons("EN"));
   */
  public static function getAllPokemons($language_code, $user_id)
  {
    $sql = "
    SELECT
      p.id AS id,
      pt.name AS name,
      (pn.user_id > 0) AS is_normal_owned,
      (ps.user_id > 0) AS is_shiny_owned
    FROM pokemons p
      INNER JOIN pokemons_translation pt ON p.id = pt.pokemon_id
      LEFT OUTER JOIN user_pokedex pn ON (p.id = pn.pokemon_id AND pn.is_shiny = 0 AND pn.user_id = :user_id)
      LEFT OUTER JOIN user_pokedex ps ON (p.id = ps.pokemon_id AND ps.is_shiny = 1 AND ps.user_id = :user_id)
    WHERE
      pt.language_code = :language_code
    ORDER BY
      p.id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":user_id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all pokemons for user ".$user_id.": ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get pokemon information (for a specific language)
   *
   * @param char(2) $language_code Language
   * @param int $id Id of the pokemon
   *
   * @return array Array containing the pokemon 'id' and 'name', 'description', 'type_1_id', 'type_2_id', 'has_gender', 'shape_id', 'shape_name', 'shape_description', 'color_id', 'color_hex', 'color_name', 'habitat_id', 'habitat_name', 'height', 'weight', 'capture_rate', 'base_happiness', 'is_legendary', 'base_hp', 'base_attack', 'base_defense', base_speed', 'base_special_attack', 'base_special_defense'
   *
   * @example print_r(Pokemons::getPokemon("EN", 1));
   */
  public static function getPokemon($language_code, $id)
  {
    $sql = '
    SELECT
      p.id as "id",
      pt.name as "name",
      pt.description as "description",
      p.type_1_id as "type_1_id",
      p.type_2_id as "type_2_id",
      p.has_gender as "has_gender",
      p.shape_id as "shape_id",
      st.name as "shape_name",
      st.description as "shape_description",
      p.color_id as "color_id",
      c.color as "color_hex",
      ct.name as "color_name",
      p.habitat_id as "habitat_id",
      ht.name as "habitat_name",
      p.height as "height",
      p.weight as "weight",
      p.capture_rate as "capture_rate",
      p.base_happiness as "base_happiness",
      p.is_legendary as "is_legendary",
      p.base_hp as "base_hp",
      p.base_attack as "base_attack",
      p.base_defense as "base_defense",
      p.base_speed as "base_speed",
      p.base_special_attack as "base_special_attack",
      p.base_special_defense as "base_special_defense"
    FROM pokemons p
      INNER JOIN pokemons_translation pt ON p.id = pt.pokemon_id
      INNER JOIN pokemon_shapes_translation st ON p.shape_id = st.pokemon_shape_id
      INNER JOIN colors c ON p.color_id = c.id
      INNER JOIN colors_translation ct ON p.color_id = ct.color_id
      LEFT OUTER JOIN pokemon_habitats_translation ht ON p.habitat_id = ht.pokemon_habitat_id
    WHERE
      p.id = :id AND
      pt.language_code = :language_code AND
      st.language_code = :language_code AND
      ct.language_code = :language_code AND
      (p.habitat_id IS NULL OR ht.language_code = :language_code);';

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get pokemon information: ".$e->getMessage());
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  /**
   * Get list of learnable attacks for a pokemon with level (for a specific language)
   *
   * @param char(2) $language_code Language
   * @param int $id Id of the pokemon
   * @param int $maximum_level Maximum level (inclusive) until which the pokemon can learn the attacks
   *
   * @return array Array containing the attack 'id' and 'name', 'description', 'required_pokemon_level'
   *
   * @example print_r(Pokemons::getPokemonLearnableAttacksWithLevel("EN", 1, 100));
   */
  public static function getPokemonLearnableAttacksWithLevel($language_code, $id, $maximum_level)
  {
    $sql = '
      SELECT
        a.attack_id AS id,
        att.name AS name,
        att.description AS description,
        a.required_level AS required_pokemon_level
      FROM pokemon_learn_attacks a
        INNER JOIN attacks_translation att ON a.attack_id = att.attack_id
      WHERE
        a.pokemon_id = :id AND
        att.language_code = :language_code AND
        a.required_level <= :maximum_level AND
        a.pokemon_learn_attack_method_id = 1
      ORDER BY a.required_level ASC;';

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":id" => $id, ":maximum_level" => $maximum_level));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get learnable attacks for pokemon ".$id." with level <= ".$maximum_level.": ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get list of learnable attacks with item for a pokemon (for a specific language)
   *
   * @param char(2) $language_code Language
   * @param int $id Id of the pokemon
   *
   * @return array Array containing the 'attack_id', 'attack_name', 'attack_description', 'item_id', 'item_name', 'item_description'
   *
   * @example print_r(Pokemons::getPokemonLearnableAttacksWithItem("EN", 1));
   */
  public static function getPokemonLearnableAttacksWithItem($language_code, $id)
  {
    $sql = '
      SELECT
        a.attack_id AS attack_id,
        att.name AS attack_name,
        att.description AS attack_description,
        i.id AS item_id,
        it.name AS item_name,
        it.description AS item_description
      FROM pokemon_learn_attacks a
        INNER JOIN attacks_translation att ON a.attack_id = att.attack_id
        INNER JOIN items_attacks_map map ON a.attack_id = map.attack_id
        INNER JOIN items i ON map.item_id = i.id
        INNER JOIN items_translation it ON i.id = it.item_id
      WHERE
        a.pokemon_id = :id AND att.language_code = :language_code AND it.language_code = :language_code
      ORDER BY it.name ASC;';

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get learnable attacks with items for pokemon ".$id.": ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get number of different pokemon captured
   *
   * @param int $user_id User id
   *
   * @return array Array containing elements 'different_normal' and 'different_shiny'
   *
   * @example Pokemons::getNumberOfDifferentCapturedPokemons(1);
   */
  public static function getNumberOfDifferentCapturedPokemons($user_id)
  {
    $sql = "
    SELECT
      (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND is_shiny = 0) AS different_normal,
      (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND is_shiny = 1) AS different_shiny;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the number of different captured pokemons of user ".$user_id.": ".$e->getMessage());
    }

    return ($query->fetch(PDO::FETCH_NAMED));
  }

  /**
   * Get pokemon to which the pokemon can evolve
   *
   * @param char(2) $language_code Language
   * @param int $pokemon_id Pokemon id
   *
   * @return array Array Empty if none, else containing elements 'pokemon_id', 'pokemon_name', 'pokemon_descrition', 'minimum_level', 'minimum_hapiness', 'need_trade', 'need_time_of_day', 'required_item_id', 'required_item_name', 'required_item_description', 'required_held_item_id', 'required_held_item_name', 'required_held_item_description'
   *
   * @example Pokemons::getEvolutions("EN", 1);
   */
  public static function getEvolutions($language_code, $pokemon_id)
  {
    $sql = "
      SELECT
      p.id AS pokemon_id,
      pt.name AS pokemon_name,
      pt.description AS pokemon_descrition,
      p.evolution_minimum_level AS minimum_level,
      p.evolution_minimum_hapiness AS minimum_hapiness,
      p.evolution_during_trade AS need_trade,
      p.evolution_time_of_day AS need_time_of_day,
      todt.name AS time_of_day_name,
      todt.description AS time_of_day_description,
      p.evolution_required_item_id AS required_item_id,
      it.name AS required_item_name,
      it.description AS required_item_description,
      p.evolution_required_held_item_id AS required_held_item_id,
      held_it.name AS required_held_item_name,
      held_it.description AS required_held_item_description
    FROM pokemons p
      INNER JOIN pokemons_translation pt ON p.id =  pt.pokemon_id
      LEFT OUTER JOIN items_translation it ON (p.evolution_required_item_id = it.item_id AND it.language_code = :language_code)
      LEFT OUTER JOIN items_translation held_it ON (p.evolution_required_held_item_id = held_it.item_id AND held_it.language_code = :language_code)
      LEFT OUTER JOIN time_of_day_translation todt ON (p.evolution_time_of_day = todt.time_of_day_id AND todt.language_code = :language_code)
    WHERE
      p.evolve_from_pokemon_id = :pokemon_id AND pt.language_code = :language_code
    ORDER BY p.id ASC;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":pokemon_id" => $pokemon_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the evolutions of pokemon ".$pokemon_id.": ".$e->getMessage());
    }

    return ($query->fetchAll(PDO::FETCH_NAMED));
  }

  /**
   * Get parent of a pokemon
   *
   * @param char(2) $language_code Language
   * @param int $pokemon_id Pokemon id
   *
   * @return array Array Empty if none, else containing elements 'id', 'name', 'description'
   *
   * @example Pokemons::getParentPokemon("EN", 2);
   */
  public static function getParentPokemon($language_code, $pokemon_id)
  {
    $sql = "
      SELECT
      p.evolve_from_pokemon_id AS id,
      pt.name AS name,
      pt.description AS description
    FROM pokemons p
      INNER JOIN pokemons_translation pt ON p.evolve_from_pokemon_id =  pt.pokemon_id
    WHERE
      p.id = :pokemon_id AND pt.language_code = :language_code;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":pokemon_id" => $pokemon_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the parent of pokemon ".$pokemon_id.": ".$e->getMessage());
    }

    $result = ($query->fetch(PDO::FETCH_NAMED));
    if (!$result)
    {
      return [];
    }
    return $result;
  }

  public static function stillHaveSpaceForAnOtherPokemon($user_id)
  {
    $sql = "
      SELECT
        (CASE WHEN u.storage_size > (SELECT COUNT(*) FROM user_pokemons WHERE user_id = :user_id)
          THEN TRUE
          ELSE FALSE
        END) AS still_have_space
      FROM users u
      WHERE u.id = :user_id;
    ";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to check user ".$user_id." storage box size: ".$e->getMessage());
      return false;
    }

    return (($query->fetch()[0]) == "1" ? true : false);
  }

  private static function addPokemonInUserPokedex($user_id,
                                                  $pokedex_id,
                                                  $is_shiny)
  {
    $sql = "
      INSERT INTO user_pokedex (user_id, pokemon_id, is_shiny)
      VALUES (:user_id, :pokedex_id, :is_shiny)
      ON DUPLICATE KEY UPDATE is_shiny = is_shiny;
    ";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id, ":pokedex_id" => $pokedex_id, ":is_shiny" => ($is_shiny ? '1' : '0')));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add pokemon ".$pokedex_id." to user ".$user_id." pokedex: ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function addPokemonToUser($user_id,
                                          $pokedex_id,
                                          $is_shiny = false,
                                          $level = 5,
                                          $normal_action_points = 10,
                                          $daily_remaining_action_points_increase = 5,
                                          $remaining_action_points = 10)
  {
    if ($level <= 0 || $user_id <= 0 || $pokedex_id <= 0 ||
        $normal_action_points <= 0 || $daily_remaining_action_points_increase <= 0 ||
        $remaining_action_points <= 0)
    {
      return false;
    }

    // Be sure the user has enough space to store the pokemon before giving the pokemon
    if (!Pokemons::stillHaveSpaceForAnOtherPokemon($user_id))
    {
      return false;
    }

    // Get base data of the pokemon to add
    $sql = '
    SELECT
      p.has_gender,
      p.base_happiness
    FROM pokemons p
    WHERE
      p.id = :pokedex_id;
    ';
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":pokedex_id" => $pokedex_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get pokemon base information: ".$e->getMessage());
      return false;
    }

    $base_pokemon_info = $query->fetch(PDO::FETCH_NAMED);

    if (!SPDO::getInstance()->beginTransaction())
    {
      error_log("Failed to begin transaction");
      return false;
    }

    // Insert the pokemon into the user pokemons
    $sql = "
      INSERT INTO user_pokemons
        (
          user_id,
          pokemon_id,
          gender_id,
          is_shiny,
          is_in_sell,
          is_in_exchange,
          victories,
          losses,
          happiness,
          level,
          xp,
          boost_hp,
          boost_attack,
          boost_defense,
          boost_speed,
          boost_special_attack,
          boost_special_defense,
          iv_hp,
          iv_attack,
          iv_defense,
          iv_speed,
          iv_special_attack,
          iv_special_defense,
          normal_action_points,
          daily_remaining_action_points_increase,
          remaining_action_points,
          learned_attack_1_id,
          learned_attack_2_id,
          learned_attack_3_id,
          learned_attack_4_id,
          nickname
        )
      VALUES
        (
          :user_id,
          :pokedex_id,
          :gender_id,
          :is_shiny,
          0, 0, 0, 0,
          :happiness,
          :level,
          0,
          0, 0, 0, 0, 0, 0,
          :iv_hp,
          :iv_attack,
          :iv_defense,
          :iv_speed,
          :iv_special_attack,
          :iv_special_defense,
          :normal_action_points,
          :daily_remaining_action_points_increase,
          :remaining_action_points,
          :learned_attack_1_id,
          :learned_attack_2_id,
          :learned_attack_3_id,
          :learned_attack_4_id,
          ''
        );";
    $query = SPDO::getInstance()->prepare($sql);

    // Put base data into the pokemon
    $gender_id = $base_pokemon_info['has_gender'] == "1" ? mt_rand(1, 2) : 3;
    $happiness = $base_pokemon_info['base_happiness'];

    // Generate pseudo random IV for the pokemon
    $iv_hp = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);
    $iv_attack = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);
    $iv_defense = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);
    $iv_speed = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);
    $iv_special_attack = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);
    $iv_special_defense = $is_shiny ? mt_rand(6, 15) : mt_rand(1, 12);

    // TODO: XP calculus using level

    // TODO: Don't use static attacks: Get at least first attack learnable from the pokemon
    // and if more than one attack, learn them (limit 4)
    $learned_attack_1_id = 1;
    $learned_attack_2_id = NULL;
    $learned_attack_3_id = NULL;
    $learned_attack_4_id = NULL;

    try
    {
      $query->execute(array
        (
          ":user_id" => $user_id,
          ":pokedex_id" => $pokedex_id,
          ":gender_id" => $gender_id,
          ":is_shiny" => ($is_shiny ? "1" : "0"),
          ":happiness" => $happiness,
          ":level" => $level,
          ":iv_hp" => $iv_hp,
          ":iv_attack" => $iv_attack,
          ":iv_defense" => $iv_defense,
          ":iv_speed" => $iv_speed,
          ":iv_special_attack" => $iv_special_attack,
          ":iv_special_defense" => $iv_special_defense,
          ":normal_action_points" => $normal_action_points,
          ":daily_remaining_action_points_increase" => $daily_remaining_action_points_increase,
          ":remaining_action_points" => $remaining_action_points,
          ":learned_attack_1_id" => $learned_attack_1_id,
          ":learned_attack_2_id" => $learned_attack_2_id,
          ":learned_attack_3_id" => $learned_attack_3_id,
          ":learned_attack_4_id" => $learned_attack_4_id
        )
      );
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add pokemon ".$pokedex_id." to user ".$user_id.": ".$e->getMessage());
      SPDO::getInstance()->rollBack();
      return false;
    }

    if (!Pokemons::addPokemonInUserPokedex($user_id, $pokedex_id, $is_shiny))
    {
      SPDO::getInstance()->rollBack();
      return false;
    }

    SPDO::getInstance()->commit();
    return true;
  }
}

?>
