<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

class Blacklist
{
  public static function isBlacklistedByAnUser($from_id, $to_id)
  {
    $sql = "
      SELECT
        (CASE
          WHEN
            EXISTS(SELECT 1 FROM blacklist WHERE from_id = :from_id AND to_id = :to_id)
          THEN 1
          ELSE 0
        END)
        AS is_blacklisted;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":from_id" => $from_id, ":to_id" => $to_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get blacklist from "+$from_id+" to "+$to_id+": ".$e->getMessage());
      return false;
    }

    return ($query->fetch(PDO::FETCH_NAMED)['is_blacklisted'] == "1");
  }

  public static function getAllBlacklistedByAnUser($from_id)
  {
    $sql = "
      SELECT
        b.to_id AS id,
        u.login AS login
      FROM blacklist b
        INNER JOIN users u ON b.to_id = u.id
      WHERE
        b.from_id = :from_id;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":from_id" => $from_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all blacklist users from "+$from_id+": ".$e->getMessage());
      return false;
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function removeUserFromBlacklist($from_id, $to_id)
  {
    $sql = "DELETE FROM blacklist WHERE from_id = :from_id AND to_id = :to_id;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":from_id" => $from_id, ":to_id" => $to_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to delete blacklist from "+$from_id+" to "+$to_id+": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function addUserInBlackList($from_id, $to_id)
  {
    $sql = "INSERT INTO blacklist (from_id, to_id) VALUES (:from_id, :to_id);";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":from_id" => $from_id, ":to_id" => $to_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add blacklist from "+$from_id+" to "+$to_id+": ".$e->getMessage());
      return false;
    }

    return true;
  }
}

?>
