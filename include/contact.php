<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

class ContactAndReport
{
  private static function send($title, $message, $sender_id, $reported_id)
  {
    // Send the information
    $sql = "";
    if ($reported_id > 0)
    {
      $sql = "INSERT INTO contact_and_report (message, title, sender_user_id, reported_user_id) VALUES (:message, :title, :sender_id, :reported_id);";
    }
    else
    {
      $sql = "INSERT INTO contact_and_report (message, title, sender_user_id) VALUES (:message, :title, :sender_id);";
    }

    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      if ($reported_id > 0)
      {
        $query->execute(array(":sender_id" => $sender_id, ":reported_id" => $reported_id, ":message" => $message, ":title" => $title));	
      }
      else
      {
        $query->execute(array(":sender_id" => $sender_id, ":message" => $message, ":title" => $title));
      }
    }
    catch (PDOException $e)
    {
      error_log("Impossible to send the report/contact:".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function sendContact($title, $message, $sender_id)
  {
    if ($sender_id <= 0)
    {
      return false;
    }

    return ContactAndReport::send($title, $message, $sender_id, 0);
  }

  public static function sendReport($title, $message, $sender_id, $reported_id)
  {
    if ($reported_id <= 0 || $sender_id <= 0)
    {
      return false;
    }

    return ContactAndReport::send($title, $message, $sender_id, $reported_id);
  }
}

?>
