<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

class HuntingCompetition
{
  /**
   * Get all pokemons giving points in the hunting competition (in a specific language)
   *
   * @param char(2) $language_code Language
   *
   * @return array Array with each sub array containing the pokemon 'id', 'name' and 'points'
   *
   * @example print_r(Pokemons::getPokemonsGivingPoints("EN"));
   */
  public static function getPokemonsGivingPoints($language_code)
  {
    $sql = "
    SELECT
      h.pokemon_id AS id,
      pt.name AS name,
      h.points AS points
    FROM hunting_competition_pokemons h
      INNER JOIN pokemons_translation pt ON pt.pokemon_id = h.pokemon_id
    WHERE pt.language_code = :language_code AND h.points > 0
    ORDER BY h.pokemon_id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all pokemons giving points for the hunting competition: ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }
}

?>
