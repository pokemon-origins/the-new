<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

class Languages
{
  /**
   * Get all available languages
   *
   * @return array Array of all languages (each array element contains 'code' and 'name' keys)
   *
   * @example print_r(json_encode(Languages::getLanguages()));
   */
  public static function getLanguages()
  {
    $response = SPDO::getInstance()->query("SELECT code, name FROM languages;");
    return $response->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get all available language codes
   *
   * @return array Array of all language codes
   *
   * @example print_r(json_encode(Languages::getLanguageCodes()));
   */
  public static function getLanguageCodes()
  {
    $response = SPDO::getInstance()->query("SELECT code FROM languages;");

    $language_codes = [];
    foreach ($response->fetchAll(PDO::FETCH_NAMED) as $code)
    {
      array_push($language_codes, $code['code']);
    }

    return $language_codes;
  }

  /**
   * Check if a language is supported
   *
   * @param char(2) $code Language code
   *
   * @return bool Language supported
   *
   * @example echo "Language EN exists: ".Languages::checkLanguage("EN");
   */
  public static function checkLanguage($code)
  {
    $sql = "SELECT COUNT(code) FROM languages WHERE code = :code;";

    $query = SPDO::getInstance()->prepare($sql);
    $query->execute(array(":code" => $code));

    return ($query->fetch()[0] == 1);
  }
}

?>
