<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("notifications.php")) { exit("Could not open notifications library."); }; // Notifications

if (!include_once("libs/translation.php")) { exit("Could not open translation library."); }; // Translation

if (!include_once("user.php")) { exit("Could not open user library."); }; // User

if (!include_once("items.php")) { exit("Could not open items library."); }; // Items

if (!include_once("pokemons.php")) { exit("Could not open pokemons library."); }; // Pokemons

use Translation\Translation;

class Rewards
{
  private static function getReward($reward_id, $user_id)
  {
    $sql = "
      SELECT
        r.item_id AS item_id,
        it.name AS item_name,
        r.pokemon_id AS pokemon_id,
        pt.name AS pokemon_name,
        r.is_shiny AS pokemon_is_shiny,
        r.dollar AS dollar,
        r.gold AS gold,
        r.points AS points
      FROM rewards r
        INNER JOIN users u ON u.id = :user_id
        LEFT OUTER JOIN pokemons_translation pt ON (pt.pokemon_id = r.pokemon_id AND pt.language_code = u.language_code)
        LEFT OUTER JOIN items_translation it ON (it.item_id = r.item_id AND it.language_code = u.language_code)
      WHERE
        r.id = :reward_id;
    ";

     $query = SPDO::getInstance()->prepare($sql);

     try
     {
       $query->execute(array(":reward_id" => $reward_id, ":user_id" => $user_id));
     }
     catch (PDOException $e)
     {
       error_log("Impossible to get info on reward ".$reward_id.": ".$e->getMessage());
       return false;
     }

     return $query->fetch(PDO::FETCH_NAMED);
  }

  public static function giveRewardToUser($reward_id, $user_id)
  {
    // Get rewards content
    $raw_data = Rewards::getReward($reward_id, $user_id);

    if ($raw_data == false)
    {
      return false;
    }

    // Give the rewards to the user
    if (SPDO::getInstance()->beginTransaction())
    {
      if ($raw_data['item_id'] != NULL)
      {
        if(!Items::addItemToUser($user_id, $raw_data['item_id'], 1))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }
      if ($raw_data['pokemon_id'] != NULL)
      {
        if (!Pokemons::addPokemonToUser($user_id,
                                        $raw_data['pokemon_id'],
                                        ($raw_data['pokemon_is_shiny'] == "1")))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }
      if ($raw_data['dollar'] > 0)
      {
        if (!User::addPokedollar($user_id, $raw_data['dollar']))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }
      if ($raw_data['points'] > 0)
      {
        if (!User::addPoints($user_id, $raw_data['points']))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }
      if ($raw_data['gold'] > 0)
      {
        if (!User::addGold($user_id, $raw_data['gold']))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      SPDO::getInstance()->commit();
      return true;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }

  public static function rewardIdToRewardName($reward_id, $user_id)
  {
    // Get rewards content
    $raw_data = Rewards::getReward($reward_id, $user_id);

    if ($raw_data == false)
    {
      return "";
    }

    // Translate content into the right language
    Translation::forceLanguage(User::getLanguage($user_id));
    $result = "";
    if ($raw_data['item_id'] != NULL)
    {
      if ($result != "") { $result .= ", "; }
      $result .= Translation::of("ITEM", ["item" => $raw_data['item_name']]);
    }
    if ($raw_data['pokemon_id'] != NULL)
    {
      if ($result != "") { $result .= ", "; }
      if ($raw_data['pokemon_is_shiny'] == "1")
      {
        $result .= Translation::of("SHINY_POKEMON", ["pokemon" => $raw_data['pokemon_name']]);
      }
      else
      {
        $result .= Translation::of("POKEMON", ["pokemon" => $raw_data['pokemon_name']]);
      }
    }
    if ($raw_data['dollar'] > 0)
    {
      if ($result != "") { $result .= ", "; }
      $result .= Translation::of("POKEDOLLAR", ["pokedollar" => $raw_data['dollar']]);
    }
    if ($raw_data['gold'] > 0)
    {
      if ($result != "") { $result .= ", "; }
      $result .= Translation::of("GOLD", ["gold" => $raw_data['gold']]);
    }
    if ($raw_data['points'] > 0)
    {
      if ($result != "") { $result .= ", "; }
      $result .= Translation::of("POINTS", ["points" => $raw_data['points']]);
    }

    return $result;
  }
}

?>
