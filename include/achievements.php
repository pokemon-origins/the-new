<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("rewards.php")) { exit("Could not open rewards library."); }; // Rewards

class Achievements
{
  //achievement_id <= 0 if all requested
  private static function getAchievements($user_id, $achievement_id)
  {
    $sql = "
      SELECT
        a.id AS id,
        atr.title AS title,
        atr.goal AS goal,
        CASE
          WHEN (ua.grade IS NOT NULL) THEN ua.grade
          ELSE 0
        END AS current_grade,
        CASE
          WHEN (ua.grade IS NULL) THEN a.condition_1
          WHEN (ua.grade = 1) THEN a.condition_2
          WHEN (ua.grade = 2) THEN a.condition_3
          WHEN (ua.grade = 3) THEN a.condition_4
          ELSE NULL
        END AS next_condition,
        CASE
          WHEN (ua.grade IS NULL) THEN a.reward_1_id
          WHEN (ua.grade = 1) THEN a.reward_2_id
          WHEN (ua.grade = 2) THEN a.reward_3_id
          WHEN (ua.grade = 3) THEN a.reward_4_id
          ELSE NULL
        END AS next_reward_id,
        CASE
          WHEN (a.id = 1) THEN u.score
          WHEN (a.id = 2) THEN u.pvp_score
          WHEN (a.id = 3) THEN (SELECT COUNT(*) FROM grades WHERE user_id = :user_id)
          WHEN (a.id = 4) THEN (SELECT SUM(victories) FROM user_pokemons WHERE user_id = :user_id)
          WHEN (a.id = 5) THEN u.successful_missions
          WHEN (a.id = 6) THEN u.successful_side_quests
          WHEN (a.id = 7) THEN (u.A_class_victories + u.B_class_victories + u.C_class_victories + u.D_class_victories)
          WHEN (a.id = 8) THEN u.hunting_competition_score
          WHEN (a.id = 9) THEN (SELECT COUNT(*) FROM user_pokemons WHERE user_id = :user_id)
          WHEN (a.id = 10) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 1 AND pokemon_id <= 151 AND is_shiny = 0)
          WHEN (a.id = 11) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 1 AND pokemon_id <= 151 AND is_shiny = 1)
          WHEN (a.id = 12) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 152 AND pokemon_id <= 251 AND is_shiny = 0)
          WHEN (a.id = 13) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 152 AND pokemon_id <= 251 AND is_shiny = 1)
          WHEN (a.id = 14) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 252 AND pokemon_id <= 386 AND is_shiny = 0)
          WHEN (a.id = 15) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 252 AND pokemon_id <= 386 AND is_shiny = 1)
          WHEN (a.id = 16) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 387 AND pokemon_id <= 493 AND is_shiny = 0)
          WHEN (a.id = 17) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 387 AND pokemon_id <= 493 AND is_shiny = 1)
          WHEN (a.id = 18) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 494 AND pokemon_id <= 649 AND is_shiny = 0)
          WHEN (a.id = 19) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 494 AND pokemon_id <= 649 AND is_shiny = 1)
          WHEN (a.id = 20) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 650 AND pokemon_id <= 721 AND is_shiny = 0)
          WHEN (a.id = 21) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 650 AND pokemon_id <= 721 AND is_shiny = 1)
          WHEN (a.id = 22) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 722 AND pokemon_id <= 807 AND is_shiny = 0)
          WHEN (a.id = 23) THEN (SELECT COUNT(*) FROM user_pokedex WHERE user_id = :user_id AND pokemon_id >= 722 AND pokemon_id <= 807 AND is_shiny = 1)
          WHEN (a.id = 24) THEN u.spent_pokedollar
          WHEN (a.id = 25) THEN u.spent_gold
          WHEN (a.id = 26) THEN u.spent_real_euros
          WHEN (a.id = 27) THEN u.number_days_connected_in_a_row
          WHEN (a.id = 28) THEN u.number_of_births
          WHEN (a.id = 29) THEN u.lottery_tickets
          WHEN (a.id = 30) THEN CHAR_LENGTH(u.description)
          WHEN (a.id = 31) THEN CASE WHEN (u.last_seen_news_id IS NULL) THEN 0 ELSE u.last_seen_news_id END
          WHEN (a.id = 32) THEN CASE WHEN (u.last_badge_id IS NULL) THEN 0 ELSE u.last_badge_id END
          ELSE NULL
        END AS current_value
      FROM achievements a
        INNER JOIN achievements_translation atr ON atr.achievement_id = a.id
        LEFT OUTER JOIN user_achievements ua ON (ua.achievement_id = a.id AND ua.user_id = :user_id)
        INNER JOIN users u ON u.id = :user_id
      WHERE
        atr.language_code = u.language_code
    ";

    if ($achievement_id > 0)
    {
      $sql .= " AND a.id = :achievement_id  ";
    }

    $sql .= " ORDER BY a.id ASC;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      if ($achievement_id > 0)
      {
        $query->execute(array(":achievement_id" => $achievement_id, ":user_id" => $user_id));
      }
      else
      {
        $query->execute(array(":user_id" => $user_id));
      }
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the achievements of user ".$user_id.": ".$e->getMessage());
      return array();
    }

    if ($achievement_id > 0)
    {
      return $query->fetch(PDO::FETCH_NAMED);
    }
    else
    {
      return $query->fetchAll(PDO::FETCH_NAMED);
    }
  }

  public static function getAllAchievements($user_id)
  {
    // Get all achievements
    $achievements = Achievements::getAchievements($user_id, 0);

    // Add a string explaining the potential reward
    foreach ($achievements as &$achievement)
    {
      $achievement['next_reward_description'] = Rewards::rewardIdToRewardName($achievement['next_reward_id'], $user_id);
    }

    return $achievements;
  }

  public static function upgradeAchievement($user_id, $achievement_id)
  {
    // Check that the achievement can be upgraded
    $achievement = Achievements::getAchievements($user_id, $achievement_id);

    if ($achievement['current_grade'] < 4 &&
        $achievement['current_value'] >= $achievement['next_condition'])
    {
      if (SPDO::getInstance()->beginTransaction())
      {
        // Upgrade the achievement
        $sql = "
          INSERT INTO user_achievements (user_id, achievement_id, grade)
          VALUES (:user_id, :achievement_id, :grade)
          ON DUPLICATE KEY UPDATE grade = :grade;
        ";

        $query = SPDO::getInstance()->prepare($sql);
        try
        {
          $query->execute(array(":achievement_id" => $achievement_id, ":user_id" => $user_id, ":grade" => ($achievement['current_grade'] + 1)));
        }
        catch (PDOException $e)
        {
          error_log("Impossible to get the upgrade achievement ".$achievement_id." of user ".$user_id.": ".$e->getMessage());
          SPDO::getInstance()->rollBack();
          return false;
        }

        // Give reward to the user
        if (!Rewards::giveRewardToUser($achievement['next_reward_id'], $user_id))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }

        SPDO::getInstance()->commit();
        return true;
      }
      else
      {
        error_log("Failed to begin transaction");
        return false;
      }
    }

    return false;
  }
}

?>
