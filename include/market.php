<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

if (!include_once("notifications.php")) { exit("Could not open notifications library."); }; // Notifications

if (!include_once("user.php")) { exit("Could not open user library."); }; // User

class Market
{
  private static function giveGold($from_id, $to_id, $quantity, $is_gift)
  {
    if ($quantity < 0)
    {
      return false;
    }

    if (SPDO::getInstance()->beginTransaction())
    {
      // Take gold from $from_id
      if (!User::payGold($from_id, $quantity, $is_gift))
      {
        SPDO::getInstance()->rollBack();
        return false;
      }

      // Give it to to $to_id
      if (!User::addGold($to_id, $quantity))
      {
        SPDO::getInstance()->rollBack();
        return false;
      }

      SPDO::getInstance()->commit();
      return true;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }

  private static function givePokedollar($from_id, $to_id, $quantity, $is_gift)
  {
    if ($quantity < 0)
    {
      return false;
    }

    if (SPDO::getInstance()->beginTransaction())
    {
      // Take pokedollar from $from_id
      if (!User::payPokedollar($from_id, $quantity, $is_gift))
      {
        SPDO::getInstance()->rollBack();
        return false;
      }

      // Give it to to $to_id
      if (!User::addPokedollar($to_id, $quantity))
      {
        SPDO::getInstance()->rollBack();
        return false;
      }

      SPDO::getInstance()->commit();
      return true;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }

  /**
   * Get gold in sell and price of a specific user
   *
   * @param int $id User ID
   *
   * @return array Array with "quantity" and "price"
   *
   * @example print_r(Market::getUserGoldInSell(1));
   */
  public static function getUserGoldInSell($id)
  {
    $sql = "
      SELECT
        gold_in_sell AS quantity,
        gold_sell_price AS price
      FROM users
      WHERE id = :id;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the gold in sell of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  /**
   * Get all gold in sell and price of all users except from one user
   *
   * @param int $id User ID to not get data
   *
   * @return array Array with "quantity" and "price", "user_id" and "user_name"
   *
   * @example print_r(Market::getAllGoldInSell(1));
   */
  public static function getAllGoldInSell($id)
  {
    $sql = "
      SELECT
        gold_in_sell AS quantity,
        gold_sell_price AS price,
        id AS user_id,
        login AS user_name
      FROM users
      WHERE
        gold_in_sell > 0 AND
        gold_sell_price > 0 AND
        id != :id
      ORDER BY
        price ASC,
        quantity DESC;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all gold in sell except for user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function updateGoldInSell($id, $gold_to_add_or_remove)
  {
    $sql = "
      UPDATE users
      SET
        gold = gold - :gold_to_add_or_remove,
        gold_in_sell = gold_in_sell + :gold_to_add_or_remove
      WHERE
        id = :id";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":gold_to_add_or_remove" => $gold_to_add_or_remove));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." gold/gold to sell of ".$gold_to_add_or_remove.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateUserGoldPrice($id, $price)
  {
    $sql = "UPDATE users SET gold_sell_price = :price WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":price" => $price));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." gold price to ".$price.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function buyGold($seller_id, $buyer_id, $quantity, $requested_gold_price)
  {
    if ($quantity <= 0 || $requested_gold_price <= 0)
    {
      error_log("Buy gold impossible beause of wrong parameters (".$buyer_id."->".$seller_id.", quantity: ".$quantity.", requested price: ".$requested_gold_price."");
      return false;
    }

    // Check that the requested gold price is the same as the one from the seller
    $sql = "
      SELECT
        su.gold_in_sell AS seller_gold,
        su.gold_sell_price AS seller_gold_price,
        bu.pokedollar AS buyer_pokedollar
      FROM users su
        INNER JOIN users bu
      WHERE
        su.id = :seller_id AND bu.id = :buyer_id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":seller_id" => $seller_id, ":buyer_id" => $buyer_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to check buy gold request consistency': ".$e->getMessage());
      return false;
    }

    $result = $query->fetch(PDO::FETCH_NAMED);
    if (intval($result["seller_gold"]) < $quantity ||
        intval($result["seller_gold_price"]) != $requested_gold_price ||
        intval($result["buyer_pokedollar"]) < $requested_gold_price * $quantity)
    {
      error_log("User ".$buyer_id." tried to buy ".$quantity." gold at ".$requested_gold_price." from ".$seller_id.".");
      return false;
    }

    // Do the transaction
    if (SPDO::getInstance()->beginTransaction())
    {
      if (Market::updateGoldInSell($seller_id, -intval($quantity)))
      {
        if (Market::givePokedollar($buyer_id, $seller_id, $requested_gold_price * $quantity, false))
        {
          if (Market::giveGold($seller_id, $buyer_id, $quantity, true))
          {
            SPDO::getInstance()->commit();

            // Inform the seller about the transaction
            Notifications::add($seller_id, "TITLE_GOLD_SOLD", "MSG_GOLD_SOLD",
                               ['quantity' => $quantity, 'price' => $requested_gold_price, 'buyer_login' => User::getLogin($seller_id)]);

            return true;
          }
        }
      }

      SPDO::getInstance()->rollback();
      return false;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }
}

?>
