<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

class Items
{
  public static function getUserItems($language_code, $user_id)
  {
    $sql = "
    SELECT
      ui.item_id AS item_id,
      ui.quantity AS quantity,
      it.name AS item_name,
      it.description AS item_description,
      i.category_id AS category_id,
      ict.name AS category_name,
      ic.pocket_id AS pocket_id,
      ipt.name AS pocket_name
    FROM user_items ui
      INNER JOIN items i ON i.id = ui.item_id
      INNER JOIN items_translation it ON it.item_id = ui.item_id
      INNER JOIN item_categories ic ON ic.id = i.category_id
      INNER JOIN item_categories_translation ict ON ict.item_category_id = i.category_id
      INNER JOIN item_pockets_translation ipt ON ipt.item_pocket_id = ic.pocket_id
    WHERE
      ui.user_id = :user_id AND
      it.language_code = :language_code AND
      ict.language_code = :language_code AND
      ipt.language_code = :language_code
    ORDER BY
      ic.pocket_id ASC,
      i.category_id ASC,
      ui.item_id ASC;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the list of all items of user id '".$user_id."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getPockets($language_code)
  {
    $sql = "
    SELECT
      item_pocket_id AS id,
      name AS name
    FROM item_pockets_translation
    WHERE language_code = :language_code
    ORDER BY item_pocket_id ASC;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the list of all item pockets: ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getItem($language_code, $item_id)
  {
    $sql = "
      SELECT
        i.id AS id,
        it.name AS name,
        it.description AS description
      FROM items i
        INNER JOIN items_translation it ON it.item_id = i.id
      WHERE i.id = :item_id AND it.language_code = :language_code;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":item_id" => $item_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the item ".$item_id." information: ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  public static function getItemFlags($language_code, $item_id)
  {
    $sql = "
      SELECT
        ifm.item_flag_id AS id,
        ift.name AS name,
        ift.description AS description
      FROM items_flags_map ifm
        INNER JOIN item_flags_translation ift ON ift.item_flag_id = ifm.item_flag_id
      WHERE ifm.item_id = :item_id AND ift.language_code = :language_code;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":item_id" => $item_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the flags of item ".$item_id.": ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function addItemToUser($user_id, $item_id, $quantity)
  {
    $sql = "
      INSERT INTO user_items (user_id, item_id, quantity)
      VALUES (:user_id, :item_id, :quantity)
      ON DUPLICATE KEY UPDATE quantity = :quantity + 1;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id, ":item_id" => $item_id, ":quantity" => $quantity));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add ".$quantity." item ".$item_id." to user ".$user_id.": ".$e->getMessage());
      return false;
    }

    return true;
  }
}

?>
