<?php
return [
    'TITLE_GOLD_SOLD' => 'Gold sold',
    'MSG_GOLD_SOLD' => 'You sold {quantity} gold at {price} pokedollar to {buyer_login}.',
    'POKEMON' => 'pokemon {pokemon}',
    'SHINY_POKEMON' => 'shiny pokemon {pokemon}',
    'GOLD' => '{gold} gold',
    'ITEM' => '{item} item',
    'POKEDOLLAR' => '{pokedollar} pokedollar',
    'POINTS' => '{points} points',
];
