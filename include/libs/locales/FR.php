<?php
return [
    'TITLE_GOLD_SOLD' => 'Or vendu',
    'MSG_GOLD_SOLD' => 'Vous avez vendu {quantity} or(s) au prix de {price} pokedollars à {buyer_login}.',
    'POKEMON' => '{pokemon} (pokemon)',
    'SHINY_POKEMON' => '{pokemon} shiney (pokemon)',
    'GOLD' => '{gold} or',
    'ITEM' => '{item} (objet)',
    'POKEDOLLAR' => '{pokedollar} pokédollars',
    'POINTS' => '{points} points',
];
