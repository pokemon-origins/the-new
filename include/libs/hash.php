<?php

class Hash
{
  /**
   * Get a salted hash associated with the $value
   *
   * @param $value Value to hash
   *
   * @return str Salted hash
   *
   * @example print_r(Hash::generateHash("BestPassEver"));
   */
  public static function generateHash($value)
  {
    // Unique salt for the website (this is not optimized but better than none)
    return crypt($value, "PokemonOriginsSalt");
  }

  /**
   * Get a salted hash associated with the $value
   *
   * @param str $value Value to hash
   * @param str $hashed_value Hashed value
   *
   * @return bool Is the value associated with the hash?
   *
   * @example print_(Hash::compareValueWithHash("BestPassEver", $hashed_value));
   */
  public static function compareValueWithHash($value, $hashed_value)
  {
    return hash_equals($hashed_value, crypt($value, $hashed_value));
  }
}

?>
