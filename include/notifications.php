<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

if (!include_once("user.php")) { exit("Could not open user library."); }; // User

if (!include_once("libs/translation.php")) { exit("Could not open translation library."); }; // Translation

use Translation\Translation;

class Notifications
{
  public static function add($user_id, $title, $notification, $notification_parameters)
  {
    // Translate to the correct language
    Translation::forceLanguage(User::getLanguage($user_id));
    $show_title = Translation::of($title);
    $show_notification = Translation::of($notification, $notification_parameters);

    // Store the notification for the user
    $sql = "INSERT INTO user_notifications (user_id, notification, title) VALUES (:user_id, :notification, :title);";
    $query = SPDO::getInstance()->prepare($sql);
    try
    {
      $query->execute(array(":user_id" => $user_id, ":notification" => $show_notification, ":title" => $show_title));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to add notification '".$title."'->'".$notification."' for user ".$user_id.": ".$e->getMessage());
      return false;
    }

    return true;
  }

  public static function getCount($user_id)
  {
    $sql = "SELECT COUNT(*) FROM user_notifications WHERE user_id = :user_id;";

    $query = SPDO::getInstance()->prepare($sql);
    $query->execute(array(":user_id" => $user_id));

    return $query->fetch()[0];
  }

  public static function getAndDelete($user_id)
  {
    // Get all notifications
    $sql = "SELECT user_id, notification, title, date FROM user_notifications WHERE user_id = :user_id ORDER BY date DESC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get user id '".$id."' notifications: ".$e->getMessage());
      return array();
    }

    $return_value = $query->fetchAll(PDO::FETCH_NAMED);

    // Delete notifications
    $sql = "DELETE FROM user_notifications WHERE user_id = :user_id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":user_id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to delete user id '".$id."' notifications: ".$e->getMessage());
      return array();
    }

    return $return_value;
  }
}

?>
