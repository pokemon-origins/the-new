<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("rewards.php")) { exit("Could not open rewards library."); }; // Rewards

class Casino
{
  private static function getRewards()
  {
    $sql = "
      SELECT
          gold_casino_reward_1_id,
          gold_casino_reward_2_id,
          gold_casino_reward_3_id,
          gold_casino_reward_4_id,
          gold_casino_reward_5_id,
          gold_casino_reward_6_id,
          dollar_casino_reward_1_id,
          dollar_casino_reward_2_id,
          dollar_casino_reward_3_id,
          dollar_casino_reward_4_id,
          dollar_casino_reward_5_id,
          dollar_casino_reward_6_id
      FROM casino
      WHERE 1
      LIMIT 1;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the casino rewards: ".$e->getMessage());
      return array();
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }

  public static function getAllRewards($user_id)
  {
    // Get all rewards
    $rewards = Casino::getRewards();

    // Add a string explaining the rewards
    $rewards['gold_casino_reward_1_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_1_id'], $user_id);
    $rewards['gold_casino_reward_2_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_2_id'], $user_id);
    $rewards['gold_casino_reward_3_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_3_id'], $user_id);
    $rewards['gold_casino_reward_4_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_4_id'], $user_id);
    $rewards['gold_casino_reward_5_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_5_id'], $user_id);
    $rewards['gold_casino_reward_6_description'] = Rewards::rewardIdToRewardName($rewards['gold_casino_reward_6_id'], $user_id);

    $rewards['dollar_casino_reward_1_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_1_id'], $user_id);
    $rewards['dollar_casino_reward_2_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_2_id'], $user_id);
    $rewards['dollar_casino_reward_3_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_3_id'], $user_id);
    $rewards['dollar_casino_reward_4_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_4_id'], $user_id);
    $rewards['dollar_casino_reward_5_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_5_id'], $user_id);
    $rewards['dollar_casino_reward_6_description'] = Rewards::rewardIdToRewardName($rewards['dollar_casino_reward_6_id'], $user_id);

    return $rewards;
  }

  public static function play($user_id, $is_gold_casino)
  {
    if (SPDO::getInstance()->beginTransaction())
    {
      // Take pokedollar or gold from user
      if ($is_gold_casino)
      {
        if (!User::payGold($user_id, 1))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }
      else
      {
        if (!User::payPokedollar($user_id, 50))
        {
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      // Get the result
      $result = array();
      $result['won'] = false;
      $rand_casino = mt_rand(1, 2000);
      $rand_reward = -1;
      if ($rand_casino == 1) { $rand_reward = 6; }
      elseif ($rand_casino <= 3) { $rand_reward = 5; }
      elseif ($rand_casino <= 10) { $rand_reward = 4; }
      elseif ($rand_casino <= 25) { $rand_reward = 3; }
      elseif ($rand_casino <= 55) { $rand_reward = 2; }
      elseif ($rand_casino <= 110) { $rand_reward = 1; }

      if ($rand_reward < 0)
      {
        SPDO::getInstance()->commit();
        return $result;
      }

      // Give the gift to the user
      $rewards = Casino::getRewards();

      $result['reward_id'] = -1;
      switch ($rand_reward) {
        case 1:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_1_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_1_id'];
          }
          break;
        case 2:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_2_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_2_id'];
          }
          break;
        case 3:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_3_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_3_id'];
          }
          break;
        case 4:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_4_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_4_id'];
          }
          break;
        case 5:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_5_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_5_id'];
          }
          break;
        case 6:
          if ($is_gold_casino)
          {
            $result['reward_id'] = $rewards['gold_casino_reward_6_id'];
          }
          else
          {
            $result['reward_id'] = $rewards['dollar_casino_reward_6_id'];
          }
          break;
      }

      // Give reward to the user
      if (!Rewards::giveRewardToUser($result['reward_id'], $user_id))
      {
        SPDO::getInstance()->rollBack();
        return false;
      }

      $result['won'] = true;
      $result['slot'] = $rand_reward;
      $result['reward_description'] = Rewards::rewardIdToRewardName($result['reward_id'], $user_id);;

      SPDO::getInstance()->commit();
      return $result;
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }
  }
}

?>
