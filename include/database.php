<?php

/**
 * PDO singleton
 *
 * Use example:
 * @code
   foreach (SPDO::getInstance()->query('SELECT code, name FROM language;') as $languages)
   {
     echo '<pre>', print_r($languages) ,'</pre>';
   }
 * @end_code
 *
 */
class SPDO
{
  /**
   * PDO class instance
   *
   * @var PDO
   * @access private
   */
  private $PDOInstance = null;

   /**
   * SPDO class instance
   *
   * @var SPDO
   * @access private
   * @static
   */
  private static $instance = null;

  /**
   * Const: SQL username
   *
   * @var string
   */
  const SQL_USER = 'pokemon_origins';

  /**
   * Const: SQL Host
   *
   * @var string
   */
  const SQL_HOST = 'localhost';

  /**
   * Const: SQL password
   *
   * @var string
   */
  const SQL_PASS = 'sdopiOPIF65D2sg';

  /**
   * Const: SQL database
   *
   * @var string
   */
  const DEFAULT_SQL_DTB = 'pokemon_origins';

  /**
   * Constructor
   *
   * @param void
   * @return void
   * @see PDO::__construct()
   * @access private
   */
  private function __construct()
  {
    $this->PDOInstance = new PDO('mysql:dbname='.self::DEFAULT_SQL_DTB.';host='.self::SQL_HOST,self::SQL_USER ,self::SQL_PASS);

    // Show errors (should not be used in production)
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    ini_set('error_reporting', E_ALL);
    $this->PDOInstance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

   /**
    * Create and get SPDO object
    *
    * @access public
    * @static
    * @param void
    * @return SPDO $instance
    */
  public static function getInstance()
  {
    if(is_null(self::$instance))
    {
      self::$instance = new SPDO();
    }
    return self::$instance;
  }

  /**
   * Execute SQL request with PDO
   *
   * @param string $query SQL request
   * @return PDOStatement PDOStatement statement
   */
  public function query($query)
  {
    return $this->PDOInstance->query($query);
  }

  /**
   * Prepare SQL request with PDO
   *
   * @param string $statement SQL request
   * @param array $driver_options (optional) PDO options
   * @return PDOStatement PDOStatement statement
   */
  public function prepare($statement, $driver_options = [])
  {
    return $this->PDOInstance->prepare($statement, $driver_options);
  }

  // Internal value of transaction to allow nested transactions
  protected $_transactionDepth = 0;

  /**
   * Begin a transaction
   *
   * @return bool Success
   */
  public function beginTransaction()
  {
    $result = false;
    if ($this->_transactionDepth == 0)
    {
      $result = $this->PDOInstance->beginTransaction();
    }
    else
    {
      $this->PDOInstance->exec("SAVEPOINT LEVEL{$this->_transactionDepth}");
      $result = true;
    }

    if ($result)
    {
      $this->_transactionDepth++;
    }

    return $result;
  }

  /**
   * Validate a transaction
   *
   * @return bool Success
   */
  public function commit()
  {
    $this->_transactionDepth--;

    if ($this->_transactionDepth == 0)
    {
      return $this->PDOInstance->commit();
    }
    else
    {
      $this->PDOInstance->exec("RELEASE SAVEPOINT LEVEL{$this->_transactionDepth}");
      return true;
    }
  }

  /**
   * Cancel a transaction
   *
   * @return bool Success
   */
  public function rollBack()
  {
    if ($this->_transactionDepth == 0)
    {
      throw new PDOException('Rollback error : There is no transaction started');
    }

    $this->_transactionDepth--;

    if($this->_transactionDepth == 0)
    {
      return $this->PDOInstance->rollBack();
    }
    else
    {
      $this->PDOInstance->exec("ROLLBACK TO SAVEPOINT LEVEL{$this->_transactionDepth}");
      return true;
    }
  }

  /**
   * Execute request and get number of affected rows
   *
   * @param string $statement SQL request
   *
   * @return int Number of affected rows
   */
  public function exec($statement)
  {
    return $this->PDOInstance->exec($statement);
  }
}

?>
