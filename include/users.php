<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

class Users
{
  /**
   * Get number of registered users
   *
   * @return int Number of connected users (-1 if an error occurs)
   *
   * @example print_r(Users::getNumberOfRegisteredUsers());
   */
  public static function getNumberOfRegisteredUsers()
  {
    $sql = "SELECT COUNT(*) FROM `users` ";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the number of registered users: ".$e->getMessage());
      return -1;
    }

    return $query->fetch()[0];
  }

  /**
   * Get number of connected users
   *
   * @return int Number of connected users (-1 if an error occurs)
   *
   * @example print_r(Users::getNumberOfConnectedUsers());
   */
  public static function getNumberOfConnectedUsers()
  {
    $sql = "SELECT COUNT(*) FROM `users` WHERE `last_time_connected` >= DATE_SUB(NOW(),INTERVAL 15 MINUTE);";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the number of connected users: ".$e->getMessage());
      return -1;
    }

    return $query->fetch()[0];
  }

  /**
   * Get top 50 users
   *
   * @param Type ("score", or "pvp_score" or "hunting_competition_score" or "achievements_score"
   *              or "quizz_score" or "grades_score")
   *
   * @return List of top 50 users in the game (each user having an "id", a "login" and "score")
   *
   * @example print_r(Users::getTop50Users('score'));
   */
  public static function getTop50Users($type)
  {
    if ($type != "score" and $type != "pvp_score" and $type != "hunting_competition_score"
        and $type != "achievements_score" and $type != "quizz_score" and $type != "grades_score")
    {
      error_log("Wrong top 50 users type parameter ".$type);
      return array();
    }

    $sql = "";
    if ($type == "score" or $type == "pvp_score" or $type == "hunting_competition_score")
    {
      $sql = "SELECT id, login, ".$type." AS score FROM users ORDER BY score DESC LIMIT 50;";
    }
    else if ($type == "achievements_score")
    {
      $sql = "
        SELECT
          ua.user_id AS id,
          u.login AS login,
          SUM(ua.grade) AS score
        FROM user_achievements ua
          INNER JOIN users u ON u.id = ua.user_id
        GROUP BY ua.user_id
        ORDER BY score DESC LIMIT 50;
      ";
    }
    else if ($type == "quizz_score")
    {
      $sql = "
        SELECT
          uqa.user_id AS id,
          u.login AS login,
          SUM(q.points) AS score
        FROM user_quizz_answers uqa
          INNER JOIN users u ON u.id = uqa.user_id
          INNER JOIN quizz q ON q.id = uqa.quizz_id
        WHERE
          q.in_use = 1 AND
          uqa.valid_answer = 1
        GROUP BY uqa.user_id
        ORDER BY score DESC LIMIT 50;
      ";
    }
    else if ($type == "grades_score")
    {
      $sql = "
        SELECT
          g.user_id AS id,
          u.login AS login,
          COUNT(*) AS score
        FROM grades g
          INNER JOIN users u ON u.id = g.user_id
        GROUP BY g.user_id
        ORDER BY score DESC LIMIT 50;
      ";
    }

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the top 50 users: ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get 10 users better than specific user
   *
   * @param user_id User id
   * @param Type ("score", or "pvp_score" or "hunting_competition_score")
   *
   * @return List of 10 users better than user_id (each user having an "id", a "login" and "score")
   *
   * @example print_r(Users::get10UsersBetterThan('score', 1));
   */
  public static function get10UsersBetterThan($type, $user_id)
  {
    if ($type != "score" and $type != "pvp_score" and $type != "hunting_competition_score"
        and $type != "achievements_score" and $type != "quizz_score" and $type != "grades_score")
    {
      error_log("Wrong 10 users better than ".$user_id." type parameter ".$type);
      return array();
    }

    $sql = "";
    if ($type == "score" or $type == "pvp_score" or $type == "hunting_competition_score")
    {
      $sql = "SELECT ou.id, ou.login, ou.".$type." AS score
              FROM users u
                INNER JOIN users ou ON ou.".$type." >= u.".$type."
              WHERE u.id = :id AND u.id != ou.id
              ORDER BY score ASC
              LIMIT 10;";
    }
    else if ($type == "achievements_score")
    {
      $sql = "
        SELECT
          oua.user_id AS id,
          ou.login AS login,
          SUM(oua.grade) AS score
        FROM users ou
          INNER JOIN user_achievements oua ON (oua.user_id = ou.id)
        WHERE oua.user_id != :id AND
              (
                SELECT SUM(grade)
                FROM user_achievements
                WHERE user_id = ou.id
              )
                >=
              (
                SELECT SUM(grade)
                FROM user_achievements
                WHERE user_id = :id
              )
        GROUP BY oua.user_id
        ORDER BY score ASC
        LIMIT 10;
      ";
    }
    else if ($type == "quizz_score")
    {
      $sql = "
        SELECT
          ou.id AS id,
          ou.login AS login,
          SUM(q.points) AS score
        FROM users ou
          INNER JOIN user_quizz_answers uqa ON (uqa.user_id = ou.id AND uqa.valid_answer = 1)
          INNER JOIN quizz q ON (q.id = uqa.quizz_id AND q.in_use = 1)
        WHERE uqa.user_id != :id AND
              (
                SELECT SUM(q.points)
                FROM user_quizz_answers
                  INNER JOIN quizz q ON (q.id = quizz_id AND q.in_use = 1)
                WHERE (user_id = ou.id AND valid_answer = 1)
              )
                >=
              (
                SELECT SUM(q.points)
                FROM user_quizz_answers
                  INNER JOIN quizz q ON (q.id = quizz_id AND q.in_use = 1)
                WHERE (user_id = :id AND valid_answer = 1)
              )
        GROUP BY uqa.user_id
        ORDER BY score ASC
        LIMIT 10;
      ";
    }
    else if ($type == "grades_score")
    {
      $sql = "
        SELECT
          ou.id AS id,
          ou.login AS login,
          COUNT(*) AS score
        FROM users ou
          INNER JOIN grades g ON (g.user_id = ou.id)
        WHERE g.user_id != :id AND
              (
                SELECT COUNT(*)
                FROM grades
                WHERE user_id = ou.id
              )
                >=
              (
                SELECT COUNT(*)
                FROM grades
                WHERE user_id = :id
              )
        GROUP BY g.user_id
        ORDER BY score ASC
        LIMIT 10;
      ";
    }

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the 10 users better ".$type." than ".$user_id.": ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get 10 users worse than specific user
   *
   * @param user_id User id
   * @param Type ("score", or "pvp_score" or "hunting_competition_score")
   *
   * @return List of 10 users worse than user_id (each user having an "id", a "login" and "score")
   *
   * @example print_r(Users::get10UsersWorseThan('score', 1));
   */
  public static function get10UsersWorseThan($type, $user_id)
  {
    if ($type != "score" and $type != "pvp_score" and $type != "hunting_competition_score"
        and $type != "achievements_score" and $type != "quizz_score" and $type != "grades_score")
    {
      error_log("Wrong 10 users worse than ".$user_id." type parameter ".$type);
      return array();
    }

    $sql = "";
    if ($type == "score" or $type == "pvp_score" or $type == "hunting_competition_score")
    {
      $sql = "SELECT ou.id, ou.login, ou.".$type." AS score
              FROM users u
                INNER JOIN users ou ON ou.".$type." < u.".$type."
              WHERE u.id = :id AND u.id != ou.id
              ORDER BY score DESC
              LIMIT 10;";
    }
    else if ($type == "achievements_score")
    {
      $sql = "
        SELECT
          oua.user_id AS id,
          ou.login AS login,
          SUM(oua.grade) AS score
        FROM users ou
          INNER JOIN user_achievements oua ON (oua.user_id = ou.id)
        WHERE oua.user_id != :id AND
              (
                SELECT SUM(grade)
                FROM user_achievements
                WHERE user_id = ou.id
              )
                <
              (
                SELECT SUM(grade)
                FROM user_achievements
                WHERE user_id = :id
              )
        GROUP BY oua.user_id
        ORDER BY score DESC
        LIMIT 10;
      ";
    }
    else if ($type == "quizz_score")
    {
      $sql = "
        SELECT
          ou.id AS id,
          ou.login AS login,
          SUM(q.points) AS score
        FROM users ou
          INNER JOIN user_quizz_answers uqa ON (uqa.user_id = ou.id AND uqa.valid_answer = 1)
          INNER JOIN quizz q ON (q.id = uqa.quizz_id AND q.in_use = 1)
        WHERE uqa.user_id != :id AND
              (
                SELECT SUM(q.points)
                FROM user_quizz_answers
                  INNER JOIN quizz q ON (q.id = quizz_id AND q.in_use = 1)
                WHERE (user_id = ou.id AND valid_answer = 1)
              )
                <
              (
                SELECT SUM(q.points)
                FROM user_quizz_answers
                  INNER JOIN quizz q ON (q.id = quizz_id AND q.in_use = 1)
                WHERE (user_id = :id AND valid_answer = 1)
              )
        GROUP BY uqa.user_id
        ORDER BY score DESC
        LIMIT 10;
      ";
    }
    else if ($type == "grades_score")
    {
      $sql = "
        SELECT
          ou.id AS id,
          ou.login AS login,
          COUNT(*) AS score
        FROM users ou
          INNER JOIN grades g ON (g.user_id = ou.id)
        WHERE g.user_id != :id AND
              (
                SELECT COUNT(*)
                FROM grades
                WHERE user_id = ou.id
              )
                <
              (
                SELECT COUNT(*)
                FROM grades
                WHERE user_id = :id
              )
        GROUP BY g.user_id
        ORDER BY score DESC
        LIMIT 10;
      ";
    }

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $user_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the 10 users worse ".$type." than ".$user_id.": ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get all users begining with $begining_login
   *
   * @param begining_login At least 2 character of the users to search
   *
   * @return List of all users begining with $begining_login (each user having an "id", a "login" and a "language_code")
   *
   * @example print_r(Users::search('test'));
   */
  public static function search($begining_login)
  {
    if (strlen($begining_login) < 2)
    {
      error_log("Tried to search users with search element '" + $begining_login + "'");
      return array();
    }

    $sql = "SELECT id, login, language_code FROM users WHERE login LIKE '".$begining_login."%';";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute();
    }
    catch (PDOException $e)
    {
      error_log("Impossible to search users begining with '".$begining_login."': ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }
}

?>
