<?php

if (!include_once("database.php")) { exit("Could not open database library."); }; // Database

if (!include_once("languages.php")) { exit("Could not open languages library."); }; // Languages

// Attack/Pokemon types
abstract class AttackAndPokemonType
{
  const Normal = 1;
  const Fighting = 2;
  const Flying = 3;
  const Poison = 4;
  const Ground = 5;
  const Rock = 6;
  const Bug = 7;
  const Ghost = 8;
  const Steel = 9;
  const Fire = 10;
  const Water = 11;
  const Grass = 12;
  const Electric = 13;
  const Psychic = 14;
  const Ice = 15;
  const Dragon = 16;
  const Dark = 17;
  const Fairy = 18;
}

// Attack categories
abstract class AttackClasses
{
  const Status = 1;
  const Physical = 2;
  const Special = 3;
}

// Effect of an attack
abstract class AttackEffect
{
  const RegularDamage = 1;
  const PutTargetToSleep = 2;
  const PoisonTarget = 3;
  const DrainHalfReceivedDamage = 4;
  const BurnTarget = 5;
  const FreezeTarget = 6;
  const ParalyzeTarget = 7;
  const SuicideEvenIfAttackMisses = 8;
  const OnlyWorksOnSleepingPokemon = 9;
  const CopyEnnemyAttack = 10;
  const RaiseUserAttackByOne = 11;
  const RaiseUserDefenseByOne = 12;
  const RaiseUserSpecialAttackByOne = 14;
  const RaiseUserEvasionByOne = 17;
  const NeverMisses = 18;
  const LowerTargetAttackByOne = 19;
  const LowerTargetDefenseByOne = 20;
  const LowerTargetSpeedByOne = 21;
  const LowerTargetAccuracyByOne = 24;
  const LowerTargetEvasionByOne = 25;
  const ResetAllStatsOfAllPokemons = 26;
  const WaitTwoTurnsAndGiveBackTwiceDamage = 27;
  const HitEveyTurnThenConfuseUser = 28;
  const EndWildBattleOrForceTrainerSwitchPokemon = 29;
  const HitMultipleTimesInOneTurn = 30;
  const ChangeUserTypeWithRandomAttackType = 31;
  const MakeTheTargetFlinch = 32; // Flinch = Ennemy can't attack for one turn if user attacked first
  const HealUserOfHalfItsMaxHp= 33;
  const BadlyPoisonTarget = 34;
  const GrabMoney = 35;
  const ReduceByHalfSpecialAttacksForFiveTurns = 36;
  const FreezeBurnOrParalyze = 37;
  const SleepTwoTurnsToHealUser = 38;
  const KillTarget= 39;
  const WaitOneTurnThenAttack = 40;
  const HalfRemainingTargetHpDamage = 41;
  const FourtyPointsDamage = 42;
  const HitMultipleTurnsAndPreventEnnemyRunningAway = 43;
  const IncreaseCurrentAttackCriticalHitByOne = 44;
  const HitTwiceInOneTurn = 45;
/*
TODO:
"46"	"9"	"If the user misses, it takes half the damage it would have inflicted in recoil."	"Inflicts [regular damage]{mechanic:regular-damage}.  If this move misses, is blocked by []{move:protect} or []{move:detect}, or has no effect, the user takes half the damage it would have inflicted in recoil.  This recoil damage will not exceed half the user's max HP.
"47"	"9"	"Protects the user's stats from being changed by enemy moves."	"Pokémon on the user's side of the [field]{mechanic:field} are immune to stat-lowering effects for five turns.
"48"	"9"	"Increases the user's chance to score a critical hit."	"User's [critical hit]{mechanic:critical-hit} rate is two levels higher until it leaves the field.  If the user has already used []{move:focus-energy} since entering the field, this move will [fail]{mechanic:fail}.
"49"	"9"	"User receives 1/4 the damage it inflicts in recoil."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/4 the damage it inflicts in recoil."
"50"	"9"	"Confuses the target."	"[Confuse]{mechanic:confuse}s the target."
"51"	"9"	"Raises the user's Attack by two stages."	"Raises the user's [Attack]{mechanic:attack} by two [stages]{mechanic:stage}."
"52"	"9"	"Raises the user's Defense by two stages."	"Raises the user's [Defense]{mechanic:defense} by two [stages]{mechanic:stage}."
"53"	"9"	"Raises the user's Speed by two stages."	"Raises the user's [Speed]{mechanic:speed} by two [stages]{mechanic:stage}."
"54"	"9"	"Raises the user's Special Attack by two stages."	"Raises the user's [Special Attack]{mechanic:special-attack} by two [stages]{mechanic:stage}."
"55"	"9"	"Raises the user's Special Defense by two stages."	"Raises the user's [Special Defense]{mechanic:special-defense} by two [stages]{mechanic:stage}."
"58"	"9"	"User becomes a copy of the target until it leaves battle."	"User copies the target's species, weight, type, [ability]{mechanic:ability}, [calculated stats]{mechanic:calculated-stats} (except [HP]{mechanic:hp}), and moves.  Copied moves will all have 5 [PP]{mechanic:pp} remaining.  [IV]{mechanic:iv}s are copied for the purposes of []{move:hidden-power}, but stats are not recalculated.
"59"	"9"	"Lowers the target's Attack by two stages."	"Lowers the target's [Attack]{mechanic:attack} by two [stages]{mechanic:stage}."
"60"	"9"	"Lowers the target's Defense by two stages."	"Lowers the target's [Defense]{mechanic:defense} by two [stages]{mechanic:stage}."
"61"	"9"	"Lowers the target's Speed by two stages."	"Lowers the target's [Speed]{mechanic:speed} by two [stages]{mechanic:stage}."
"63"	"9"	"Lowers the target's Special Defense by two stages."	"Lowers the target's [Special Defense]{mechanic:special-defense} by two [stages]{mechanic:stage}."
"66"	"9"	"Reduces damage from physical attacks by half."	"Erects a barrier around the user's side of the field that reduces damage from [physical]{mechanic:physical} attacks by half for five turns.  In double battles, the reduction is 1/3.  [Critical hit]{mechanic:critical-hit}s are not affected by the barrier.
"67"	"9"	"Poisons the target."	"[Poisons]{mechanic:poisons} the target."
"68"	"9"	"Paralyzes the target."	"[Paralyzes]{mechanic:paralyzes} the target."
"69"	"9"	"Has a $effect_chance% chance to lower the target's Attack by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Attack]{mechanic:attack} by one [stage]{mechanic:stage}."
"70"	"9"	"Has a $effect_chance% chance to lower the target's Defense by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Defense]{mechanic:defense} by one [stage]{mechanic:stage}."
"71"	"9"	"Has a $effect_chance% chance to lower the target's Speed by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Speed]{mechanic:speed} by one [stage]{mechanic:stage}."
"72"	"9"	"Has a $effect_chance% chance to lower the target's Special Attack by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Special Attack]{mechanic:special-attack} by one [stage]{mechanic:stage}."
"73"	"9"	"Has a $effect_chance% chance to lower the target's Special Defense by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage}."
"74"	"9"	"Has a $effect_chance% chance to lower the target's accuracy by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [accuracy]{mechanic:accuracy} by one [stage]{mechanic:stage}."
"76"	"9"	"User charges for one turn before attacking.  Has a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  User charges for one turn before attacking.  [Critical hit chance]{mechanic:critical-hit-chance} is one level higher than normal.  Has a $effect_chance% chance to make the target [flinch]{mechanic:flinch}.
"77"	"9"	"Has a $effect_chance% chance to confuse the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [confuse]{mechanic:confuse} the target."
"78"	"9"	"Hits twice in the same turn.  Has a $effect_chance% chance to poison the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Hits twice in the same turn.  Has a $effect_chance% chance to [poison]{mechanic:poison} the target."
"79"	"9"	"Never misses."	"Inflicts [regular damage]{mechanic:regular-damage}.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers."
"80"	"9"	"Transfers 1/4 of the user's max HP into a doll, protecting the user from further damage or status changes until it breaks."	"Transfers 1/4 the user's max [HP]{mechanic:hp} into a doll that absorbs damage and causes most negative move effects to [fail]{mechanic:fail}.  If the user leaves the [field]{mechanic:field}, the doll will vanish.  If the user cannot pay the [HP]{mechanic:hp} cost, this move will fail.
"81"	"9"	"User foregoes its next turn to recharge."	"Inflicts [regular damage]{mechanic:regular-damage}.  User loses its next turn to "recharge", and cannot attack or [switch]{mechanic:switch} out during that turn."
"82"	"9"	"If the user is hit after using this move, its Attack rises by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Every time the user is hit after it uses this move but before its next action, its [Attack]{mechanic:attack} raises by one [stage]{mechanic:stage}."
"83"	"9"	"Copies the target's last used move."	"This move is replaced by the target's last successfully used move, and its PP changes to 5.  If the target hasn't used a move since entering the field, if it tried to use a move this turn and [failed]{mechanic:fail}, or if the user already knows the targeted move, this move will fail.  This effect vanishes when the user leaves the field.
"84"	"9"	"Randomly selects and uses any move in the game."	"Selects any move at random and uses it.  Moves the user already knows are not eligible.  Assist, meta, protection, and reflection moves are also not eligible; specifically, []{move:assist}, []{move:chatter}, []{move:copycat}, []{move:counter}, []{move:covet}, []{move:destiny-bond}, []{move:detect}, []{move:endure}, []{move:feint}, []{move:focus-punch}, []{move:follow-me}, []{move:helping-hand}, []{move:me-first}, []{move:metronome}, []{move:mimic}, []{move:mirror-coat}, []{move:mirror-move}, []{move:protect}, []{move:quick-guard}, []{move:sketch}, []{move:sleep-talk}, []{move:snatch}, []{move:struggle}, []{move:switcheroo}, []{move:thief}, []{move:trick}, and []{move:wide-guard} will not be selected by this move.
"85"	"9"	"Seeds the target, stealing HP from it every turn."	"Plants a seed on the target that [drains]{mechanic:drain} 1/8 of its max [HP]{mechanic:hp} at the end of every turn and heals the user for the amount taken.  Has no effect on []{type:grass} Pokémon.  The seed remains until the target leaves the field.
"86"	"9"	"Does nothing."	"Does nothing.
"87"	"9"	"Disables the target's last used move for 1-8 turns."	"Disables the target's last used move, preventing its use for 4–7 turns, selected at random, or until the target leaves the [field]{mechanic:field}.  If the target hasn't used a move since entering the [field]{mechanic:field}, if it tried to use a move this turn and [failed]{mechanic:failed},  if its last used move has 0 PP remaining, or if it already has a move disabled, this move will fail."
"88"	"9"	"Inflicts damage equal to the user's level."	"Inflicts damage equal to the user's level.  Type immunity applies, but other type effects are ignored."
"89"	"9"	"Inflicts damage between 50% and 150% of the user's level."	"Inflicts [typeless]{mechanic:typeless} damage between 50% and 150% of the user's level, selected at random in increments of 10%."
"90"	"9"	"Inflicts twice the damage the user received from the last physical hit it took."	"Targets the last opposing Pokémon to hit the user with a [physical]{mechanic:physical} move this turn.  Inflicts twice the damage that move did to the user.  If there is no eligible target, this move will [fail]{mechanic:fail}.  Type immunity applies, but other type effects are ignored.
"91"	"9"	"Forces the target to repeat its last used move every turn for 2 to 6 turns."	"The next 4–8 times (selected at random) the target attempts to move, it is forced to repeat its last used move.  If the selected move allows the trainer to select a target, an opponent will be selected at random each turn.  If the target is prevented from using the selected move by some other effect, []{move:struggle} will be used instead.  This effect ends if the selected move runs out of [PP]{mechanic:pp}.
"92"	"9"	"Sets the user's and targets's HP to the average of their current HP."	"Changes the user's and target's remaining [HP]{mechanic:hp} to the average of their current remaining [HP]{mechanic:hp}.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.  This effect does not count as inflicting damage for other moves and effects that respond to damage taken.
"93"	"9"	"Has a $effect_chance% chance to make the target flinch.  Only works if the user is sleeping."	"Only usable if the user is [sleep]{mechanic:sleep}ing.  Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to make the target []{mechanic:flinch}."
"94"	"9"	"Changes the user's type to a random type either resistant or immune to the last move used against it."	"Changes the user's type to a type either resistant or immune to the last damaging move that hit it.  The new type is selected at random and cannot be a type the user already is.  If there is no eligible new type, this move will [fail]{mechanic:fail}."
"95"	"9"	"Ensures that the user's next move will hit the target."	"If the user targets the same target again before the end of the next turn, the move it uses is guaranteed to hit.  This move itself also ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.
"96"	"9"	"Permanently becomes the target's last used move."	"Permanently replaces itself with the target's last used move.  If that move is []{move:chatter} or []{move:struggle}, this move will [fail]{mechanic:fail}.
"98"	"9"	"Randomly uses one of the user's other three moves.  Only works if the user is sleeping."	"Only usable if the user is [sleep]{mechanic:sleep}ing.  Randomly selects and uses one of the user's other three moves.  Use of the selected move requires and costs 0 [PP]{mechanic:pp}.
"99"	"9"	"If the user faints this turn, the target automatically will, too."	"If the user [faints]{mechanic:faints} before its next move, the Pokémon that [faint]{mechanic:faint}ed it will automatically [faint]{mechanic:faint}.  End-of-turn damage is ignored.
"100"	"9"	"Inflicts more damage when the user has less HP remaining, with a maximum of 200 power."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power varies inversely with the user's proportional remaining [HP]{mechanic:hp}.
"101"	"9"	"Lowers the PP of the target's last used move by 4."	"Lowers the PP of the target's last used move by 4.  If the target hasn't used a move since entering the [field]{mechanic:field}, if it tried to use a move this turn and [failed]{mechanic:failed}, or if its last used move has 0 PP remaining, this move will fail."
"102"	"9"	"Cannot lower the target's HP below 1."	"Inflicts [regular damage]{mechanic:regular-damage}.  Will not reduce the target's [HP]{mechanic:hp} below 1."
"103"	"9"	"Cures the entire party of major status effects."	"Removes [major status effects]{mechanic:major-status-effects} and [confusion]{mechanic:confusion} from every Pokémon in the user's party."
"104"	"9"	"Inflicts regular damage with no additional effect."	"Inflicts [regular damage]{mechanic:regular-damage}."
"105"	"9"	"Hits three times, increasing power by 100% with each successful hit."	"Inflicts [regular damage]{mechanic:regular-damage}.  Hits three times in the same turn.  The second hit has double power, and the third hit has triple power.  Each hit has a separate [accuracy]{mechanic:accuracy} check, and this move stops if a hit misses.
"106"	"9"	"Takes the target's item."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is holding an item and the user is not, the user will permanently take the item.  Damage is still inflicted if an item cannot be taken.
"107"	"9"	"Prevents the target from leaving battle."	"The target cannot [switch out]{mechanic:switch-out} normally.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.  This effect ends when the user leaves the [field]{mechanic:field}.
"108"	"9"	"Target loses 1/4 its max HP every turn as long as it's asleep."	"Only works on [sleep]{mechanic:sleep}ing Pokémon.  Gives the target a nightmare, damaging it for 1/4 its max [HP]{mechanic:hp} every turn.  If the target wakes up or leaves the [field]{mechanic:field}, this effect ends."
"109"	"9"	"Raises the user's evasion by two stages."	"Raises the user's [evasion]{mechanic:evasion} by two [stages]{mechanic:stage}.
"110"	"9"	"Ghosts pay half their max HP to hurt the target every turn.  Others decrease Speed but raise Attack and Defense."	"If the user is a []{type:ghost}: user pays half its max [HP]{mechanic:hp} to place a curse on the target, damaging it for 1/4 its max [HP]{mechanic:hp} every turn.
"112"	"9"	"Prevents any moves from hitting the user this turn."	"No moves will hit the user for the remainder of this turn.  If the user is last to act this turn, this move will [fail]{mechanic:fail}.
"113"	"9"	"Scatters Spikes, hurting opposing Pokémon that switch in."	"Scatters spikes around the opposing [field]{mechanic:field}, which damage opposing Pokémon that enter the [field]{mechanic:field} for 1/8 of their max [HP]{mechanic:hp}.  Pokémon immune to []{type:ground} moves are immune to this damage, except during []{move:gravity}.  Up to three layers of spikes may be laid down, adding 1/16 to the damage done: two layers of spikes damage for 3/16 max [HP]{mechanic:hp}, and three layers damage for 1/4 max [HP]{mechanic:hp}.
"114"	"9"	"Forces the target to have no Evade, and allows it to be hit by Normal and Fighting moves even if it's a Ghost."	"Resets the target's [evasion]{mechanic:evasion} to normal and prevents any further boosting until the target leaves the [field]{mechanic:field}.  A []{type:ghost} under this effect takes normal damage from []{type:normal} and []{type:fighting} moves.  This move itself ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers."
"115"	"9"	"User and target both faint after three turns."	"Every Pokémon is given a counter that starts at 3 and decreases by 1 at the end of every turn, including this one.  When a Pokémon's counter reaches zero, that Pokémon [faint]{mechanic:faint}s.  A Pokémon that leaves the [field]{mechanic:field} will lose its counter; its replacement does not inherit the effect, and other Pokémon's counters remain.
"116"	"9"	"Changes the weather to a sandstorm for five turns."	"Changes the weather to a sandstorm for five turns.  Pokémon that are not []{type:ground}, []{type:rock}, or []{type:steel} take 1/16 their max [HP]{mechanic:hp} at the end of every turn.  Every []{type:rock} Pokémon's original [Special Defense]{mechanic:special-defense} is raised by 50% for the duration of this effect.
"117"	"9"	"Prevents the user's HP from lowering below 1 this turn."	"The user's [HP]{mechanic:hp} cannot be lowered below 1 by any means for the remainder of this turn.
"118"	"9"	"Power doubles every turn this move is used in succession after the first, resetting after five turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  User is forced to use this move for five turns.  Power doubles every time this move is used in succession to a maximum of 16x, and resets to normal after the lock-in ends.  If this move misses or becomes unusable, the lock-in ends.
"119"	"9"	"Raises the target's Attack by two stages and confuses the target."	"Raises the target's [Attack]{mechanic:attack} by two [stages]{mechanic:stage}, then [confuses]{mechanic:confuses} it.  If the target's [Attack]{mechanic:attack} cannot be [raised]{mechanic:raised} by two [stages]{mechanic:stage}, the [confusion]{mechanic:confusion} is not applied."
"120"	"9"	"Power doubles every turn this move is used in succession after the first, maxing out after five turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power doubles after every time this move is used, whether consecutively or not, maxing out at 16x.  If this move misses or the user leaves the [field]{mechanic:field}, power resets."
"121"	"9"	"Target falls in love if it has the opposite gender, and has a 50% chance to refuse attacking the user."	"Causes the target to fall in love with the user, giving it a 50% chance to do nothing each turn.  If the user and target are the same gender, or either is genderless, this move will [fail]{mechanic:fail}.  If either Pokémon leaves the [field]{mechanic:field}, this effect ends."
"122"	"9"	"Power increases with happiness, up to a maximum of 102."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power increases with [happiness]{mechanic:happiness}, given by `happiness * 2 / 5`, to a maximum of 102.  Power bottoms out at 1."
"123"	"9"	"Randomly inflicts damage with power from 40 to 120 or heals the target for 1/4 its max HP."	"Randomly uses one of the following effects.
"124"	"9"	"Power increases as happiness decreases, up to a maximum of 102."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power increases inversely with [happiness]{mechanic:happiness}, given by `(255 - happiness) * 2 / 5`, to a maximum of 102.  Power bottoms out at 1."
"125"	"9"	"Protects the user's field from major status ailments and confusion for five turns."	"Protects Pokémon on the user's side of the [field]{mechanic:field} from [major status]{mechanic:major-status} effects and [confusion]{mechanic:confusion} for five turns.  Does not cancel existing ailments.  This effect remains even if the user leaves the [field]{mechanic:field}.
"126"	"9"	"Has a $effect_chance% chance to [burn]{mechanic:burn} the target.  Lets frozen Pokémon thaw themselves."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target.  [Frozen]{mechanic:frozen} Pokémon may use this move, in which case they will thaw."
"127"	"9"	"Power varies randomly from 10 to 150."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power is selected at random between 10 and 150, with an average of 71:
"128"	"9"	"Allows the trainer to switch out the user and pass effects along to its replacement."	"User [switches out]{mechanic:switches-out}, and the trainer selects a replacement Pokémon from the party.  [Stat]{mechanic:stat} changes, [confusion]{mechanic:confusion}, and persistent move effects are passed along to the replacement Pokémon.
"129"	"9"	"Has double power against, and can hit, Pokémon attempting to switch out."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target attempts to [switch out]{mechanic:switch-out} this turn before the user acts, this move hits the target before it leaves and has double power.
"130"	"9"	"Frees the user from binding moves, removes Leech Seed, and blows away Spikes."	"Inflicts [regular damage]{mechanic:regular-damage}.  Removes []{move:leech-seed} from the user, frees the user from []{move:bind}, []{move:clamp}, []{move:fire-spin}, []{move:magma-storm}, []{move:sand-tomb}, []{move:whirlpool}, and []{move:wrap}, and clears []{move:spikes}, []{move:stealth-rock}, and []{move:toxic-spikes} from the user's side of the [field]{mechanic:field}.  If this move misses or has [no effect]{mechanic:no-effect}, its effect doesn't activate."
"131"	"9"	"Inflicts 20 points of damage."	"Inflicts exactly 20 damage."
"133"	"9"	"Heals the user by half its max HP.  Affected by weather."	"Heals the user for half its max [HP]{mechanic:hp}.
"136"	"9"	"Power and type depend upon user's IVs.  Power can range from 30 to 70."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power and type are determined by the user's [IV]{mechanic:iv}s.
"137"	"9"	"Changes the weather to rain for five turns."	"Changes the weather to rain for five turns, during which []{type:water} moves inflict 50% extra damage, and []{type:fire} moves inflict half damage.
"138"	"9"	"Changes the weather to sunny for five turns."	"Changes the weather to sunshine for five turns, during which []{type:fire} moves inflict 50% extra damage, and []{type:water} moves inflict half damage.
"139"	"9"	"Has a $effect_chance% chance to raise the user's Defense by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}. Has a $effect_chance% chance to raise the user's [Defense]{mechanic:defense} one [stage]{mechanic:stage}."
"140"	"9"	"Has a $effect_chance% chance to raise the user's Attack by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}. Has a $effect_chance% chance to raise the user's [Attack]{mechanic:attack} one [stage]{mechanic:stage}."
"141"	"9"	"Has a $effect_chance% chance to raise all of the user's stats by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}. Has a $effect_chance% chance to raise all of the user's stats one [stage]{mechanic:stage}."
"143"	"9"	"User pays half its max HP to max out its Attack."	"User pays half its max [HP]{mechanic:hp} to raise its [Attack]{mechanic:attack} to +6 [stages]{mechanic:stage}.  If the user cannot pay the [HP]{mechanic:hp} cost, this move will [fail]{mechanic:fail}."
"144"	"9"	"Discards the user's stat changes and copies the target's."	"Discards the user's [stat changes]{mechanic:stat-changes} and copies the target's.
"145"	"9"	"Inflicts twice the damage the user received from the last special hit it took."	"Targets the last opposing Pokémon to hit the user with a [special]{mechanic:special} move this turn.  Inflicts twice the damage that move did to the user.  If there is no eligible target, this move will [fail]{mechanic:fail}.  Type immunity applies, but other type effects are ignored.
"146"	"9"	"Raises the user's Defense by one stage.  User charges for one turn before attacking."	"Inflicts [regular damage]{mechanic:regular-damage}.  Raises the user's [Defense]{mechanic:defense} by one [stage]{mechanic:stage}.  User then charges for one turn before attacking.
"147"	"9"	"Has a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to make each target [flinch]{mechanic:flinch}.
"148"	"9"	"Inflicts regular damage and can hit Dig users."	"Inflicts [regular damage]{mechanic:regular-damage}.
"149"	"9"	"Hits the target two turns later."	"Inflicts [typeless]{mechanic:typeless} [regular damage]{mechanic:regular-damage} at the end of the third turn, starting with this one.  This move cannot score a [critical hit]{mechanic:critical-hit}.  If the target [switches out]{mechanic:switches-out}, its replacement will be hit instead.  Damage is calculated at the time this move is used; [stat changes]{mechanic:stat-changes} and [switching out]{mechanic:switching-out} during the delay won't change the damage inflicted.  No move with this effect can be used against the same target again until after the end of the third turn.
"150"	"9"	"Inflicts regular damage and can hit Pokémon in the air."	"Inflicts [regular damage]{mechanic:regular-damage}.
"151"	"9"	"Has a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to make the target [flinch]{mechanic:flinch}.
"152"	"9"	"Requires a turn to charge before attacking."	"Inflicts [regular damage]{mechanic:regular-damage}.  User charges for one turn before attacking.
"153"	"9"	"Has a $effect_chance% chance to paralyze the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [paralyze]{mechanic:paralyze} the target.
"154"	"9"	"Immediately ends wild battles.  No effect otherwise."	"Does nothing.  Wild battles end immediately."
"155"	"9"	"Hits once for every conscious Pokémon the trainer has."	"Inflicts [typeless]{mechanic:typeless} [regular damage]{mechanic:regular-damage}.  Every Pokémon in the user's party, excepting those that have fainted or have a [major status effect]{mechanic:major-status-effect}, attacks the target.  Calculated stats are ignored; the base stats for the target and assorted attackers are used instead.  The random factor in the damage formula is not used.  []{type:dark} Pokémon still get [STAB]{mechanic:stab}.
"156"	"9"	"User flies high into the air, dodging all attacks, and hits next turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  User flies high into the air for one turn, becoming immune to attack, and hits on the second turn.
"157"	"9"	"Raises user's Defense by one stage."	"Raises user's [Defense]{mechanic:defense} by one [stage]{mechanic:stage}.
"159"	"9"	"Can only be used as the first move after the user enters battle.  Causes the target to flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Causes the target to []{mechanic:flinch}.  Can only be used on the user's first turn after entering the [field]{mechanic:field}."
"160"	"9"	"Forced to use this move for several turns.  Pokémon cannot fall asleep in that time."	"Inflicts [regular damage]{mechanic:regular-damage}.  User is forced to use this move for 2–5 turns, selected at random.  All Pokémon on the [field]{mechanic:field} wake up, and none can fall to [sleep]{mechanic:sleep} until the lock-in ends.
"161"	"9"	"Stores energy up to three times for use with Spit Up and Swallow."	"Raises the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage} each.  Stores energy for use with []{move:spit-up} and []{move:swallow}.  Up to three levels of energy can be stored, and all are lost if the user leaves the [field]{mechanic:field}.  Energy is still stored even if the stat boosts cannot be applied.
"162"	"9"	"Power is 100 times the amount of energy Stockpiled."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power is equal to 100 times the amount of energy stored by []{move:stockpile}.  Ignores the random factor in the damage formula.  Stored energy is consumed, and the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} are reset to what they would be if []{move:stockpile} had not been used.  If the user has no energy stored, this move will [fail]{mechanic:fail}.
"163"	"9"	"Recovers 1/4 HP after one Stockpile, 1/2 HP after two Stockpiles, or full HP after three Stockpiles."	"Heals the user depending on the amount of energy stored by []{move:stockpile}: 1/4 its max [HP]{mechanic:hp} after one use, 1/2 its max [HP]{mechanic:hp} after two uses, or fully after three uses.  Stored energy is consumed, and the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} are reset to what they would be if []{move:stockpile} had not been used.  If the user has no energy stored, this move will [fail]{mechanic:fail}."
"165"	"9"	"Changes the weather to a hailstorm for five turns."	"Changes the weather to hail for five turns, during which non-[]{type:ice} Pokémon are damaged for 1/16 their max [HP]{mechanic:hp} at the end of every turn.
"166"	"9"	"Prevents the target from using the same move twice in a row."	"Prevents the target from attempting to use the same move twice in a row.  When the target leaves the [field]{mechanic:field}, this effect ends.
"167"	"9"	"Raises the target's Special Attack by one stage and confuses the target."	"Raises the target's [Special Attack]{mechanic:special-attack} by one [stage]{mechanic:stage}, then [confuses]{mechanic:confuses} it."
"168"	"9"	"Burns the target."	"[Burns]{mechanic:burns} the target."
"169"	"9"	"Lowers the target's Attack and Special Attack by two stages.  User faints."	"Lowers the target's [Attack]{mechanic:attack} and [Special Attack]{mechanic:special-attack} by two [stages]{mechanic:stage}.  User faints."
"170"	"9"	"Power doubles if user is burned, paralyzed, or poisoned."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the user is [burned]{mechanic:burned}, [paralyzed]{mechanic:paralyzed}, or [poisoned]{mechanic:poisoned}, this move has double power."
"171"	"9"	"If the user takes damage before attacking, the attack is canceled."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the user takes damage this turn before hitting, this move will [fail]{mechanic:fail}.
"172"	"9"	"If the target is paralyzed, inflicts double damage and cures the paralysis."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is [paralyzed]{mechanic:paralyzed}, this move has double power, and the target is cured of its [paralysis]{mechanic:paralysis}."
"173"	"9"	"Redirects the target's single-target effects to the user for this turn."	"Until the end of this turn, any moves that opposing Pokémon target solely at the user's ally will instead target the user.  If both Pokémon on the same side of the [field]{mechanic:field} use this move on the same turn, the Pokémon that uses it last will become the target.
"174"	"9"	"Uses a move which depends upon the terrain."	"Uses another move chosen according to the terrain.
"175"	"9"	"Raises the user's Special Defense by one stage.  User's Electric moves have doubled power next turn."	"Raises the user's [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage}.  If the user uses an []{type:electric} move next turn, its power will be doubled."
"176"	"9"	"For the next few turns, the target can only use damaging moves."	"Target is forced to only use damaging moves for the next 3–5 turns, selected at random.  Moves that select other moves not known in advance do not count as damaging.
"177"	"9"	"Ally's next move inflicts half more damage."	"Boosts the power of the target's moves by 50% until the end of this turn.
"178"	"9"	"User and target swap items."	"User and target permanently swap [held item]{mechanic:held-item}s.  Works even if one of the Pokémon isn't holding anything.  If either Pokémon is holding mail, this move will [fail]{mechanic:fail}.
"179"	"9"	"Copies the target's ability."	"User's ability is replaced with the target's until the user leaves the [field]{mechanic:field}.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.
"180"	"9"	"User will recover half its max HP at the end of the next turn."	"At the end of the next turn, user will be healed for half its max [HP]{mechanic:hp}.  If the user is [switched out]{mechanic:switched-out}, its replacement will be healed instead for half of the user's max HP.  If the user [faint]{mechanic:faint}s or is forcefully switched by []{move:roar} or []{move:whirlwind}, this effect will not activate."
"181"	"9"	"Randomly selects and uses one of the trainer's other Pokémon's moves."	"Uses a move from another Pokémon in the user's party, both selected at random.  Moves from fainted Pokémon can be used.  If there are no eligible Pokémon or moves, this move will [fail]{mechanic:fail}.
"182"	"9"	"Prevents the user from leaving battle.  User regains 1/16 of its max HP every turn."	"Prevents the user from [switching out]{mechanic:switching-out}.  User regains 1/16 of its max [HP]{mechanic:hp} at the end of every turn.  If the user was immune to []{type:ground} attacks, it will now take normal damage from them.
"183"	"9"	"Lowers the user's Attack and Defense by one stage after inflicting damage."	"Inflicts [regular damage]{mechanic:regular-damage}, then lowers the user's [Attack]{mechanic:attack} and [Defense]{mechanic:defense} by one [stage]{mechanic:stage} each."
"184"	"9"	"Reflects back the first effect move used on the user this turn."	"The first non-damaging move targeting the user this turn that inflicts [major status effect]{mechanic:major-status-effect}s, [stat change]{mechanic:stat-change}s, or [trap]{mechanic:trap}ping effects will be reflected at its user.
"185"	"9"	"User recovers the item it last used up."	"User recovers the last item consumed by the user or a Pokémon in its position on the [field]{mechanic:field}.  The item must be used again before it can be recovered by this move again.  If the user is holding an item, this move [fail]{mechanic:fail}s.
"186"	"9"	"Inflicts double damage if the user takes damage before attacking this turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target damaged the user this turn and was the last to do so, this move has double power.
"187"	"9"	"Destroys Reflect and Light Screen."	"Destroys any []{move:light-screen} or []{move:reflect} on the target's side of the [field]{mechanic:field}, then inflicts [regular damage]{mechanic:regular-damage}."
"188"	"9"	"Target sleeps at the end of the next turn."	"Puts the target to [sleep]{mechanic:sleep} at the end of the next turn.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.  If the target leaves the [field]{mechanic:field}, this effect is canceled.  If the target has a status effect when this move is used, this move will [fail]{mechanic:fail}.
"189"	"9"	"Target drops its held item."	"Inflicts [regular damage]{mechanic:regular-damage}.  Target loses its [held item]{mechanic:held-item}.
"190"	"9"	"Lowers the target's HP to equal the user's."	"Inflicts exactly enough damage to lower the target's [HP]{mechanic:hp} to equal the user's.  If the target's HP is not higher than the user's, this move has no effect.  Type immunity applies, but other type effects are ignored.  This effect counts as damage for moves that respond to damage."
"191"	"9"	"Inflicts more damage when the user has more HP remaining, with a maximum of 150 power."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power increases with the user's remaining [HP]{mechanic:hp} and is given by `150 * HP / max HP`, to a maximum of 150 when the user has full HP."
"192"	"9"	"User and target swap abilities."	"User and target switch abilities.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.
"193"	"9"	"Prevents the target from using any moves that the user also knows."	"Prevents any Pokémon on the opposing side of the [field]{mechanic:field} from using any move the user knows until the user leaves the [field]{mechanic:field}.  This effect is live; if the user obtains new moves while on the [field]{mechanic:field}, these moves become restricted.  If no opposing Pokémon knows any of the user's moves when this move is used, this move will [fail]{mechanic:fail}."
"194"	"9"	"Cleanses the user of a burn, paralysis, or poison."	"Removes a [burn]{mechanic:burn}, [paralysis]{mechanic:paralysis}, or [poison]{mechanic:poison} from the user."
"195"	"9"	"If the user faints this turn, the PP of the move that fainted it drops to 0."	"If the user [faint]{mechanic:faint}s before it next acts, the move that fainted it will have its [PP]{mechanic:pp} dropped to 0.  End-of-turn damage does not trigger this effect."
"196"	"9"	"Steals the target's move, if it's self-targeted."	"The next time a Pokémon uses a beneficial move on itself or itself and its ally this turn, the user of this move will steal the move and use it itself.  Moves which may be stolen by this move are identified by the "snatchable" flag.
"197"	"9"	"Inflicts more damage to heavier targets, with a maximum of 120 power."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power increases with the target's weight in kilograms, to a maximum of 120.
"198"	"9"	"Has a $effect_chance% chance to inflict a status effect which depends upon the terrain."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to inflict an effect chosen according to the terrain.
"199"	"9"	"User receives 1/3 the damage inflicted in recoil."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/3 the damage it inflicts in recoil."
"200"	"9"	"Confuses the target."	"[Confuse]{mechanic:confuse}s all targets."
"201"	"9"	"Has an increased chance for a critical hit and a $effect_chance% chance to [burn]{mechanic:burn} the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  User's [critical hit]{mechanic:critical-hit} rate is one level higher when using this move. Has a $effect_chance% chance to [burn]{mechanic:burn} the target."
"202"	"9"	"Halves all Electric-type damage."	"[]{type:electric} moves inflict half damage, regardless of target.  If the user leaves the [field]{mechanic:field}, this effect ends."
"203"	"9"	"Has a $effect_chance% chance to badly poison the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [badly poison]{mechanic:badly-poison} the target."
"204"	"9"	"If there be weather, this move has doubled power and the weather's type."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a weather move is active, this move has double power, and its type becomes the type of the weather move.  []{move:shadow-sky} is typeless for the purposes of this move."
"205"	"9"	"Lowers the user's Special Attack by two stages after inflicting damage."	"Inflicts [regular damage]{mechanic:regular-damage}, then lowers the user's [Special Attack]{mechanic:special-attack} by two [stages]{mechanic:stage}."
"206"	"9"	"Lowers the target's Attack and Defense by one stage."	"Lowers the target's [Attack]{mechanic:attack} and [Defense]{mechanic:defense} by one [stage]{mechanic:stage}."
"207"	"9"	"Raises the user's Defense and Special Defense by one stage."	"Raises the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage}."
"208"	"9"	"Inflicts regular damage and can hit Bounce and Fly users."	"Inflicts [regular damage]{mechanic:regular-damage}.
"209"	"9"	"Raises the user's Attack and Defense by one stage."	"Raises the user's [Attack]{mechanic:attack} and [Defense]{mechanic:defense} by one [stage]{mechanic:stage} each."
"210"	"9"	"Has an increased chance for a critical hit and a $effect_chance% chance to poison the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  User's [critical hit]{mechanic:critical-hit} rate is one level higher when using this move. Has a $effect_chance% chance to [poison]{mechanic:poison} the target."
"211"	"9"	"Halves all Fire-type damage."	"[]{type:fire} moves inflict half damage, regardless of target.  If the user leaves the [field]{mechanic:field}, this effect ends."
"212"	"9"	"Raises the user's Special Attack and Special Defense by one stage."	"Raises the user's [Special Attack]{mechanic:special-attack} and [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage} each."
"213"	"9"	"Raises the user's Attack and Speed by one stage."	"Raises the user's [Attack]{mechanic:attack} and [Speed]{mechanic:speed} by one [stage]{mechanic:stage} each."
"214"	"9"	"User's type changes to match the terrain."	"User's type changes according to the terrain.
"215"	"9"	"Heals the user by half its max HP."	"Heals the user for half its max [HP]{mechanic:hp}.  If the user is []{type:flying}, its []{type:flying} type is ignored until the end of this turn."
"216"	"9"	"Disables moves and immunities that involve flying or levitating for five turns."	"For five turns (including this one), all immunities to []{type:ground} moves are disabled.  For the duration of this effect, the [evasion]{mechanic:evasion} of every Pokémon on the field is lowered by two [stages]{mechanic:stage}.  Cancels the effects of []{move:bounce}, []{move:fly}, and []{move:sky-drop}.
"217"	"9"	"Forces the target to have no evasion, and allows it to be hit by Psychic moves even if it's Dark."	"Resets the target's [evasion]{mechanic:evasion} to normal and prevents any further boosting until the target leaves the [field]{mechanic:field}.  A []{type:dark} Pokémon under this effect takes normal damage from []{type:psychic} moves.  This move itself ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers."
"218"	"9"	"If the target is asleep, has double power and wakes it up."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is [sleep]{mechanic:sleep}ing, this move has double power, and the target wakes up."
"219"	"9"	"Lowers user's Speed by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}, then lowers the user's [Speed]{mechanic:speed} by one [stage]{mechanic:stage}."
"220"	"9"	"Power raises when the user has lower Speed, up to a maximum of 150."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power increases with the target's current [Speed]{mechanic:speed} compared to the user, given by `1 + 25 * target Speed / user Speed`, capped at 150."
"221"	"9"	"User faints.  Its replacement has its HP fully restored and any major status effect removed."	"User faints.  Its replacement's [HP]{mechanic:hp} is fully restored, and any [major status effect]{mechanic:major-status-effect} is removed.  If the replacement Pokémon is immediately fainted by a switch-in effect, the next replacement is healed by this move instead."
"222"	"9"	"Has double power against Pokémon that have less than half their max HP remaining."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target has less than half its max [HP]{mechanic:hp} remaining, this move has double power."
"223"	"9"	"Power and type depend on the held berry."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power and type are determined by the user's held berry.  The berry is consumed.  If the user is not holding a berry, this move will [fail]{mechanic:fail}."
"224"	"9"	"Hits through Protect and Detect."	"Inflicts [regular damage]{mechanic:regular-damage}.  Removes the effects of []{move:detect} or []{move:protect} from the target before hitting.
"225"	"9"	"If target has a berry, inflicts double damage and uses the berry."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is holding a berry, this move has double power, and the user takes the berry and uses it immediately.
"226"	"9"	"For three turns, friendly Pokémon have doubled Speed."	"For the next three turns, all Pokémon on the user's side of the [field]{mechanic:field} have their original [Speed]{mechanic:speed} doubled.  This effect remains if the user leaves the [field]{mechanic:field}."
"227"	"9"	"Raises one of a friendly Pokémon's stats at random by two stages."	"Raises one of the target's stats by two [stages]{mechanic:stage}.  The raised stat is chosen at random from any stats that can be raised by two stages.  If no stat is eligible, this move will [fail]{mechanic:fail}.
"228"	"9"	"Strikes back at the last Pokémon to hit the user this turn with 1.5× the damage."	"Targets the last opposing Pokémon to hit the user with a damaging move this turn.  Inflicts 1.5× the damage that move did to the user.  If there is no eligible target, this move will [fail]{mechanic:fail}.  Type immunity applies, but other type effects are ignored."
"229"	"9"	"User must switch out after attacking."	"Inflicts [regular damage]{mechanic:regular-damage}, then the user immediately [switches out]{mechanic:switches-out}, and the trainer selects a replacement Pokémon from the party.  If the target [faint]{mechanic:faint}s from this attack, the user's trainer selects the new Pokémon to send out first.  If the user is the last Pokémon in its party that can battle, it will not [switch out]{mechanic:switch-out}.
"230"	"9"	"Lowers the user's Defense and Special Defense by one stage after inflicting damage."	"Inflicts [regular damage]{mechanic:regular-damage}, then lowers the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} by one [stage]{mechanic:stage} each."
"231"	"9"	"Power is doubled if the target has already moved this turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target uses a move or [switches out]{mechanic:switches-out} this turn before this move is used, this move has double power."
"232"	"9"	"Power is doubled if the target has already received damage this turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target takes damage this turn for any reason before this move is used, this move has double power."
"233"	"9"	"Target cannot use held items."	"Target cannot use its held item for five turns.  If the target leaves the [field]{mechanic:field}, this effect ends.
"234"	"9"	"Throws held item at the target; power depends on the item."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power and type are determined by the user's [held item]{mechanic:held-item}.  The item is consumed.  If the user is not holding an item, or its item has no set type and power, this move will [fail]{mechanic:fail}.
"235"	"9"	"Transfers the user's major status effect to the target."	"If the user has a [major status effect]{mechanic:major-status-effect} and the target does not, the user's status is transferred to the target."
"236"	"9"	"Power increases when this move has less PP, up to a maximum of 200."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power is determined by the [PP]{mechanic:pp} remaining for this move, after its [PP]{mechanic:pp} cost is deducted.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.
"237"	"9"	"Prevents target from restoring its HP for five turns."	"For the next five turns, the target may not use any moves that only restore [HP]{mechanic:hp}, and move effects that heal the target are disabled.  Moves that steal [HP]{mechanic:hp} may still be used, but will only inflict damage and not heal the target."
"238"	"9"	"Power increases against targets with more HP remaining, up to a maximum of 121 power."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power directly relates to the target's relative remaining [HP]{mechanic:hp}, given by `1 + 120 * current HP / max HP`, to a maximum of 121."
"239"	"9"	"User swaps Attack and Defense."	"The user's original [Attack]{mechanic:attack} and [Defense]{mechanic:defense} are swapped.
"240"	"9"	"Nullifies target's ability until it leaves battle."	"The target's ability is disabled as long as it remains on the [field]{mechanic:field}.
"241"	"9"	"Prevents the target from scoring critical hits for five turns."	"For five turns, opposing Pokémon cannot score [critical hits]{mechanic:critical-hit}."
"242"	"9"	"Uses the target's move against it before it attacks, with power increased by half."	"If the target has selected a damaging move this turn, the user will copy that move and use it against the target, with a 50% increase in power.
"243"	"9"	"Uses the target's last used move."	"Uses the last move that was used successfully by any Pokémon, including the user.
"244"	"9"	"User swaps Attack and Special Attack changes with the target."	"User swaps its [Attack]{mechanic:attack} and [Special Attack]{mechanic:special-attack} [stat modifiers]{mechanic:stat-modifiers} modifiers with the target."
"245"	"9"	"User swaps Defense and Special Defense changes with the target."	"User swaps its [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} modifiers with the target."
"246"	"9"	"Power increases against targets with more raised stats, up to a maximum of 200."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power starts at 60 and is increased by 20 for every [stage]{mechanic:stage} any of the target's stats has been raised, capping at 200.  [Accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers do not increase this move's power."
"247"	"9"	"Can only be used after all of the user's other moves have been used."	"Inflicts [regular damage]{mechanic:regular-damage}.  This move can only be used if each of the user's other moves has been used at least once since the user entered the [field]{mechanic:field}.  If this is the user's only move, this move will [fail]{mechanic:fail}."
"248"	"9"	"Changes the target's ability to Insomnia."	"Changes the target's ability to []{ability:insomnia}.
"249"	"9"	"Only works if the target is about to use a damaging move."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target has not selected a damaging move this turn, or if the target has already acted this turn, this move will [fail]{mechanic:fail}.
"250"	"9"	"Scatters poisoned spikes, poisoning opposing Pokémon that switch in."	"Scatters poisoned spikes around the opposing [field]{mechanic:field}, which [poison]{mechanic:poison} opposing Pokémon that enter the [field]{mechanic:field}.  A second layer of these spikes may be laid down, in which case Pokémon will be [badly poison]{mechanic:badly-poison}ed instead.  Pokémon immune to either []{type:ground} moves or being [poison]{mechanic:poison}ed are immune to this effect.  Pokémon otherwise immune to []{type:ground} moves are affected during []{move:gravity}.
"251"	"9"	"User and target swap stat changes."	"User swaps its [stat modifiers]{mechanic:stat-modifiers} with the target."
"252"	"9"	"Restores 1/16 of the user's max HP each turn."	"Restores 1/16 of the user's max [HP]{mechanic:hp} at the end of each turn.  If the user leaves the [field]{mechanic:field}, this effect ends.
"253"	"9"	"User is immune to Ground moves and effects for five turns."	"For five turns, the user is immune to []{type:ground} moves.
"254"	"9"	"User takes 1/3 the damage inflicted in recoil.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/3 the damage it inflicts in recoil.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target.  [Frozen]{mechanic:frozen} Pokémon may use this move, in which case they will thaw."
"255"	"9"	"User takes 1/4 its max HP in recoil."	"Inflicts [typeless]{mechanic:typeless} [regular damage]{mechanic:regular-damage}.  User takes 1/4 its max [HP]{mechanic:hp} in recoil.  Ignores [accuracy]{mechanic:accuracy} and [evasion]{mechanic:evasion} modifiers.
"256"	"9"	"User dives underwater, dodging all attacks, and hits next turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  User dives underwater for one turn, becoming immune to attack, and hits on the second turn.
"257"	"9"	"User digs underground, dodging all attacks, and hits next turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  User digs underground for one turn, becoming immune to attack, and hits on the second turn.
"258"	"9"	"Inflicts regular damage and can hit Dive users."	"Inflicts [regular damage]{mechanic:regular-damage}.
"259"	"9"	"Lowers the target's evasion by one stage.  Removes field effects from the enemy field."	"Lowers the target's [evasion]{mechanic:evasion} by one [stage]{mechanic:stage}.  Clears away fog.  Removes the effects of []{move:mist}, []{move:light-screen}, []{move:reflect}, []{move:safeguard}, []{move:spikes}, []{move:stealth-rock}, and []{move:toxic-spikes} from the target's side of the [field]{mechanic:field}.
"260"	"9"	"For five turns, slower Pokémon will act before faster Pokémon."	"For five turns (including this one), slower Pokémon will act before faster Pokémon.  Move priority is not affected.  Using this move when its effect is already active will end the effect.
"261"	"9"	"Has a $effect_chance% chance to freeze the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [freeze]{mechanic:freeze} the target.
"262"	"9"	"Prevents the target from leaving battle and inflicts 1/16 its max HP in damage for 2-5 turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  For the next 2–5 turns, the target cannot leave the field and is damaged for 1/16 its max HP at the end of each turn.  The user continues to use other moves during this time.  If the user leaves the [field]{mechanic:field}, this effect ends.
"263"	"9"	"User takes 1/3 the damage inflicted in recoil.  Has a $effect_chance% chance to paralyze the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/3 the damage it inflicts in recoil.  Has a $effect_chance% chance to [paralyze]{mechanic:paralyze} the target."
"264"	"9"	"User bounces high into the air, dodging all attacks, and hits next turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  User bounces high into the air for one turn, becoming immune to attack, and hits on the second turn.  Has a $effect_chance% chance to [paralyze]{mechanic:paralyze} the target.
"266"	"9"	"Lowers the target's Special Attack by two stages if it's the opposite gender."	"Lowers the target's [Special Attack]{mechanic:special-attack} by two [stages]{mechanic:stage}.  If the user and target are the same gender, or either is genderless, this move will [fail]{mechanic:fail}."
"267"	"9"	"Causes damage when opposing Pokémon switch in."	"Spreads sharp rocks around the opposing [field]{mechanic:field}, damaging any Pokémon that enters the [field]{mechanic:field} for 1/8 its max [HP]{mechanic:hp}.  This damage is affected by the entering Pokémon's susceptibility to []{type:rock} moves.
"268"	"9"	"Has a higher chance to confuse the target when the recorded sound is louder."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has either a 1%, 11%, or 31% chance to [confuse]{mechanic:confuse} the target, based on the volume of the recording made for this move; louder recordings increase the chance of [confusion]{mechanic:confusion}.  If the user is not a []{pokemon:chatot}, this move will not cause [confusion]{mechanic:confusion}.
"269"	"9"	"If the user is holding a appropriate plate or drive, the damage inflicted will match it."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the user is holding a plate or a drive, this move's type is the type corresponding to that item.
"270"	"9"	"User receives 1/2 the damage inflicted in recoil."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/2 the damage it inflicts in recoil."
"271"	"9"	"User faints, and its replacement is fully healed."	"User [faint]{mechanic:faint}s.  Its replacement's [HP]{mechanic:hp} and [PP]{mechanic:pp} are fully restored, and any [major status effect]{mechanic:major-status-effect} is removed."
"272"	"9"	"Has a $effect_chance% chance to lower the target's Special Defense by two stages."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to lower the target's [Special Defense]{mechanic:special-defense} by two [stages]{mechanic:stage}."
"273"	"9"	"User vanishes, dodging all attacks, and hits next turn.  Hits through Protect and Detect."	"Inflicts [regular damage]{mechanic:regular-damage}.  User vanishes for one turn, becoming immune to attack, and hits on the second turn.
"274"	"9"	"Has a $effect_chance% chance to [burn]{mechanic:burn} the target and a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target and a separate $effect_chance% chance to make the target [flinch]{mechanic:flinch}."
"275"	"9"	"Has a $effect_chance% chance to freeze the target and a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [freeze]{mechanic:freeze} the target and a separate $effect_chance% chance to make the target [flinch]{mechanic:flinch}."
"276"	"9"	"Has a $effect_chance% chance to paralyze the target and a $effect_chance% chance to make the target flinch."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [paralyze]{mechanic:paralyze} the target and a separate $effect_chance% chance to make the target [flinch]{mechanic:flinch}."
"277"	"9"	"Has a $effect_chance% chance to raise the user's Special Attack by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to raise the user's [Special Attack]{mechanic:special-attack} by one [stage]{mechanic:stage}."
"278"	"9"	"Raises the user's Attack and accuracy by one stage."	"Raises the user's [Attack]{mechanic:attack} and [accuracy]{mechanic:accuracy} by one [stage]{mechanic:stage}."
"279"	"9"	"Prevents any multi-target moves from hitting friendly Pokémon this turn."	"Moves with multiple targets will not hit friendly Pokémon for the remainder of this turn.  If the user is last to act this turn, this move will [fail]{mechanic:fail}.
"280"	"9"	"Averages Defense and Special Defense with the target."	"Averages the user's unmodified [Defense]{mechanic:defense} with the target's unmodified Defense; the value becomes the unmodified Defense for both Pokémon. Unmodified [Special Defense]{mechanic:special-defense} is averaged the same way."
"281"	"9"	"Averages Attack and Special Attack with the target."	"Averages the user's unmodified [Attack]{mechanic:attack} with the target's unmodified Attack; the value becomes the unmodified Attack for both Pokémon. Unmodified [Special Attack]{mechanic:special-attack} is averaged the same way.
"282"	"9"	"All Pokémon's Defense and Special Defense are swapped for 5 turns."	"For five turns (including this one), every Pokémon's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} are swapped."
"283"	"9"	"Inflicts damage based on the target's Defense, not Special Defense."	"Inflicts [regular damage]{mechanic:regular-damage}.  Damage calculation always uses the target's [Defense]{mechanic:defense}, regardless of this move's damage class."
"284"	"9"	"Inflicts double damage if the target is Poisoned."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is [poisoned]{mechanic:poisoned}, this move has double power."
"285"	"9"	"Raises the user's Speed by two stages and halves the user's weight."	"Raises the user's [Speed]{mechanic:speed} by two [stages]{mechanic:stage}.  Halves the user's weight; this effect does not stack."
"286"	"9"	"Moves have 100% accuracy against the target for three turns."	"For three turns (including this one), moves used against the target have 100% [accuracy]{mechanic:accuracy}, but the target is immune to []{type:ground} damage.  Accuracy of one-hit KO moves is exempt from this effect.
"287"	"9"	"Negates held items for five turns."	"For five turns (including this one), passive effects of held items are ignored, and Pokémon will not use their held items."
"288"	"9"	"Removes any immunity to Ground damage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Removes the target's [immunity]{mechanic:immune} to []{type:ground}-type damage.  This effect removes any existing Ground immunity due to []{ability:levitate}, []{move:magnet-rise}, or []{move:telekinesis}, and causes the target's []{type:flying} type to be ignored when it takes Ground damage.
"289"	"9"	"Always scores a critical hit."	"Inflicts [regular damage]{mechanic:regular-damage}.  Always scores a [critical hit]{mechanic:critical-hit}."
"290"	"9"	"Deals splash damage to Pokémon next to the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  If this move successfully hits the target, any Pokémon adjacent to the target are damaged for 1/16 their max [HP]{mechanic:hp}."
"291"	"9"	"Raises the user's Special Attack, Special Defense, and Speed by one stage each."	"Raises the user's [Special Attack]{mechanic:special-attack}, [Special Defense]{mechanic:special-defense}, and [Speed]{mechanic:speed} by one [stage]{mechanic:stage} each."
"292"	"9"	"Power is higher when the user weighs more than the target, up to a maximum of 120."	"Inflicts [regular damage]{mechanic:regular-damage}.  The greater the user's weight compared to the target's, the higher power this move has, to a maximum of 120.
"293"	"9"	"Hits any Pokémon that shares a type with the user."	"Inflicts [regular damage]{mechanic:regular-damage}.  Only Pokémon that share a type with the user will take damage from this move."
"294"	"9"	"Power is higher when the user has greater Speed than the target, up to a maximum of 150."	"Inflicts [regular damage]{mechanic:regular-damage}.  The greater the user's [Speed]{mechanic:speed} compared to the target's, the higher power this move has, to a maximum of 150.
"295"	"9"	"Changes the target's type to Water."	"Changes the target to pure []{type:water}-type until it leaves the field.  If the target has []{ability:multitype}, this move will [fail]{mechanic:fail}."
"296"	"9"	"Inflicts regular damage.  Raises the user's Speed by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Raises the user's [Speed]{mechanic:speed} by one [stage]{mechanic:stage}."
"297"	"9"	"Lowers the target's Special Defense by two stages."	"Inflicts [regular damage]{mechanic:regular-damage}.  Lowers the target's [Special Defense]{mechanic:special-defense} by two [stages]{mechanic:stage}."
"298"	"9"	"Calculates damage with the target's attacking stat."	"Inflicts [regular damage]{mechanic:regular-damage}.  Damage is calculated using the target's attacking stat rather than the user's."
"299"	"9"	"Changes the target's ability to Simple."	"Changes the target's ability to []{ability:simple}."
"300"	"9"	"Copies the user's ability onto the target."	"Changes the target's ability to match the user's.  This effect ends when the target leaves battle."
"301"	"9"	"Makes the target act next this turn."	"The target will act next this turn, regardless of [Speed]{mechanic:speed} or move priority.If the target has already acted this turn, this move will [fail]{mechanic:fail}."
"302"	"9"	"Has double power if it's used more than once per turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  If []{move:round} has already been used this turn, this move's power is doubled.  After this move is used, any other Pokémon using it this turn will immediately do so (in the order they would otherwise act), regardless of [Speed]{mechanic:speed} or priority.  Pokémon using other moves will then continue to act as usual."
"303"	"9"	"Power increases by 100% for each consecutive use by any friendly Pokémon, to a maximum of 200."	"Inflicts [regular damage]{mechanic:regular-damage}.  If any friendly Pokémon used this move earlier this turn or on the previous turn, that use's power is added to this move's power, to a maximum of 200."
"304"	"9"	"Ignores the target's stat modifiers."	"Inflicts [regular damage]{mechanic:regular-damage}.  Damage calculation ignores the target's [stat modifiers]{mechanic:stat-modifiers}, including [evasion]{mechanic:evasion}."
"305"	"9"	"Removes all of the target's stat modifiers."	"Inflicts [regular damage]{mechanic:regular-damage}.  All of the target's [stat modifiers]{mechanic:stat-modifiers} are reset to zero."
"306"	"9"	"Power is higher the more the user's stats have been raised, to a maximum of 31×."	"Inflicts [regular damage]{mechanic:regular-damage}.  Power is increased by 100% its original value for every [stage]{mechanic:stage} any of the user's stats have been raised.  [Accuracy]{mechanic:accuracy}, [evasion]{mechanic:evasion}, and lowered stats do not affect this move's power.  For a Pokémon with all five stats modified to +6, this move's power is 31×."
"307"	"9"	"Prevents any priority moves from hitting friendly Pokémon this turn."	"Moves with priority greater than 0 will not hit friendly Pokémon for the remainder of this turn.  If the user is last to act this turn, this move will [fail]{mechanic:fail}.
"308"	"9"	"User switches places with the friendly Pokémon opposite it."	"User switches position on the field with the friendly Pokémon opposite it.  If the user is in the middle position in a triple battle, or there are no other friendly Pokémon, this move will [fail]{mechanic:fail}."
"309"	"9"	"Raises user's Attack, Special Attack, and Speed by two stages.  Lower user's Defense and Special Defense by one stage."	"Raises the user's [Attack]{mechanic:attack}, [Special Attack]{mechanic:special-attack}, and [Speed]{mechanic:speed} by two [stages]{mechanic:stage} each.  Lowers the user's [Defense]{mechanic:defense} and [Special Defense]{mechanic:special-defense} by one []{mechanic:stage} each."
"310"	"9"	"Heals the target for half its max HP."	"Heals the target for half its max [HP]{mechanic:hp}."
"311"	"9"	"Has double power if the target has a major status ailment."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target has a [major status ailment]{mechanic:major-status-ailment}, this move has double power."
"312"	"9"	"Carries the target high into the air, dodging all attacks against either, and drops it next turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  User carries the target high into the air for one turn, during which no moves will hit either Pokémon and neither can act.  On the following turn, the user drops the target, inflicting damage and ending the effect.
"313"	"9"	"Raises the user's Attack by one stage and its Speed by two stages."	"Raises the user's [Attack]{mechanic:attack} by one [stage]{mechanic:stage} and its [Speed]{mechanic:speed} by two stages."
"314"	"9"	"Ends wild battles.  Forces trainers to switch Pokémon."	"Inflicts [regular damage]{mechanic:regular-damage}, then [switches]{mechanic:switch-out} the target out for another of its trainer's Pokémon, selected at random.
"315"	"9"	"Destroys the target's held berry."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the target is [holding]{mechanic:held-item} a [berry]{mechanic:berry}, it's destroyed and cannot be used in response to this move."
"316"	"9"	"Makes the target act last this turn."	"Forces the target to act last this turn, regardless of [Speed]{mechanic:speed} or move [priority]{mechanic:priority}.  If the target has already acted this turn, this move will [fail]{mechanic:fail}."
"317"	"9"	"Raises the user's Attack and Special Attack by one stage."	"Raises the user's [Attack]{mechanic:attack} and [Special Attack]{mechanic:special-attack} by one [stage]{mechanic:stage} each.  During []{move:sunny-day}, raises both stats by two stages."
"318"	"9"	"Has double power if the user has no held item."	"Inflicts [regular damage]{mechanic:regular-damage}.  If the user has no [held item]{mechanic:held-item}, this move has double power."
"319"	"9"	"User becomes the target's type."	"User's type changes to match the target's."
"320"	"9"	"Has double power if a friendly Pokémon fainted last turn."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon fainted on the previous turn, this move has double power."
"321"	"9"	"Inflicts damage equal to the user's remaining HP.  User faints."	"Inflicts damage equal to the user's remaining [HP]{mechanic:hp}.  User faints."
"322"	"9"	"Raises the user's Special Attack by three stages."	"Raises the user's [Special Attack]{mechanic:special-attack} by three [stages]{mechanic:stage}."
"323"	"9"	"Raises the user's Attack, Defense, and accuracy by one stage each."	"Raises the user's [Attack]{mechanic:attack}, [Defense]{mechanic:defense}, and [accuracy]{mechanic:accuracy} by one [stage]{mechanic:stage} each."
"324"	"9"	"Gives the user's held item to the target."	"Transfers the user's [held item]{mechanic:held-item} to the target.  If the user has no held item, or the target already has a held item, this move will [fail]{mechanic:fail}."
"325"	"9"	"With [Grass Pledge]{move:grass-pledge}, halves opposing Pokémon's Speed for four turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon used []{move:grass-pledge} earlier this turn, all opposing Pokémon have halved [Speed]{mechanic:speed} for four turns (including this one)."
"326"	"9"	"With [Water Pledge]{move:water-pledge}, doubles the effect chance of friendly Pokémon's moves for four turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon used []{move:water-pledge} earlier this turn, moves used by any friendly Pokémon have doubled effect chance for four turns (including this one)."
"327"	"9"	"With [Fire Pledge]{move:fire-pledge}, damages opposing Pokémon for 1/8 their max HP every turn for four turns."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon used []{move:fire-pledge} earlier this turn, all opposing Pokémon will take 1/8 their max [HP]{mechanic:hp} in damage at the end of every turn for four turns (including this one)."
"328"	"9"	"Raises the user's Attack and Special Attack by one stage each."	"Raises the user's [Attack]{mechanic:attack} and [Special Attack]{mechanic:special-attack} by one [stage]{mechanic:stage} each."
"329"	"9"	"Raises the user's Defense by three stages."	"Raises the user's [Defense]{mechanic:defense} by three [stages]{mechanic:stage}."
"330"	"9"	"Has a $effect_chance% chance to put the target to sleep."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to put the target to [sleep]{mechanic:sleep}.If the user is a []{pokemon:meloetta}, it will toggle between Aria and Pirouette Forme."
"331"	"9"	"Lowers the target's Speed by one stage."	"Inflicts [regular damage]{mechanic:regular-damage}.  Lowers the target's [Speed]{mechanic:speed} by one [stage]{mechanic:stage}."
"332"	"9"	"Requires a turn to charge before attacking.  Has a $effect_chance% chance to paralyze the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [paralyze]{mechanic:paralysis} the target.  User charges for one turn before attacking."
"333"	"9"	"Requires a turn to charge before attacking.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [burn]{mechanic:burn} the target.  User charges for one turn before attacking."
"335"	"9"	"Lowers the user's Defense, Special Defense, and Speed by one stage each."	"Inflicts [regular damage]{mechanic:regular-damage}.  Lowers the user's [Defense]{mechanic:defense}, [Special Defense]{mechanic:special-defense}, and [Speed]{mechanic:speed} by one [stage]{mechanic:stage} each."
"336"	"9"	"With [Fusion Bolt]{move:fusion-bolt}, inflicts double damage."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon used []{move:fusion-bolt} earlier this turn, this move has double power."
"337"	"9"	"With [Fusion Flare]{move:fusion-flare}, inflicts double damage."	"Inflicts [regular damage]{mechanic:regular-damage}.  If a friendly Pokémon used []{move:fusion-flare} earlier this turn, this move has double power."
"338"	"9"	"Has a $effect_chance% chance to confuse the target."	"Inflicts [regular damage]{mechanic:regular-damage}.  Has a $effect_chance% chance to [confuse]{mechanic:confuse} the target.
"339"	"9"	"Deals both []{type:fighting} and []{type:flying}-type damage."	"Inflicts regular damage.  For the purposes of type effectiveness, this move is both []{type:fighting}- and []{type:flying}-type: its final effectiveness is determined by multiplying the effectiveness of each type against each of the target's types.
"340"	"9"	"Protects all friendly Pokémon from damaging moves.  Only works on the first turn after the user is sent out."	"Protects all friendly Pokémon from damaging moves.  Only works on the first turn after the user is sent out."
"341"	"9"	"Can only be used after the user has eaten a berry."	"Inflicts regular damage.  Can only be used if the user has eaten a berry since the beginning of the battle.
"342"	"9"	"Raises the Attack and Special Attack of all []{type:grass} Pokémon in battle."	"Raises the Attack and Special Attack of all []{type:grass} Pokémon in battle."
"343"	"9"	"Covers the opposing field, lowering opponents' Speed by one stage upon switching in."	"Shoots a web over the opponents' side of the field, which lowers the Speed of any opposing Pokémon that enters the field by one stage.
"344"	"9"	"Raises the user's Attack by two stages if it KOs the target."	"Inflicts regular damage.  Raises the user's Attack by two stages if it KOs the target."
"345"	"9"	"Adds []{type:ghost} to the target's types."	"Adds []{type:ghost} to the target's types."
"346"	"9"	"Lowers the target's Attack and Special Attack by one stage."	"Lowers the target's Attack and Special Attack by one stage."
"347"	"9"	"Changes all []{type:normal} moves to []{type:electric} moves for the rest of the turn."	"Changes all Pokémon's []{type:normal} moves to []{type:electric} moves for the rest of the turn."
"348"	"9"	"Heals the user for half the total damage dealt to all targets."	"Heals the user for half the total damage dealt to all targets."
"349"	"9"	"Adds []{type:grass} to the target's types."	"Adds []{type:grass} to the target's types."
"350"	"9"	"Super-effective against []{type:water}."	"Deals regular damage.  This move is super-effective against the []{type:water} type.
"351"	"9"	"Lowers all targets' Attack and Special Attack by one stage.  Makes the user switch out."	"Lowers all targets' Attack and Special Attack by one stage.  Makes the user switch out."
"352"	"9"	"Inverts the target's stat modifiers."	"Inverts the target's stat modifiers."
"353"	"9"	"Drains 75% of the damage inflicted to heal the user."	"Deals regular damage.  Drains 75% of the damage inflicted to heal the user."
"354"	"9"	"Protects all friendly Pokémon from non-damaging moves."	"Protects all friendly Pokémon from non-damaging moves for the rest of the turn.
"355"	"9"	"Raises the Defense of all []{type:grass} Pokémon in battle."	"Raises the Defense of all []{type:grass} Pokémon in battle."
"356"	"9"	"For five turns, heals all Pokémon on the ground for 1/16 max HP each turn and strengthens their []{type:grass} moves to 1.5× their power."	"For five turns, heals all Pokémon on the ground for 1/16 their max HP each turn and strengthens their []{type:grass} moves to 1.5× their power.
"357"	"9"	"For five turns, protects all Pokémon on the ground from major status ailments and confusion, and halves the power of incoming []{type:dragon} moves."	"For five turns, protects all Pokémon on the ground from major status ailments and confusion and weakens []{type:dragon} moves used against them to 0.5× their power.
"358"	"9"	"Changes the target's move's type to []{type:electric} if it hasn't moved yet this turn."	"Changes the target's move's type to []{type:electric} if it hasn't moved yet this turn."
"359"	"9"	"Prevents all Pokémon from fleeing or switching out during the next turn."	"Prevents all Pokémon from fleeing or switching out during the next turn."
"360"	"9"	"Blocks damaging attacks and lowers attacking Pokémon's Attack by two stages on contact.  Switches Aegislash to Shield Forme."	"Blocks damaging attacks and lowers attacking Pokémon's Attack by two stages on contact.  Switches Aegislash to Shield Forme."
"361"	"9"	"Lowers the target's Special Attack by one stage."	"Lowers the target's Special Attack by one stage."
"362"	"9"	"Blocks damaging attacks and damages attacking Pokémon for 1/8 their max HP."	"Blocks damaging attacks and damages attacking Pokémon for 1/8 their max HP."
"363"	"9"	"Raises a selected ally's Special Defense by one stage."	"Raises a selected ally's Special Defense by one stage."
"364"	"9"	"Lowers the target's Attack, Special Attack, and Speed by one stage if it is poisoned."	"Lowers the target's Attack, Special Attack, and Speed by one stage if it is poisoned."
"365"	"9"	"Explodes if the target uses a []{type:fire} move this turn, damaging it for 1/4 its max HP and preventing the move."	"Explodes if the target uses a []{type:fire} move this turn, damaging it for 1/4 its max HP and preventing the move."
"366"	"9"	"Takes one turn to charge, then raises the user's Special Attack, Special Defense, and Speed by two stages."	"Takes one turn to charge, then raises the user's Special Attack, Special Defense, and Speed by two stages."
"367"	"9"	"Raises the Defense and Special Defense of all friendly Pokémon with []{ability:plus} or []{ability:minus} by one stage."	"Raises the Defense and Special Defense of all friendly Pokémon with []{ability:plus} or []{ability:minus} by one stage."
"368"	"9"	"For five turns, prevents all Pokémon on the ground from sleeping and strengthens their []{type:electric} moves to 1.5× their power."	"For five turns, prevents all Pokémon on the ground from sleeping and strengthens their []{type:electric} moves to 1.5× their power.
"369"	"9"	"Lowers the target's Special Attack by two stages."	"Lowers the target's Special Attack by two stages."
"370"	"9"	"Doubles prize money."	"Doubles prize money. Stacks with a held item.  Only works once per battle."
"10001"	"9"	"Has an increased chance for a critical hit in Hyper Mode."	"Inflicts [regular damage]{mechanic:regular-damage}.  User's [critical hit]{mechanic:critical-hit} rate is one level higher when using this move while in [hyper mode]{mechanic:hyper-mode}."
"10002"	"9"	"User receives 1/2 its HP in recoil."	"Inflicts [regular damage]{mechanic:regular-damage}.  User takes 1/2 of its current [HP]{mechanic:hp} in recoil."
"10003"	"9"	"Halves HP of all Pokémon on the field.  Must recharge"	"Halves [HP]{mechanic:hp} of all Pokémon on the field.  User loses its next turn to "recharge", and cannot attack or [switch]{mechanic:switch} out during that turn."
"10004"	"9"	"Lowers the target's evasion by two stages."	"Lowers the target's [evasion]{mechanic:evasion} by two [stages]{mechanic:stage}."
"10005"	"9"	"Removes [Light Screen]{move:light-screen}, [Reflect]{move:reflect}, and [Safeguard]{move:safeguard}."	"Removes the effects of []{move:light-screen}, []{move:reflect}, and []{move:safeguard}."
"10006"	"9"	"Changes the weather to Shadow Sky for five turns."	"Changes the weather to Shadow Sky for five turns.  Pokémon other than []{type:shadow} Pokémon take 1/16 their max [HP]{mechanic:hp} at the end of every turn.  This move is typeless for the purposes of []{move:weather-ball}."
"10007"	"9"	"Unknown."	"This move has a unique effect, but it is unreleased, so the specific effect is unknown."
*/
}

// Attack targets (which pokemon to target with an attack)
abstract class AttackTarget
{
  const SpecificAttack = 1;
  const MeFirstAttack = 2;
  const SelectedAdjacentAlly = 3;
  const UsersField = 4;
  const SelectedUserOrAdjacentAlly = 5;
  const EnnemiesField = 6;
  const User = 7;
  const RandomAdjacentEnnemy = 8;
  const AllAdjacentPokemons = 9;
  const SelectedAdjacentPokemon = 10;
  const AllAdjacentEnnemies = 11;
  const EntireField = 12;
  const UserAndAllies= 13;
  const AllPokemonsIncludingUser = 14;
}

class Attacks
{
  /**
   * Get all attack categories (for a specific language)
   *
   * @param char(2) $language_code Language
   *
   * @return array Array with each sub array containing the attack category 'id', 'color', 'name' and 'description'
   *
   * @example print_r(Attacks::getAttackCategories("EN"));
   */
  public static function getAttackCategories($language_code)
  {
    $sql = "SELECT c.id, ct.name, c.color, ct.description FROM attack_classes c INNER JOIN attack_classes_translation ct ON c.id = ct.attack_class_id WHERE ct.language_code = :language_code;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the attack categories: ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get all attack/pokemon types (for a specific language)
   *
   * @param char(2) $language_code Language
   *
   * @return array Array with each sub-array containing the attack/pokemon type 'id', 'color', 'name' and 'description'
   *
   * @example print_r(Attacks::getTypes("EN"));
   */
  public static function getTypes($language_code)
  {
    $sql = "SELECT t.id, tt.name, t.color, tt.description FROM types t INNER JOIN types_translation tt ON t.id = tt.type_id WHERE tt.language_code = :language_code;";

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $values = array(":language_code" => $language_code);
      $query->execute($values);
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the attack/pokemon types: ".$e->getMessage());
      return array();
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  /**
   * Get a specific attack/pokemon type (for a specific language)
   *
   * @param char(2) $language_code Language
   *
   * @return array Array containing the attack/pokemon type 'id', 'color', 'name' and 'description', a sub-array (id 'strengths_to') containing type strengths (with the victim_type, the strength_multiplicator, the victim_type_name, the victime_type_description and the victim_color) and a sub-array (id 'strengths_from') containing type strengths (with the attacker_type, the strength_multiplicator and the attacket_type_name).
   *
   * @example print_r(Attacks::getType(1, "EN"));
   */
  public static function getType($type_id, $language_code)
  {
    // Get the attack type infos
    $sql = "SELECT t.id, tt.name, t.color, tt.description FROM types t INNER JOIN types_translation tt ON t.id = tt.type_id WHERE tt.language_code = :language_code AND t.id = :id;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $type_id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the attack/pokemon type infos: ".$e->getMessage());
      return array();
    }

    $attack_type = $query->fetch(PDO::FETCH_NAMED);
    if (empty($attack_type))
    {
      return array();
    }

    // Get the strengths_to
    $sql = "SELECT s.victim_type, s.strength_multiplicator, tt.name, tt.description, t.color FROM type_strengths s INNER JOIN types_translation tt ON s.victim_type = tt.type_id INNER JOIN types t ON s.victim_type = t.id WHERE s.attacker_type = :id AND tt.language_code = :language_code;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $type_id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the strengths to infos: ".$e->getMessage());
      return array();
    }

    $attack_type['strengths_to'] = $query->fetchAll(PDO::FETCH_NAMED);

    // Get the strengths_from
    $sql = "SELECT s.attacker_type, s.strength_multiplicator, tt.name, tt.description, t.color FROM type_strengths s INNER JOIN types_translation tt ON s.attacker_type = tt.type_id INNER JOIN types t ON s.attacker_type = t.id WHERE s.victim_type = :id AND tt.language_code = :language_code;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":id" => $type_id, ":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get the strengths to infos: ".$e->getMessage());
      return array();
    }

    $attack_type['strengths_from'] = $query->fetchAll(PDO::FETCH_NAMED);

    return $attack_type;
  }

  /**
   * Add an attack (all languages should be provided)
   *
   * @param int $id Id
   * @param AttackAndPokemonType $attack_type_id Attack type (id)
   * @param AttackCategory $attack_class_id Attack class (id)
   * @param int $attack_effect_1_id Attack effect (id), can be AttackEffect::None if none)
   * @param int $attack_effect_2_id Attack effect 2 (id), can be AttackEffect::None if none)
   * @param int(3) $power Power of the attack
   * @param int(2) $speed Speed of the attack
   * @param int(3) $accuracy_percentage Accuracy percentage of the attack
   * @param bool $unique_target Can attack only one pokemon at a time
   * @param bool $friend_target Can attack ally pokemon
   * @param int(3) $critical_strike_percentage Percentage of chance to do a critical hit
   * @param array $attack_translation Array with each sub-array (id being the language code) containing the 'name' and the 'description' of the attack
   *
   * @return bool Success
   *
   * @example Attacks::addAttack(1, 1, 1, 1, 1, 50, 20, 80, 75, true, false, 10,
		                             [
		                              "EN" => ["name" => "TestAttack", "description" => "Test en"],
		                              "FR" => ["name" => "TestAttaque", "description" => "Test fr"]
		                             ]);
   */
  public static function addAttack($id, $attack_type_id, $attack_class_id, $attack_effect_1_id, $attack_effect_2_id, $power, $pp, $speed, $accuracy_percentage, $unique_target, $friend_target, $critical_strike_percentage, $attack_translation)
  {
    if (!is_int($id) or !is_int($attack_type_id) or !is_int($attack_class_id) or !is_int($attack_effect_1_id) or !is_int($attack_effect_2_id) or !is_int($power) or !is_int($pp) or !is_int($speed) or !is_int($accuracy_percentage) or !is_bool($unique_target) or !is_bool($friend_target) or !is_int($critical_strike_percentage))
    {
      error_log("Impossible to add attack with not int in int parameters.");
      return false;
    }

    $languages = Languages::getLanguageCodes();
    if (SPDO::getInstance()->beginTransaction())
    {
      if (!$unique_target) { $unique_target = 0; } else { $unique_target = 1; }
      if (!$friend_target) { $friend_target = 0; } else { $friend_target = 1; }

      // Create base attack
      if (SPDO::getInstance()->exec("INSERT INTO attacks (id, attack_type_id, attack_class_id, attack_effect_1_id, attack_effect_2_id, power, pp, speed, accuracy_percentage, unique_target, friend_target, critical_strike_percentage) VALUES ($id, $attack_type_id, $attack_class_id, $attack_effect_1_id, $attack_effect_2_id, $power, $pp, $speed, $accuracy_percentage, $unique_target, $friend_target, $critical_strike_percentage);") < 1)
      {
        error_log("Could not create the attack with id=$id: ".$e->getMessage());
        SPDO::getInstance()->rollBack();
        return false;
      }

      // Add language specific attack elements
      foreach($languages as $language_code)
      {
        if (array_key_exists($language_code, $attack_translation))
        {
          $specific_attack = $attack_translation[$language_code];
          if (array_key_exists("name", $specific_attack) and array_key_exists("description", $specific_attack))
          {
            $name = $attack_translation[$language_code]['name'];
            $description = $attack_translation[$language_code]['description'];

            $sql = "INSERT INTO attacks_translation (attack_id, language_code, name, description) VALUES (:attack_id, :language_code, :name, :description)";
            $query = SPDO::getInstance()->prepare($sql);

            try
            {
              $query->execute(array(":attack_id" => $id, ":language_code" => $language_code, ":name" => $name, ":description" => $description));
            }
            catch (PDOException $e)
            {
              error_log("Could not create translation attack with elements attack_id=$id, language_code=$language_code name=$name and description='$description': ".$e->getMessage());
              SPDO::getInstance()->rollBack();
              return false;
            }
          }
          else
          {
            error_log("Name or description not specified in order to create an attack.");
            SPDO::getInstance()->rollBack();
            return false;
          }
        }
        else
        {
          error_log("Language ".$language_code." not specified in order to create the attack.");
          SPDO::getInstance()->rollBack();
          return false;
        }
      }

      if (!SPDO::getInstance()->commit())
      {
        error_log("Could not commit the attack.");
        SPDO::getInstance()->rollBack();
        return false;
      }
    }
    else
    {
      error_log("Failed to begin transaction");
      return false;
    }

    return true;
  }

  public static function getAllAttacks($language_code)
  {
    $sql = "
      SELECT
        a.id AS id,
        at.name AS name
      FROM attacks a
        INNER JOIN attacks_translation at ON at.attack_id = a.id
      WHERE
        at.language_code = :language_code
      ORDER BY
        at.name ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get all attacks: ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getPokemonsLearningAttack($language_code, $attack_id, $method_id)
  {
    $sql = "
      SELECT
        pla.pokemon_id AS id,
        pt.name AS name
      FROM pokemon_learn_attacks pla
        INNER JOIN pokemons_translation pt ON pt.pokemon_id = pla.pokemon_id
      WHERE
        pla.attack_id = :attack_id AND
        pla.pokemon_learn_attack_method_id = :method_id AND
        pt.language_code = :language_code
      ORDER BY
        pla.pokemon_id ASC;";
    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":method_id" => $method_id, ":attack_id" => $attack_id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get pokemons learning attack ".$attack_id." with method ".$method_id.": ".$e->getMessage());
    }

    return $query->fetchAll(PDO::FETCH_NAMED);
  }

  public static function getAttack($language_code, $id)
  {
    $sql = '
      SELECT
        a.id AS id,
        atr.name AS name,
        atr.description AS description,
        a.power AS power,
        a.pp AS pp,
        a.accuracy AS accuracy,
        a.priority AS priority,
        a.effect_chance AS effect_chance,
        ac.id AS class_id,
        ac.color AS class_color,
        act.name AS class_name,
        act.description AS class_description,
        atat.target_id AS target_id,
        atat.name AS target_name,
        atat.description AS target_description,
        aet.effect_id AS effect_id,
        aet.short_description AS effect_description,
        t.id AS type_id,
        t.color AS type_color,
        tt.name AS type_name,
        tt.description AS type_description
      FROM attacks a
        INNER JOIN attacks_translation atr ON atr.attack_id = a.id
        INNER JOIN attack_classes ac ON ac.id = a.class_id
        INNER JOIN attack_classes_translation act ON act.attack_class_id = a.class_id
        INNER JOIN attack_targets_translation atat ON atat.target_id = a.target_id
        INNER JOIN attack_effects_translation aet ON aet.effect_id = a.effect_id
        INNER JOIN types t ON t.id = a.type_id
        INNER JOIN types_translation tt ON tt.type_id = a.type_id
      WHERE
        a.id = :id AND
        atr.language_code = :language_code AND
        act.language_code = :language_code AND
        atat.language_code = :language_code AND
        aet.language_code = :language_code AND
        tt.language_code = :language_code;';

    $query = SPDO::getInstance()->prepare($sql);

    try
    {
      $query->execute(array(":language_code" => $language_code, ":id" => $id));
    }
    catch (PDOException $e)
    {
      error_log("Impossible to get attack information: ".$e->getMessage());
    }

    return $query->fetch(PDO::FETCH_NAMED);
  }
}

?>
