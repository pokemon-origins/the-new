echo "-------------HTML/CSS lines (AngularJs Gui)--------------------"
find . -type d \( -path ./website/external -o -path ./workshop \) -prune -o \( -name '*.html' -o -name '*.css' \) | xargs cat | wc -l

echo "-----------JS lines (AngularJs intelligence)-------------------"
find . -type d \( -path ./website/external -o -path ./workshop \) -prune -o \( -name '*.js' \) | xargs cat | wc -l

echo "------------------------PHP lines (API)------------------------"
find . -type d \( -path ./website/external -o -path ./workshop \) -prune -o \( -name '*.php' \) | xargs cat | wc -l

echo "---------------------------SQL lines---------------------------"
find . -type d \( -path ./website/external -o -path ./workshop \) -prune -o \( -name '*.sql' \) | xargs cat | wc -l

echo "----------------------------Scripts----------------------------"
find . -type d \( -path ./website/external -o -path ./workshop \) -prune -o \( -name '*.sh' \) | xargs cat | wc -l

echo "------------------------Images---------------------------------"
cd images

echo "Number of images:"
echo "JPG:"
find . -name \*.jpg -not -path \*/\.\* | wc -l
echo "PNG:"
find . -name \*.png -not -path \*/\.\* | wc -l
echo "SVG:"
find . -name \*.svg -not -path \*/\.\* | wc -l
echo "GIF:"
find . -name \*.gif -not -path \*/\.\* | wc -l

cd ..

echo "--------------------------End----------------------------------"

